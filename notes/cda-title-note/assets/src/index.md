# Présentation

As part of my vocational as an Application Designer and Developer at ENI Chartes de Bretagne, I developed a project, the mobile application Foncière AALTO, in the company Useweb in Cesson-Sévigné.

This application was developed in Flutter and Dart, it allows an investor linked to Foncière AALTO to connect to his personal space to consult his financial information, he or she can consult the details of each investment fund but also consult the latest financial news. If he wishes, he can modify his personal information, his financial contacts and his administrative documents in PDF format. If a new fund is available, they will receive a notification and can subscribe if they wish by filling in a form, downloading a PDF subscription form and sending it back from their smartphone. He can also download his financial statement in PDF if he wishes.

The application is connected to an API that depends on a back-office managed by the administrators where the data is retrieved and displayed in detail in tables and graphs. If the user wishes, he/she also has access to a web space where he/she can connect to perform the same actions as on his/her phone.

## Slide 1 : l'entreprise

- PME d'une dizaine de personnes
- rachetée par Michael HYOT en 2010 du conseil d'administration de Secob, entreprise de comptabilité
- spécialisée dans la création de site web en Nuxt.js avec un back-office avec Laravel
- une équipe s'occupe du référencement naturel
- première application mobile avec Flutter

# Contexte

## Slide 2 : cahier des charges

- une entreprise d'investissements et gestions d'actifs immobilier
- investisseurs privés et institutionnels
- un parc total de 200 000 m² de locaux d’activités et d’immeubles de bureaux
- propriété de groupe CAP Transactions
- premier projet : un intranet en 2018 par Adrien Beaudouin en Laravel 5.8
  - back-office en Vue.js embarqué -> DETAILS
  - espace investisseur en Vue.js embarqué -> DETAILS
- modifications du back-office et application mobile
  - interface mobile efficiente et notifications

## Slide 3 : contraintes & livrables

- application mobile disponible sur iOS et Android
- temps de développement extensible de 3 à 6 mois
- sécurisée
- qualité par rapport aux autres technologies
- non-précisé : Android Lollipop 5.0 et iOS 9 (iOS 11 pour l'upload)

## Slide 4 : analyse du besoin

- analyse réalisée par le manager
- maquettes réalisées par la designeuse
- sur les stores mais authentification obligatoire
- création d'une api protégée
- application
  - récupérer ses informations financière
  - consulter les nouveaux fonds disponibles
  - recevoir des notifications
  - consulter ses fonds d'investissement
  - consulter les actualités proposées par Foncière AALTO
- back-office
  - ajouts de champs : texte, fichiers...
  - email avec un jeton d'authentification (nouvelle offre)
  - suivi de l'activité des investisseurs
  - étapes pour les actifs immobiliers

# Gestion

## Slide 5 : Gantt du back-end

- retrait des jours superflus : cours, vacances...
- commencé le 1er septembre 2020
- terminé en janvier 2021
- seule développeuse
- autoplanification
- tâches toute nouvelles
- recette client
  - suppression d'une table
  - modification des spécifications fonctionnelles
- décalage dans le développement

## Slide 6 : Gantt de l'application

- apprendre les fondamentaux
- créer l'api
- mettre à jour le projet : sanctum
- pattern de développement : juste `lib`
- grand décalage le temps d'apprendre

## Slide 7 : environnement

- seule développeuse
- équipe de développeurs pour conseiller
  - projet original en Laravel
- manager qui a écrit les spécifications fonctionnelles
- retours clients
- pas de personne ressource pour Flutter
  - problèmes techniques
  - besoin de plus de temps

## Slide 8 : environnement management

- cycle en V pour la gestion globale du projet
- agilité pour les échanges avec le manager
- retours clients de façon régulière

## Slide 9 : objectifs de qualité

- solidité et fiabilité
  - opposition aux autres technologies
- MVVM
- sécurisé
- maintenable et évolutif
  - durable dans le temps
- écologie : Flutter plus lourd aux téléchargement

# Conception

## Slide 10 : technologies

- Flutter 1.2 en migration vers 2.0 -> POURQUOI
- Laravel 5.8 migré vers 6.0 pour Sanctum -> POURQUOI
- MySQL 8.0
- Nuxt.js 2.15 (non détaillé)

## Slide 11 : architecture

- MVVM - Model View View-Modèle
- MVC - Model View Controller

## Slide 12 : tables principales

- real_estate_companies -> Fonds d'investissement
- investors -> Investisseurs
- locals -> Actifs immobiliers
- leases -> Bails
- tenants -> Locataires

Relations

- real_estate_companies -> MANY TO MANY -> investors
- real_estate_companies -> MANY TO ONE -> locals
- locals -> MANY TO ONE -> leases
  - historiques des bails mais un seul bail actif
- leases -> ONE TO MANY -> tenants

## Slide 13 : tables secondaires

- users -> ONE TO ONE -> investors
- addresses -> MANY TO MANY (POLY)
- posts
- media -> MANY TO MANY (POLY)
- tables de Laravel

# Développement

## Slide 14 : CompanyController & routes API

- requête avec Eloquent en Many To Many basé sur une condition de la table pivot
- resources de Laravel
- routes api protégées par le middleware Sanctum, officiel

## Slide 15 : représentations SQL

- requêtes SQL traduisant l'ORM en deux versions : where & join
- autres requêtes

## Slide 16 : resources

- resources différenciées selon le but
  - liste limitée pour tous les fonds d'investissement
  - liste détaillée avec les relations pour le détail d'un seul fond d'investissement
  - limiter la taille de la sortie API

## Slide 17 : dart modèle et sérialisation

- reproduction du modèle du back-end tel que l'API le propose
- sérialisation créée automatiquement avec la librairie officielle json_serializable -> DETAILS

## Slide 18 : IHM connexion

- form avec key et node
- TextEditingField avec controller, specific keyboard, formatter...
- node qui suit l'utilisateur dans le form
- connexion avec validation
  - feedback utilisateur selon le status code
  - connexion depuis un service

## Slide 19 : appel API sanctum

- connexion sur la route login, en méthode POST
  - device_id
- traitement de la réponse, sauvegarde du token
- traitement de la requête
  - validate
  - vérification des informations et retours différents
  - génération du token
- suppression du token lors de la déconnexion

## Slide 20 : IHM fonds d'investissement

- widget stateful
- récupération des données asynchrone dans un FutureBuilder
  - refresh
- MVVM
- view model avec requête GET avec le token
  - récupération des données
  - traitement du JSON
  - conversion en une List()

## Slide 21 : sécurité

Détails

# Démonstration

## Slide 22 : démonstration

Vidéo de démonstration.

# Recherche

## Slide 23 : recherche sur la sérialisation

- utilisateur de la librairie json_serializable
- maintenance manuelle complexe et inutile
- ajout des dépendances
- utilisation de l'annotation et des méthodes générées
- JsonKey()
- CLI de génération

# Conclusion

## Slide 24 : application sur les stores

- différences de déploiement
- Android plus simple
  - Flutter vient de Google
- iOS bien plus compliqué à cause des certificats
  - Xcode, versions et distributions de mac
- Maintenance et mise à jour assez facile

## Slide 25 : satisfaction

- flutter très agréable à prendre en main
- communauté très active
- perdue au début à cause de l'absence d'architecture
  - MVVM
- domaine mobile différent du web
- possibilité de mener des projets personnels
- refactoring de l'application
  - base de données locale
  - tests
  - bloc pattern

## Slide 26 : questions

End!
