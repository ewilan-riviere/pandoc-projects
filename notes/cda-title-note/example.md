# 1ère slide

image des distri linux
Jouer sous Linux :

- Difficultés à avoir des jeux disponibles par le passé
- Windows comme OS le plus répandu
- Open source et Linux : jeux propriétaires (éditeurs)
- Linux : multiples communautés, distributions différentes, difficultés d'adapter les jeux sur toutes les distributions

# 2ème slide

image de steam
Arrivée de Steam sur le marché :

- Volonté d'adapter le plus de jeux possibles : SteamOS, Linux-friendly
- Principal quesiton : DRM
- Opposition à l'open source habituel sous Linux, question de propriété et dépendance
- Acheter des jeux sous Steam : choix cornélien (adapté sous Linux mais pas de droits sur le jeu)

# 3ème slide

image de ./play.it
./play.it comme alternative pour jouer sous Linux

- Adapter des jeux à Linux, sur plusieurs de ses distributions
- Plus qu'adapter des jeux Windows, installer correctement des jeux même déjà compatibles sous Linux
- Utilise des jeux sans DRM (GOG ou Humble Bundle) qu'on peut télécharger après les avoir acheter et paratager librement
- Structure légère et adaptable avec des scripts qui utilisent l'archive du jeu pour construire le répertoire d'installation
- Jeu directement disponible dans les applications Linux sans passer par un lanceur comme Steam

# 4ème slide

image de vv221
Le client
Antoine Le Gonidec (vv221)

- Origine du projet : double-boot Linux/Windows
- Question des DRM
- Open source
- Accent sur les scripts : site web peu important
- Volonté d'attirer des personnes moins habituées à l'utilisation du terminal
- Projets existants : sous DRM, avec lanceur, plus lourds, importance de la légèreté et modulaire

# 5ème slide

image du site de ./play.it wiki
Projet de ./play.it

- Un début avec quelques jeux et scripts
- Un site web de style wiki avec les pages codées en dur pour présenter les jeux et les scripts
- Quelques années plus tard, plus de 400 jeux et toujours pas de base de données
- Difficulté de maintenance et d'ajout des jeux

# 6ème slide

image du nouveau site web

- Nécessité d'un nouveau site web
- Attirer un public frileux des manipulations liées au terminal : public venant de Windows ayant un double boot pour jouer ou pour travailler
- Site agréable dans sa présentation et simple dans sa navigation
- Scripts affichés clairement avec la liste des commandes à faire, de manière ludique
- Avec une base de données
- Utilisation d'une API pour récupérer la liste des jeux
- Ajouter des informations à ces jeux depuis le site web avec une interface utilisateur (CRUD)
- Pas de JavaScript pour les fonctionnalités vitales : avoir toute le site accessible sans JS, pour les personnes qui n'utilisent pas de JS, pour des connexion/machines trop lentes, accessibilité

# 7ème slide

image de l'api et de la base de données vers ./play.it

- Une base de données gère toutes les informations optionnelles
- Titre, plateforme d'achat et scripts gérés par l'API : une fiche non renseignée aura ces infos de base
- Pas de JavaScript : pas de framework JS
- Utiliser une API déjà existante pour récupérer la liste des jeux
- Créer un CRUD pour des informations optionnelles pour les jeux : description, développeur, date de sortie, tags, config. PC, PEGI...
- Design agréable

# 8ème slide

image des frameworks et dépendances
Outils et méthodes

- Laravel 5.8, Composer 1.8, NodeJS 11.15, Bootstrap 4
- Laravel : front-end et back-end
  - Blade à la place d'un framework JS
  - Connexion à l'API dotaslashplay.it dans un seul sens
- Pas de l'API rest dans un premier temps
- Composer pour les dépendances de Laravel
- Blade : php avec une syntaxe facilitant la lecture, connexion directe aux controllers de Laravel
- NodeJS pour Mix de Laravel permettant de compiler le SASS et les polices utilisées

# 9ème slide

image du carousel

- Utilisation du CSS 3 pour faire les animations souvent faites en JavaScript
  - problème : limitation par le code du nombre d'images
- Barre de navigation également en CSS 3 qui ne se déclenche que si le JavaScript a été désactivé

# 10ème slide

image files
Expression des besoins

- Différence entre les fonctionnalités prévues et finalement développées
- accent sur les présentation des jeux et des fiches
- mise de côté de l'internationalisation, du forum et du tri des jeux
- Responsive moins important

# 11ème slide

image read

- Récupération depuis l'API : affichage avec les images de base de données locale
- Sélection par lettre qui nécessite un tri par lettre de la liste récupérée
- Recherche des jeux depuis l'API

# 12me slide

image fiche
Fiche

- Titre, plateforme d'achat et scripts récupérés depuis l'API
- Bannières, impressions d'écran, titre avec les graphismes du jeu : database images
- Description, franchise, date de sortie, développeur, liens vers le forum et les reviews : database infos
- PEGI : âges  et sujets perturbants : database pegi
- Configuration : processeur, mémoire, graphismes, espace disque : database config
- Modification de la fiche si utilisateur connecté

# 13ème slide

Conception : site web

- Comprendre Laravel et réussir à traiter les données reçues de l'API : m'approprier le projet
- Un premier test de CRUD : une todolist
- Premier défi : ordonner les informations de l'api pour obtenir une liste utilisable pour un humain
- Classer les jeux selon la première lettre de leur jeu pour créer des sections
- Créer la structure avec toutes les pages prévues pour l'expression de besoins : même si tout ne pouvait pas être traité pendant cette partie du projet

# 14ème slide

Conception : base de données

- Créer la base de données pour le CRUD des fiches
- Faire le lien avec l'API depuis la table games_info
- Relations entre les tables : Id de games_info permettant de faire le lien entre toutes les tables
- Cardinalités
  - Tables : games_image, games_pegi, games_config : relation 0,1
  - Table : games_tag : relation n,m (table intermédaire)
    - Example : jeux de stratégie
  - Table : users : isolée et sans liens avec les autres : que les utilisateurs
- Internationalisation : mise de côté pour ce projet mais sera intégrée plus tard (changement dans la base de données)
- Détailler les champs

# 15ème slide

list

- récupération de la liste depuis l'API
- les classer par lettre dans un tableau : variable $alpha_json
- création des variables game_id et game_name nécessaires à la vue pour retrouver les jeux selon leur game_id (lien entre l'API et la table games_info) avec leur nom

# 16ème slide

list_view

# 17ème slide

jeux

- a reçu ses informations depuis la function byname (comme list mais pour les sections)
- variable n modifée lors du clic sur la section
- utilisation de la variable n pour sélectionner les jeux à afficher par une boucle if
- boucle for qui tourne sur les noms des jeux limités par la variable n
- affichage du lien faire la fiche du jeu en route
- affichage de la bannière si elle existe, une bannière par défaut est donnée aux jeux qui n'en ont pas
- affichage du nom

# 18ème slide

edit

- récupération de la table gamesinfo selon la variable game_id passée par la function edit
- Récupération du nom du jeu grâce au game_id
- une boucle IF crée les entrées en base de données si la variable de gamesinfo est nulle, ce qui signifie que le jeu n'a pas encore reçu d'informations
- la variable gamesinfo est de nouveau récupérée pour les jeux qui n'avaient pas d'entrée
- récupération de toutes les modèles liés à cette table grâce à l'id de la variable gamesinfo
- requête pour une relation n, m de la table tags
- envoi des variables vers la vue

# 19ème slide

routes

- la plupart des routes sont gérées par le WebsiteController si elles n'utilisent pas la base de données et qu'elles ne retournent qu'une vue
- l'introduction et l'accueil ont leur propre controller qui leur permet de tirer des informations dans la base de données pour le text affiché : développement futur avec la possibilité de modifier le texte directement
- la liste des jeux a deux routes différentes : une pour tous les jeux et l'autre pour les sections avec une variable n dans la route, elles sont gérées par deux functions différentes assez proches dans le GamesController
- la route jeux-fiche est gérée par le GamesController et la function show qui renvoit toutes les informations directement à la vue de chaque fiche
- les routes protégées par l'authentification sont les routes permettant d'éditer les fiches de jeux, liées à la route resource gamesinfo, la route de logout et la route de confirmation de suppression des infos liées à un jeu

# 20ème slide

# 21ème slide

difficultés

- comprendre Laravel
- réussir à traiter plus de 400 jeux et les ordonner
- lier l'API et une base de données
- réaliser un design suffisamment attractif en évitant le JavaScript

# 22ème slide

satisfaction

- avoir réalisé un CRUD parfaitement fonctionnel qui gère plusieurs situation :
  - example : il n'y a pas d'entrée en base de données
  - impressions d'écran ajoutées qui ont des ratio différents : utilisation de Image Intervention grâce à des recherches sur Stack Overflow
- le site web est utilisable actuellement et peut être mis en production
- mon client est content de mon travail
- le projet me plaît (éthique, domaine...) et je souhaite continuer à travailler dessus

# 24ème slide

futur

- continuer à travailler sur le projet : mettre en place un forum, des reviews, un glossaire...
- faire une API rest pour ajouter des nouveaux jeux depuis le site web
- projet d'envergure qui m'a permis de comprendre comment travailler sur un projet à long terme