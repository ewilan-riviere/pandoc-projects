# Preface {.unnumbered}

Sit cillum id enim enim laborum cupidatat. Amet cupidatat reprehenderit consectetur do aliqua ut consequat non in elit nostrud. Aliqua id amet eu amet adipisicing fugiat pariatur excepteur consectetur laborum labore adipisicing in. Occaecat est fugiat id exercitation laborum non nisi. Voluptate labore dolor tempor Lorem aliquip quis sint ad duis.

Tempor ad ex eiusmod in laboris incididunt dolor do nostrud id incididunt veniam. Voluptate dolor sint veniam consequat laborum reprehenderit veniam dolor et. Irure elit ullamco qui irure adipisicing Lorem dolore aute et nostrud elit.

Qui minim mollit est dolor magna voluptate nulla. Occaecat qui cupidatat officia ut ipsum id. Aute anim exercitation qui fugiat ad officia qui reprehenderit qui amet ea. Ex incididunt amet culpa eiusmod cillum laborum veniam officia esse ipsum nostrud culpa. Deserunt ut in qui eu deserunt voluptate amet dolore. Ex ut labore nostrud Lorem ipsum duis. Excepteur reprehenderit voluptate exercitation incididunt adipisicing anim magna labore ad ipsum.

# Markdown

## Code blocks

Check my [`nuxt.config.js`](#nuxt-config-js)

```{#nuxt-config-js .js .numberLines startFrom="0"}
export default {
  generate: {
    exclude: [/^\/content/],
  },
  target: process.env.TARGET,
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: metadata.tags.title,
    titleTemplate: metadata.tags.titleTemplate,
    htmlAttrs: {
      lang: metadata.settings.locale,
    },
    meta: [...staticMetadata(), ...dynamicMetadata()],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
}
```

```php
<?php

namespace App\Http\Controllers\Api;

use File;
use ZipArchive;
use App\Models\Book;
use App\Models\Serie;
use App\Models\Author;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\MediaLibrary\Support\MediaStream;

class DownloadController extends Controller
{
  public function book(Request $request, string $author, string $book)
  {
    $book = Book::whereSlug($book)->firstOrFail();
    if ($book->author->slug === $author) {
        $epub = $book->getMedia('epubs')->first();
    } else {
        return response()->json(['Error' => 'Book not found'], 401);
    }

    return response()->download($epub->getPath(), $epub->file_name);
  }
}
```

## Tables

  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

| Themenschwerpunkt       | Programmierung der App                                  |
| ----------------------- | ------------------------------------------------------- |
| Ziele und Anforderungen | RE-M 5 Automatisches einlesen der Rechnung per Foto     |
|                         | RE-M 7 Sicherung der Daten                              |
|                         | RE-M 8 Automatische Einkaufsliste                       |
|                         | RE-M 9 Authentifizierung                                |
|                         | RE-M 12 Projektwebsite                                  |
|                         | RE-M 16 Veröffentlichung                                |
|                         | RE-O 3 Rechnungen und Einkauflisten als PDF exportieren |

-------------------------------------------------------------
 Centered   Default           Right Left
  Header    Aligned         Aligned Aligned
----------- ------- --------------- -------------------------
   First    row                12.0 Example of a row that
                                    spans multiple lines.

Second    row                 5.0 Here's another one. Note
                                    the blank line between
                                    rows.
-------------------------------------------------------------

+---------------+---------------+--------------------+
| Fruit         | Price         | Advantages         |
+===============+===============+====================+
| Bananas       | $1.34         | - built-in wrapper |
|               |               | - bright color     |
+---------------+---------------+--------------------+
| Oranges       | $2.10         | - cures scurvy     |
|               |               | - tasty            |
+---------------+---------------+--------------------+

| Right | Left | Default | Center |
| ----: | :--- | ------- | :----: |
|    12 | 12   | 12      |   12   |
|   123 | 123  | 123     |  123   |
|     1 | 1    | 1       |   1    |

## Blockquote

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.

## Links

[I'm an inline-style link](https://www.google.com)

[I'm an inline-style link with title](https://www.google.com "Google's Homepage")

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[I'm a relative reference to a repository file](../blob/master/LICENSE)

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself].

URLs and URLs in angle brackets will automatically get turned into links.
<http://www.example.com> or <http://www.example.com> and sometimes
example.com (but not on Github, for example).

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com

## Images

![image](assets/images/default.jpg)

## Math

$a^2 + b^2 = c^2$

$v(t) = v_0 + \frac{1}{2}at^2$

$\gamma = \frac{1}{\sqrt{1 - v^2/c^2}}$

$\exists x \forall y (Rxy \equiv Ryx)$

$p \wedge q \models p$

$\Box\diamond p\equiv\diamond p$

$\int_{0}^{1} x dx = \left[ \frac{1}{2}x^2 \right]_{0}^{1} = \frac{1}{2}$

$e^x = \sum_{n=0}^\infty \frac{x^n}{n!} = \lim_{n\rightarrow\infty} (1+x/n)^n$

## Bibliography link example

i.  [@nonexistent]
i.  @nonexistent
i.  @z1 says fooo.
i.  @z1 [p. 30] says baaar.
i.  @z1 [p. 30, with suffix] says blahblah.
i.  @z1 [-@z2 p. 30; see also @z3] says blah-blubb.
i.  In a footnote.[^1]
i.  A citation group [see  @z1 p. 34-35; also @z3 chap. 3].
i.  Another one [see @z1 p. 34-35].
i.  And still another, in a footnote.[^2]
i.  Quote with a *suffix* and a *locator* [@z1 pp. 33, 35-37, and nothing else].
i.  Quote with only one locator [@z1 and nowhere else].
i.  Now a few *modifiers*[^3]...
i.  With some extra Markup [*see* @z1 p. **32**].
i.  Jane Doz doesnt like me [***see*** **@z4**].

[^1]: A citation without locators [@z3].

[^2]: Multiple citations [siehe @z2 chap. 3; @z3; @z1].

[^3]: ...like a quote without author: [-@z1]. And now OStR Oster with a locator [-@z2 p. 44].

$x^2 + y^2 = 1$ @z1
