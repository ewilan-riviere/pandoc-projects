# Pandoc Projects

Generate all reports from `reports/` to PDF into `output/`

```bash
make
```

Generate specific report to PDF

```bash
make name=title-web-developer-mobile report
```

Clean all PDF

```bash
make clean
```

To add new project

```bash
make name=my-awesome-project report-new
```

## Filters

### pandoc-mustache

With [pandoc-mustache](https://github.com/michaelstepner/pandoc-mustache), you can display metadata from `metadata.yaml` into Markdown

```md
{{title}}
```

You need Python 3 with pip to install `pip install -U pandoc-mustache`

```bash
make filter=pandoc-mustache
```

```bash
make filter=pandoc-mustache name=my-awesome-project report
```

## With `make`

### Templates

You can change template to use with `template` option. The value have to be a `tex` file inside `templates/` dir.

> report is default template

```bash
make template=pandoc.tex
```

### Highlight

You can choose another highlight theme for code blocks, default is a fork of `kate` theme but you can choose original highlight theme or custom `templates/themes/basic.theme`

```bash
make highlight=zenburn
```

### Bibliography

You can use `citeproc` to get bibliography, in sample `metadata.yaml` you have `bibliography` key (if you don't want bibliography, just remove this key)

```yaml
bibliography: index.bib
```

In `index.bib`, for example

```bib
@Book{ref1,
  author="Thales von Milet",
  title="Doppelwinkel-Funktionen",
  url="http://de.wikipedia.org/wiki/Formelsammlung_Trigonometrie#Doppelwinkelfunktionen",
  year="600 v.Chr.",
  address="Milet, Kleinasien",
  publisher="Wikipédia"
}
```

In `assets/src/any-filename.md`

```md
# References

::: {#refs}
:::
```

`citeproc` will generate bibliography in `{#refs}`

And in any markdown file you can reference bibliography

```bash
About Doppelwinkel-Funktionen[@ref1]
```

This `[@ref1]` will reference this element from `index.bib`

### CSL

CSL represent to format of bibliography, you can choose a different CSL format with `csl`, add just new CSL file to `templates/csl/`

>Default format is `multiple-sclerosis-journal`

```bash
make csl=new-csl.csl
```

If you want to get some infos about CSL styles, check [**example gallery**](https://github.com/KurtPfeifle/pandoc-csl-testing/blob/master/example-gallery.md) and you can download `csl` on [**official CSL styles**](https://github.com/citation-style-language/styles)
