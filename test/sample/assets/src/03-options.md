# Options

## Filters

### pandoc-mustache

With [pandoc-mustache](https://github.com/michaelstepner/pandoc-mustache), you can display metadata from `metadata.yaml` into Markdown

```md
{{title}}
```

You need Python 3 with pip to install `pip install -U pandoc-mustache`

```bash
make filter=pandoc-mustache
```

```bash
make filter=pandoc-mustache name=my-awesome-project report
```

## With `make`

### Templates

You can change template to use with `template` option. The value have to be a `tex` file inside `templates/` dir.

> report is default template

```bash
make template=pandoc.tex
```

### Highlight

You can choose another highlight theme for code blocks, default is a fork of `kate` theme but you can choose original highlight theme or custom `templates/themes/basic.theme`

```bash
make highlight=zenburn
```

### Bibliography

You can use `citeproc` to get bibliography, in sample `metadata.yaml` you have `bibliography` key (if you don't want bibliography, just remove this key)

```yaml
bibliography: index.bib
```

In `index.bib`, for example

```bib
@Book{ref1,
  author="Thales von Milet",
  title="Doppelwinkel-Funktionen",
  url="http://de.wikipedia.org/wiki/Formelsammlung_Trigonometrie#Doppelwinkelfunktionen",
  year="600 v.Chr.",
  address="Milet, Kleinasien",
  publisher="Wikipédia"
}
```

In `assets/src/any-filename.md`

```md
# References

::: {#refs}
:::
```

`citeproc` will generate bibliography in `{#refs}`

And in any markdown file you can reference bibliography

```bash
About Doppelwinkel-Funktionen[@ref1]
```

This `[@ref1]` will reference this element from `index.bib`

### CSL

CSL represent to format of bibliography, you can choose a different CSL format with `csl`, add just new CSL file to `templates/csl/`

>Default format is `multiple-sclerosis-journal`

```bash
make csl=new-csl.csl
```

If you want to get some infos about CSL styles, check [**example gallery**](https://github.com/KurtPfeifle/pandoc-csl-testing/blob/master/example-gallery.md) and you can download `csl` on [**official CSL styles**](https://github.com/citation-style-language/styles)

## Glossary

From <https://github.com/tomncooper/pandoc-gls>

Welcome to (+pandoc-gls)' test source code document. But I really don't want this ->
(-pandoc-gls) or this (-+^FOO) to be linked to the glossary!

You have to watch out for those (+^BAR), they are (+^FOO) (although (+BAZ) disagrees).
(++^FOO) should not be trusted! Also watch out for the (+BAR)'s dog it is a biter!

Some other text with _emphasised_ glossary terms _(+FOO)_ **(+BAZ)**

We also need to make sure the surrounding punctuation and characters are not ignored:

- ((+FOO)).
- ((++BAZ)),
- ((+^BAR))!?!?!?!
- (+pandoc-gls)oloing

Some inline code `code ac (+^BAZ)`.

Why not have a table:

| Name    | Description |
|---------|-------------|
| Dave    | (+BAR)      |
| (+BAZ)  | (++FOO)     |
