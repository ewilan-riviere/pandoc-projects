# Preface {.unnumbered}

Sit cillum id enim enim laborum cupidatat. Amet cupidatat reprehenderit consectetur do aliqua ut consequat non in elit nostrud. Aliqua id amet eu amet adipisicing fugiat pariatur excepteur consectetur laborum labore adipisicing in. Occaecat est fugiat id exercitation laborum non nisi. Voluptate labore dolor tempor Lorem aliquip quis sint ad duis.

Tempor ad ex eiusmod in laboris incididunt dolor do nostrud id incididunt veniam. Voluptate dolor sint veniam consequat laborum reprehenderit veniam dolor et. Irure elit ullamco qui irure adipisicing Lorem dolore aute et nostrud elit.

Qui minim mollit est dolor magna voluptate nulla. Occaecat qui cupidatat officia ut ipsum id. Aute anim exercitation qui fugiat ad officia qui reprehenderit qui amet ea. Ex incididunt amet culpa eiusmod cillum laborum veniam officia esse ipsum nostrud culpa. Deserunt ut in qui eu deserunt voluptate amet dolore. Ex ut labore nostrud Lorem ipsum duis. Excepteur reprehenderit voluptate exercitation incididunt adipisicing anim magna labore ad ipsum.

# Chapter example

Extract of "Ellana la prophétie" by Pierre Bottero in French, volume 3 in Le Pacte des MarchOmbres series.

>Ellana la prophétie est le troisième et dernier tome que Pierre Bottero consacre à son personnage favori, Ellana. C’est la fin de la trilogie Le Pacte des Marchombres. Ce dernier tome marque le grand retour, aussi attendu qu’espéré, des héros d’Ewilan. Car ce livre est aussi une suite au cycle d’Ewilan, commencé en 2003 par la trilogie La Quête d’Ewilan, suivie par une seconde, Les Mondes d’Ewilan. En effet, si dans les tomes 1 et 2 du Pacte des Marchombres, l’action se situe avant La Quête d’Ewilan, Ellana la prophétie reprend l’intrigue là elle s’était arrêtée à la fin des Tentacules du mal, dernier tome des Mondes d’Ewilan.

## Chapitre 1

Parmi les différents itinéraires qui s'offraient à eux, Salim avait opté pour celui qui traversait les montagnes de l'Est à la hauteur d'Al-Jeit. Ce choix impliquait un important détour vers le sud, mais leur évitait de franchir le profond canyon de la Voleuse, la rivière qui serpentait sur l'autre versant des montagnes.

Seul, Salim aurait coupé au plus court pour le plaisir de se mesurer aux falaises dont lui avait parlé Ellana, mais ni Ewilan ni Edwin n'étaient de vrais grimpeurs et la sécurité avait prévalu.

— Vous pourrez passer les montagnes avec vos chevaux, avait argumenté Ellana, ce qui vous avantagera pour traverser le désert des Murmures. Au final, vous gagnerez peut-être du temps.

— Plus facile, plus rapide, pourquoi alors n'as-tu pas emprunté cet itinéraire lorsque tu as rallié le Rentaï ? s'était étonné Salim.

— Parce qu'un des marchombres qui m'a testée lors de mon Ahn-Ju avait tenté de me tuer et tout prêtait à croire qu'il recommencerait. Je devais brouiller les pistes.

— Salvarode ?

— Ou Jorune. Je ne l'ai jamais su.

La voix d'Ellana s'était durcie lorsqu'elle avait mentionné Jorune, le marchombre responsable de la mort de Jilano. Si, par respect pour son maître, elle avait renoncé à exercer sa vengeance, la haine palpitait encore en elle, brûlante, lorsqu'elle l'évoquait.

— Des deux, c'est pourtant Salvarode qui a trahi la guilde au profit des mercenaires du Chaos, avait remarqué Salim.

— Certes, mais celui qui m'a poignardée lors de mon Ahn-Ju voyait parfaitement dans le noir, or la greffe de Salvarode n'avait aucune relation avec les yeux.

— Dis, Ellana, si le Rentaï m'accepte, quelle sera ma greffe ?

— Nul ne peut le prédire, jeune apprenti !

C'était à cette réponse laconique que songeait Salim alors qu'il chevauchait à travers les dunes du désert des Murmures en compagnie d'Edwin et Ewilan.

La greffe.

Le Rentaï l'en jugerait-il digne et, si ce n'était pas le cas, la déception qu'il ressentirait n'aurait-elle pas sur lui l'effet qu'elle avait eu sur Nillem, l'ancien élève de Sayanel dont Ellana ne parlait qu'à contrecœur ?

Lorsqu'il était parvenu à reconstituer l'histoire de Nillem et à comprendre les effets pernicieux de son orgueil, Salim s'était juré de ne jamais suivre son exemple. Il avait toutefois acquis suffisamment de maturité pour deviner que le piège dans lequel était tombé Nillem n'était pas de ceux qu'il était facile d'éviter, tant la frontière se montrait ténue entre confiance en soi et prétention.

La voix d'Ewilan tira Salim de ses pensées.

— Ellana était-elle censée nous en révéler autant sur la greffe ?

— De quoi parles-tu ?

— La greffe et le moyen de l'obtenir sont parmi les secrets les mieux gardés de la guilde. Ne risque-t-elle pas d'avoir des problèmes avec le Conseil pour en avoir parlé à des non-marchombres ?

Salim secoua la tête.

— Je ne pense pas qu'elle se soit posé la question et non, je ne pense pas qu'elle aura des problèmes. Pour tout dire je ne vois pas un marchombre se risquer à lui en faire le reproche. Sayanel est sans doute le seul qui le pourrait mais cela ne correspond pas avec ce que je sais de lui.

— D'autres que les marchombres connaissent le Rentaï, intervint Edwin.

— Les mercenaires du Chaos ?

— Pas seulement. Les rêveurs du septième cercle le connaissent depuis bien plus longtemps qu'eux, tout comme l'Empereur et son entourage proche, ainsi que certains seigneurs tels que Saï Hil' Muran ou mon père Hander Til' Illan.

Salim poussa un sifflement surpris.

— Eh bien! Cela fait du monde...

— Oui, et puisque nous en sommes aux révélations, je peux te dévoiler que des Alaviriens n'arpentant pas la voie des marchombres ont même tenté l'escalade du Rentaï afin de comprendre ce qui s'y déroulait.

— Et ?

— Très peu s'y sont risqués, très peu parmi ceux-là sont redescendus et aucun n'a rien appris.

Salim et Ewilan tournèrent ensemble la tête vers le maître d'armes.

— Fais-tu partie du nombre ? demanda Salim.

Edwin laissa filer la question comme si elle avait été tissée de vent et non de mots. Le visage impassible, il désigna la direction de l'est d'un doigt sûr.

— Nous devrions atteindre le Rentaï demain dans la matinée.

Ocre et bleu.

Ocre, une succession de dunes aux courbes douces et aux arêtes aussi vives que fragiles que le vent avait gravées de mystérieux messages ondulés.

Bleu, un ciel intense que le soleil implacable, malgré ses efforts, ne parvenait pas à adoucir.

Le désert incitait au silence. Les trois compagnons n'échangeaient guère plus d'une dizaine de phrases par jour et, pourtant, ils se sentaient plus proches qu'ils ne l'avaient jamais été. Le désert liait les âmes.

Lorsque la chaleur devenait trop écrasante, Ewilan tentait de dessiner une pluie rafraîchissante. Lorsqu'elle y parvenait, la rigueur du voyage se voyait atténuée, mais l'effet ne durait pas et il était vain de compter sur cette eau pour se désaltérer.

— C'est un des paradoxes du dessin, expliqua-t-elle un matin à ses amis. Boire l'eau que je crée étanchera votre soif le temps, limité, que durera mon dessin, ce qui a une utilité très relative. En revanche, si j'utilise les plus hautes Spires pour imaginer une eau éternelle, vos corps ne seront pas capables de l'assimiler et elle ne vous soulagera même pas de la chaleur.

— Où se trouve le paradoxe dans ce que tu expliques ? demanda Edwin.

— Mon eau ne peut pas te désaltérer mais tu peux t'y noyer, et dans ce cas l'effet est... permanent.

— Je vois, fit Edwin en scrutant le paysage depuis le sommet de la dune qu'ils venaient d'atteindre.

Plongé depuis des mois dans une relation quasi exclusive avec Ellana, Salim avait oublié à quel point le maître d'armes était impressionnant. De stature moyenne, sans une once de graisse sur une musculature fine taillée pour l'efficacité, il portait sur les gens et les choses un regard gris acier auquel rien n'échappait. Outre son physique, sa manière de se mouvoir tout en puissance contenue, sa voix calme et posée et la certitude irradiant de lui qu'il était prêt à réagir en une fraction de seconde à n'importe quel événement en faisaient un personnage hors du commun.

Et c'était compter sans sa maîtrise absolue des arts du combat.

De tous les arts du combat.

Malgré ses indéniables compétences de chef, Edwin veillait à ne pas prendre le commandement de leur petite équipe et, sans émettre le moindre commentaire, se conformait aux décisions de Salim.

Ainsi mis en valeur, Salim sentait les dernières bribes de son adolescence s'effilocher derrière lui et ce fut d'un geste plein d'assurance qu'il désigna l'horizon empourpré par le soleil levant.

— Le Rentaï, annonça-t-il.

## Chapitre 2

Le Rentaï n'était pas une montagne isolée mais un impressionnant et chaotique massif composé de falaises abruptes, d'éperons rocheux surplombant des éboulis vertigineux, de gorges étroites s'enfonçant dans l'obscurité, de pics escarpés, de dalles en dévers et de vires inaccessibles. D'un ton plus sombre que le désert environnant, l'ocre de ses flancs était parsemé de rares taches de végétation témoignant de la présence d'eau.

— Ellana m'a dit que nous trouverions à boire au pied de la première falaise, indiqua Salim alors qu'ils s'approchaient. Et là où il y a à boire, nous découvrirons aussi les mercenaires, si mercenaires il y a...

Sur le qui-vive depuis que le Rentaï était apparu, les trois amis scrutaient le sable à la recherche d'éventuelles traces mais aucune empreinte, aucune silhouette, n'était visible. Salim et Edwin conservaient néanmoins leur arc à la main, une flèche encochée tandis qu'Ewilan se tenait prête à investir l'Imagination.

Une éternité plus tôt, de gigantesques rochers avaient dévalé les flancs du Rentaï pour s'écraser au pied de la falaise formant sa base. Il en avait résulté un labyrinthe, impressionnant par la dimension des blocs qui le constituaient plus que par sa complexité et, entre les blocs, les premières zones d'ombre véritable que les voyageurs rencontraient depuis qu'ils avaient franchi la Voleuse.

Les sabots de leurs chevaux ne faisaient aucun bruit dans le sable et, attentifs à ce qui les environnait, ils ne parlaient pas.

Ce fut sans doute ce qui leur sauva la vie. Dans le profond silence du désert, le sifflement des flèches fut nettement perceptible.

Salim plongea sur Ewilan et roula dans le sable avec elle. En sécurité.

Edwin aussi avait plongé.

Une fraction de seconde après Salim.

Une fraction de seconde utilisée pour repérer les tireurs embusqués au sommet d'un rocher proche.

Alors qu'il n'avait pas encore touché le sol, il banda son arc et lâcha la flèche qu'il tenait prête. Elle se ficha dans la poitrine d'un des hommes qui poussa un cri et bascula dans le vide.

Déjà Edwin effectuait un roulé-boulé, se relevait, posait un genou à terre, décochait un nouveau trait.

Il fila, presque invisible, accompagné de son jumeau décoché, lui, par Salim.

Deux mercenaires du Chaos — ce ne pouvaient être que des mercenaires du Chaos — dégringolèrent du rocher.

— Je m'occupe de leurs flèches ! cria Ewilan en se jetant dans l'Imagination.

Un dôme scintillant se matérialisa au-dessus d'eux. Une dizaine de traits mercenaires y rebondirent alors que les flèches de Salim et Edwin le traversaient sans mal. Deux nouveaux archers s'écrasèrent sur le sable. Les survivants, quatre ou cinq hommes, disparurent.

— Ils abandonnent ? s'étonna Salim.

— Ça m'étonnerait, répondit Edwin. L'effet de surprise n'ayant pas joué, ils vont...

Cinq mercenaires surgirent en courant de derrière un rocher et, sabre au clair, se ruèrent sur eux, si rapides que Salim et Edwin n'eurent que le temps de lâcher leur arc pour saisir leur lame et se porter à leur rencontre. Ce fut le choc.

Ewilan s'était tapie dans une excavation et attendait le moment propice pour agir. Un moment qui tardait. Les combattants étaient trop proches, la mêlée trop confuse. Elle se mordit le poing et se résigna à regarder.

Confronté à deux mercenaires décidés à le tuer, Salim évitait les coups avec l'extraordinaire vivacité que lui avait enseignée Ellana. Pareil à un feu follet, il plongeait, roulait, bondissait, insaisissable.

_«Parade et attaque sont l'inspiration et l'expiration du marchombre. Son combat est un seul souffle, qu'il dure une seconde ou une heure.»_

Salim laissa la lame de son adversaire frôler son bras, tournoya, frappa, se remit en garde.

Gorge ouverte, un mercenaire s'effondra.

Escrimeurs pourtant confirmés, les trois hommes qui avaient attaqué Edwin réalisaient, stupéfaits, que celui qui leur faisait face était plus dangereux qu'eux trois réunis.

Incapables de percer ses défenses, ils éprouvaient de plus en plus de difficultés à résister à la pression qu'il leur imposait, commençaient à commettre des erreurs, ne parvenaient plus à...

Le sabre d'Edwin glissa sous une garde relâchée, se ficha dans un cœur, ressortit, écarlate, fouetta l'air.

Deux fois.

Le maître d'armes se tourna vers Salim qui affrontait le dernier mercenaire. Il n'eut pas le temps d'intervenir.

_«L'erreur est de croire que le marchombre a besoin d'une arme. Le marchombre est une arme.»_

De son sabre, le mercenaire contra le poignard, estima qu'il tenait un avantage, se décala...

Le coude de Salim percuta son menton avec une telle violence que sa tête partit en arrière, ses vertèbres cervicales se brisèrent net, entraînant une mort instantanée.

Jambes fléchies, prêt à réagir, Salim pivota pour observer les environs puis il se détendit imperceptiblement. Edwin approuva d'une moue satisfaite.

— Tu as fait des progrès, constata-t-il. Ces hommes n'étaient pas d'excellents combattants, mais tu n'as commis aucune erreur.

— Je considérerai cela comme un compliment, décida Salim. Crois-tu qu'il y en ait d'autres ?

— Si c'est le cas, nous ne tarderons pas à le savoir.

Les chevaux s'étaient égaillés. Lorsque Ewilan siffla Aquarelle, la petite jument revint au galop vers sa maîtresse, suivie des chevaux d'Edwin et Salim. Les trois amis se mirent en selle et, attentifs au moindre bruit, au moindre mouvement, reprirent leur chemin.

Ils parvinrent très vite au pied d'une falaise, premier véritable bastion du Rentaï. Quelques buissons épineux poussaient sur le sable et, derrière un rocher, Edwin découvrit une vasque naturelle creusée dans la pierre qui recueillait l'eau d'une source discrète.

— Ce doit être l'endroit dont nous a parlé Ellana, dit-il en parcourant les alentours du regard. Nous pourrions nous y...

Il se tut.

Un air extatique s'était peint sur le visage de Salim. Les mains posées à plat contre la falaise, les yeux levés, il semblait écouter une musique qu'il était le seul à entendre.

— Salim ? s'inquiéta Ewilan. Ça va ?

Il s'ébroua comme s'il sortait d'un rêve.

— Oui, oui, ça va. Incroyablement bien. Vous pensez-vous en sécurité ici ?

— C'est un lieu qui en vaut un autre, répondit Edwin. Nous ne serons pas attaqués par le haut et si Ewilan dessine un de ces écrans scintillants dont elle a le secret, je ne vois pas ce qui...

Il se tut à nouveau.

Sans plus leur accorder la moindre attention, Salim avait commencé à escalader la falaise.

## Chapitre 3

Salim perçut le murmure en atteignant le sommet de la falaise.

Un étrange et doux murmure, improbable mariage des arpèges d'un instrument à cordes, du vent et d'une voix cristalline. Il ne provenait de nulle part en particulier et, jouant avec les échos du Rentaï, il se déploya autour de Salim, caressant ses oreilles avant de se glisser en lui, intime et apaisant.

Salim l'écouta sans bouger, insensible au vide béant dans son dos et à la beauté du désert qui s'étirait jusqu'à l'horizon.

Il l'écouta longtemps.

Replié sur lui-même et ouvert sur l'univers.

Lorsque le murmure se tut, il n'éprouva aucune déception, aucune crainte. Il se mit en marche.

Il aborda une étendue caillouteuse formée de galets ronds qui roulaient sous ses pas. Le soleil matinal avait entrepris la conquête du Rentaï et tentait de l'écraser de ses rayons, mais Salim ne sentait pas leur morsure sur ses épaules et sa nuque. Il avançait, confiant, sans éprouver le moindre doute sur l'itinéraire qu'il devait suivre.

 _«Il n'y a pas de bon ou de mauvais chemin dans le Rentaï Il y a juste ton chemin.»_

Au sortir de l'étendue caillouteuse, et alors que des dizaines d'aiguilles rocheuses le regardaient de haut, il pénétra dans une gorge si étroite qu'il pouvait toucher ses parois en écartant les bras. La fraîcheur qui y régnait le surprit le temps d'un sourire puis il l'oublia pour ne plus penser qu'à son chemin.

Son chemin.

La gorge se rétrécit encore. Il dut se placer de profil pour se faufiler entre ses mâchoires, se baisser, ramper...

La gorge débouchait sur le vide. Un gouffre de plusieurs centaines de mètres dont le fond était garni de roches acérées. Une arche de pierre le traversait de part en part, si aérienne, si fine, si fragile d'aspect qu'elle paraissait incapable de soutenir le poids d'un oiseau.

Salim s'y engagea sans la moindre crainte.

Le murmure revint à cet instant précis. Plus fort, plus prégnant, encore plus harmonieux.

Son chemin.

Bras écartés pour disputer son équilibre au vent, Salim traversa le pont, atteignit la paroi opposée, la gravit. Ses mouvements étaient en accord parfait avec son esprit. Le murmure l'accompagnait.

Il l'accompagnait toujours lorsque, après avoir dévalé un éboulis et escaladé une nouvelle falaise, Salim arriva devant une dalle bleutée à la surface aussi lisse qu'un miroir. Un hoquet du Rentaï avait jeté cette improbable passerelle au-dessus d'un large canyon et, comme la paroi opposée était bien plus basse que celle où se tenait Salim, la dalle s'en trouvait transformée en toboggan pour titan.

Son chemin.

La pierre était glissante. Qu'à cela ne tienne. Jambes fléchies, pieds perpendiculaires à la pente, Salim la descendit comme une flèche, achevant sa glissade folle par un bond sur le côté qui lui permit de se jucher sur un rocher. De là il sauta sur une étroite vire saillant d'une paroi grêlée de trous minuscules qu'il entreprit de gravir.

Le murmure devint un chant.

Formidable d'exultation.

Il n'y avait plus de prises sur la paroi polie, presque lustrée par le vent du désert.

Salim continuait à grimper.

Porté par le chant.

Le chant du Rentaï.

Son chemin.

Il atteignit le sommet. S'arrêta. Il lui était impossible d'aller plus loin, d'aller plus haut.

Peu importait. Il était arrivé.

Il se tenait sur une dalle plate, à la pointe d'un piton qui dépassait d'une tête les autres aiguilles du Rentaï et, où qu'il se tournât, sa position lui offrait une vue aussi inouïe qu'imprenable sur le désert des Murmures.

Le désert des Murmures.

Tirait-il son nom du chant qui désormais vibrait en lui ?

Imitant sans le savoir les gestes qu'avait accomplis Ellana des années plus tôt, Salim déposa son arc et son carquois à ses pieds, dégrafa son ceinturon et le fourreau du poignard qui y était attaché, ôta ses bottes. Il retira ensuite sa tunique et se plaça face à l'est.

Inspiration. Profonde. Mains qui montent, s'écartent, paumes tournées vers le haut.

Expiration. Longue. Mains qui reviennent vers le centre.

Inspiration.

Expiration.

Le chant du Rentaï accompagnait chacun de ses gestes, les auréolant d'une lumière d'absolu.

Son chemin.

Salim s'immergea quatre fois dans la gestuelle marchombre.

En direction des quatre points cardinaux.

Lorsque son cœur battit au rythme de celui du Rentaï, lorsque le murmure devenu chant jaillit de chacune de ses cellules, lorsque le soleil se trouva juste au-dessus de lui, il s'avança vers le centre de la dalle.

Une forme y était creusée. Douce, les contours arrondis. Salim s'y allongea, remarquant sans surprise qu'elle paraissait avoir été moulée sur son corps.

Il ferma les yeux.

La pierre, chauffée par le soleil, palpitait contre lui comme si elle avait été vivante.

L'extraordinaire sérénité dans laquelle baignait Salim se para d'une certitude.

La pierre était vivante.

Le chant du Rentaï prit de l'ampleur, devint une voix. Aussi brûlante que le soleil. Aussi dense que la nuit. Aussi grave que l'éternité.

— Qui es-tu ?

— Bonjour, répondit Salim sans ouvrir les yeux.

La roche fléchit sous son poids. Se referma au-dessus de lui.

Doucement.

Les yeux toujours fermés, l'âme tranquille, Salim s'enfonça dans le Rentaï.
