.SILENT: all generate report clean title-web-dev report-new report-demo
.PHONY: generate

MYDIR = reports/*/

template ?= 1
highlight ?= 1
filter ?=
csl ?= 1

ifeq ($(template),1)
    TEMPLATE = "../../templates/report.tex"
else
    TEMPLATE = "../../templates/$(template).tex"
endif
ifeq ($(highlight),1)
    HIGHLIGHT = "../../templates/themes/basic.theme"
else
    HIGHLIGHT = $(highlight)
endif
ifeq ($(filter),1)

else
    FILTER = "../../templates/filters/$(filter)"
endif
ifeq ($(csl),1)
    CSL = "../../templates/csl/multiple-sclerosis-journal.csl"
else
    CSL = "../../templates/$(csl)"
endif

all:
	echo "Generate all reports"; \
	echo ""; \
	for dir in $(shell ls -d ${MYDIR}) ; do \
		make -s dir=$$dir generate ; \
		echo ""; \
	done

generate:
	cd $(dir); \
	$(eval filename = $(shell basename $(dir))) \
	rm -f ../../output/$(filename).pdf; \
	echo "Generate $(filename).pdf..."; \
	pandoc \
		--from=markdown \
		--template=$(TEMPLATE) \
		--metadata-file=metadata.yaml \
		--pdf-engine=xelatex \
		$(if $(filter),--filter=$(FILTER),) \
		--highlight-style=$(HIGHLIGHT) \
		--citeproc \
		--csl=$(CSL) \
		--out=../../output/$(filename).pdf \
		--top-level-division=chapter \
		--lua-filter ../../templates/filters/pandoc-gls.lua \
		assets/src/*.md
	echo "Successfully generated at $(filename).pdf!"

report-demo:
	cd test/sample-to-copy; \
	rm -f ../../output/demo.pdf; \
	echo "Generate demo.pdf..."; \
	pandoc \
		--from=markdown \
		--template=$(TEMPLATE) \
		--metadata-file=metadata.yaml \
		--pdf-engine=xelatex \
		$(if $(filter),--filter=$(filter),) \
		--highlight-style=$(HIGHLIGHT) \
		--citeproc \
		--csl=$(CSL) \
		--out=../../output/demo.pdf \
		--top-level-division=chapter \
		assets/src/*.md
	echo "Successfully generated at demo.pdf!"


report:
	make -s dir=reports/$(name) generate; \

clean:
	find . -type f -name '*.pdf' -delete
	echo "Delete all PDF files!";

title-web-dev:
	cd reports/title-web-developer-mobile; \
	pdflatex report-original.tex; \
	rm -f *.pdf; \
	pdflatex report-original.tex; \
	mv report-original.pdf title-web-developer-mobile-original.pdf; \
	mv title-web-developer-mobile-original.pdf ../../output

report-new:
	echo "Create $(name)"
	cp -r test/sample-to-copy reports/
	mv reports/sample-to-copy reports/$(name)
	echo "Project ready!"

# slides

# beamer, revealjs
format ?= 1
output ?= 1

ifeq ($(format),1)
    FORMAT = beamer
	OUTPUT = pdf
else
    FORMAT = $(format)
	OUTPUT = html
endif

slide:
	cd slides/cda-title; \
	pandoc \
	-t $(FORMAT) \
	-s index.md \
	-o ../../output/cda-title-slides.$(OUTPUT);

slide-example:
	cd slides/example; \
	pandoc -t $(FORMAT) -s index.md -o ../../output/example-slides.$(OUTPUT);

note:
	make -s dir=notes/$(name) generate; \
