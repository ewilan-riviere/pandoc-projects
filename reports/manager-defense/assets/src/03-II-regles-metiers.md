# Règles métiers & héritage

Le projet demande de bien saisir qui sont les acteurs et comment ils interagissent entre eux avant d'analyser en détailler chacune des étapes de mise en place d'un équipement de son installation au SAV.

## Les différents acteurs

### Useweb

**Petite agence web**[@useweb], l'équipe travaille à la réalisation de sites web sur mesure pour des clients, en particulier des entreprises, allant de 3 000 € à 300 000 €. Le but de Useweb est de proposer un suivi précis des demandes des clients afin de leur proposer une solution répondant au mieux à leurs besoins avec la possibilité d'avoir un suivi sur du long terme. Il arrive aussi que les clients demandent des développements supplémentaires après plusieurs mois si l'outil leur convient, mais que leurs besoins ont évolué. Useweb assure donc un suivi de (+TMA) sur plusieurs années dans le domaine du numérique et la plupart des clients ont leurs sites web hébergés sur les serveurs de l'agence.

C'est donc une PME d'une dizaine de personnes rachetée par Michael HYOT en 2010 du conseil d'administration de Secob, entreprise de comptabilité. Plusieurs prestations sont proposées : du **design alliant l'UI à l'UX**, le **développement web**, du **référencement naturel** et du **référencement payant**, la **création de site web** et des **social Ads**.

L'**agence a déjà travaillé avec Groupama** par le passé, notamment sur le projet **Groupama Extranet** qui a été mis en place par Adrien Beaudouin, l'ancien lead tech de l'équipe. Les projets suivants ont repris la **même architecture technique**, mais le départ d'Adrien Beaudouin n'a pas été sans poser quelques problèmes de maintenance de chaque projet lié à cette architecture. Le présent projet est le troisième utilisant cette solution technique, qui n'a pas été mise à jour par le refus du manager en gestion du projet.

De plus, le départ de Michel TRAUTH, l'ancien manager de l'équipe de développement, a créé de nombreux problèmes de communication et de suivi pour le projet actuel. Il a été remplacé pour une partie de ses fonctions par Laurent Chevalier et pour une autre par Stéphane BLIVET.

> Le projet a donc subi quelques déconvenues durant son développement à cause d'un manque de cohérence dans le suivi, la maintenance de l'application et dans la communication.

La situation de l'agence est expliquée plus en détails dans les **Annexes**.

### Groupama

**Commanditaire du projet**, **Groupama**[@groupama] est une société d'**assurance mutuelle** d'origine française, liée à deux marques connues. Le label Groupama d'abord, qui est une marque d'assurance généraliste distribuée par un réseau de Caisses régionales, et implantée dans plus de 10 pays en Europe et en Asie. Mais aussi Gan, assureur des entrepreneurs ayant des filiales concernant les assurances, le patrimoine et la prévoyance.

Le groupe est présent des assurances aux services financiers. Groupama est premier en matière d’assurance agricole, de santé individuelle et des collectivités locale, c'est l'assureur de la moitié des communes françaises. Ainsi, c'est le 2ème assureur en habitation et le 4ème en assurance auto[^2]. Il couvre les besoins des particuliers, des professionnels, des entreprises, des associations et des collectivités locales. Groupama est également présent sur le marché de l’assistance avec Mutuaide[^3] et avec Groupama Protection Juridique qui accompagne ses clients pour la résolution de litiges.

Parmi ses activités, on peut souligner plusieurs filiales : Gan (Gan Eurocourtage, Gan Assurances, Gan Patrimoine, Gan Prévoyance), Orange Bank (avec 35 % du capital), Groupama Épargne salariale, Société française de Protection juridique, Groupama Immobilier, Mutuaide, G2S (Groupama Supports et Services) ou Rent A Car (avec 20 % du capital). Ce sont l'ensemble de ces filiales qui ont permis à Groupama de s'implanter à l'étranger et de gagner en importance. En plus de ces activités, Groupama a souhaité s'investir dans des actions de mécénat, scientifique et culturel à travers des fondations et être acteur du sport avec le parrainage nautique.

C'est **Groupama Loire Bretagne (appelé GLB)**, une caisse régionale, qui a souhaité développer le projet en collaboration avec Useweb pour affiner **son activité avec Télésécurité Loire Bretagne**.

## Télésécurité Loire Bretagne (TLB)

Filiale du groupe Groupama Gan, **TLB**[@tlb] bénéficie d'un savoir faire issu de d'un grand groupe d'assurance, précurseur en matière de prévention des risques, l'entreprise propose plusieurs **de télésurveillance**. Elle assure un diagnostic des besoins client pour une solution personnalisée, afin de proposer la protection la plus appropriée. Très complet et intégré, les services proposés concernent l'*installation*, la *maintenance*, la *télésurveillance* (24h/24, 7j/7) et l'*intervention* en cas d'alerte.

Ils proposent des équipements de pointe pour une solution optimale, par exemple le couplage « sirène/interphonie » comme mode de dissuasion le plus efficace, en cas d'intrusion dans une habitation. Leur offre de maintenance est rapide et efficace, à l'aide d'équipes de techniciens spécialisés, réparties sur les régions Bretagne et Pays de Loire pour réaliser les opérations de maintenance dans les meilleurs délais.

TLB est donc en dépendance vis-à-vis de **Groupama qui va se charger de trouver**, proposer ou joindre **les clients** quand **TLB** va s'occuper de la **partie technique** de l'installation et de la maintenance des équipements. L'idée est de simplifier les processus de communication entre les différentes entités afin de permettre d'avoir une vue d'ensemble, d'améliorer la productivité et diminuer les erreurs possibles.

Cependant, ces deux entreprises disposent d'actrices et d'acteurs qui ont des rôles et des droits spécifiques sur chaque étape du processus de validation d'un équipement ou de création d'un client, il s'agit donc d'identifier et de répondre aux besoins spécifiques de chacun.

## Contraintes et processus

Les deux entreprises sont donc co-dépendantes, Groupama va se charger de l'aspect relation client qui comprend le démarchage des clients et leur suivi quand TLB s'occupe de l'installation et de la technique.

Actuellement leur mode de communication est basé sur un fichier Excel par équipement et par client, maintenu par Groupama et qui navigue surtout entre les différents acteurs de la société, et avec lequel les techniciens de TLB peuvent interagir ponctuellement. Ceux-ci renseignent d'habitude les détails sur l'installation et la prise en charge sur une plateforme différente, **Praxedo**[@praxedo], une application web permettant de gérer les interventions comme le temps passé ou la facturation. Cette plateforme va ensuite communiquer avec **Sage**[@sage], un autre logiciel, utilisé par Groupama pour la comptabilité et les devis. Ce que les techniciens de TLB ont saisi sur **Praxedo** se voit donc transféré sur Sage et c'est depuis cette plateforme que Groupama va pouvoir renseigner les informations de facturation sur les fichiers Excel.

Cependant, cette communication n'est ici abordée que de manière superficielle, car elle prend tout son sens dans le processus d'installation d'un équipement où tous les acteurs interagissent les uns avec les autres. Actuellement, le processus fonctionne comme ceci :

![Première partie du processus](assets/images/workflow-01.jpg)

---

![Seconde partie du processus](assets/images/workflow-02.jpg)

L'idée est de garder la logique du processus tout rendant chaque étape plus marquée et où la validation est soumise à des règles précises. Du côté de Groupama, on a quatre rôles bien différenciés : l'**apporteur**, le **commercial**, l'**utilisateur (technicien côté Groupama)** et l'**administrateur**, avec des **droits liés à la validation** des étapes ou la visualisation de certaines informations, quand du côté de **TLB**, on a seulement les **techniciens**.

L'**apporteur** va *amener un client*, généralement suite à une demande de télésurveillance ou à une proposition de Groupama, ce client sera potentiellement déjà dans l'application où sera créé s'il est nouveau. Si cette dernière option est nécessaire, alors l'apporteur ajoutera le client en renseignant ses informations personnelles et l'équipement souhaité, dans l'autre cas, seul l'équipement est ajouté sur la fiche du client. Parmi ces équipements, nous avons la *vidéo protection*, l'*alarme intrusion*, le *contrôle d’accès*, la *vidéo Gari*, le *TAP* ou le *SAF Gari*, qui peuvent être installés en plusieurs exemplaires par client.

Une fois que l'équipement aura été défini, la première étape est proposée : la découverte client et c'est le **commercial** qui reprendra la main à ce moment (ce qui nécessite que toutes les informations client aient été bien saisies), à tout moment le processus d'installation peut être abandonné. Le commercial va donc pouvoir **organiser jusqu'à deux rendez-vous** client en saisissant leur date ainsi que leur durée avec une option pour ajouter une alerte Outlook. Le commercial pourra alors s'assurer que le **client est éligible** à l'installation de l'équipement, et si c'est le cas, il va pouvoir **valider l'étape**. Si tout se passe bien, l'équipement **passe en devis en cours** où le commercial se chargera de mettre en place le devis qu'il faudra faire signer par le client, après avoir indiqué la date de signature, le processus peut continuer vers l'installation de l'équipement.

Dans un premier temps, il était prévu que l'installation soit gérée depuis l'API de Praxedo où le technicien qui a installé l'équipement aura saisi la date d'installation et de facturation liées à cet équipement. Il sera explicité plus loin que cette solution n'a pas pu être retenue et par quoi elle a été remplacée. **Après cette installation** l'équipement devient **actif** et n'est plus censé être modifié, sauf si le client décide de **résilier** ou de faire une **demande de SAV**. Bien sûr, à tout moment de ce processus, **l'équipement peut être abandonné** pour des raisons différentes qui peuvent être indiquées à chaque étape.

Le **SAV** propose **trois niveaux différents**, du premier qui demande une intervention minimale à distance au troisième qui demande l'intervention d'un technicien et donc un rendez-vous avec une facturation. La mise en SAV est un état de l'équipement, au-delà d'étape du processus, qui ramène ces étapes en arrière selon la gravité de la demande de SAV. Et le processus reprend de la même manière que lors de la première installation, le SAV étant désactivé une fois que l'équipement est de nouveau actif.

---

La **connexion à l'application** doit se faire via **(+ADFS)** par (+SAML), donc par la session Windows des machines autorisées, contrairement aux projets précédents qui étaient basés sur une authentification classique par identifiant et mot de passe. Cependant, les techniciens de Groupama sont susceptibles de changer assez souvent, il faut donc une manière simple d'accéder à l'application sans devoir gérer des comptes liés à une personne particulière.

Ces utilisateurs ont des rôles différents, qui ont pu être détaillés précédemment comme le commercial et l'apporteur qui ont des droits ponctuels pour des étapes précises du processus. L'**administrateur** aura **tous les droits** sur l'application, comprenant l'édition des utilisateurs eux-mêmes ainsi que l'accès aux statistiques. L'**utilisateur standard**, qui représente le technicien de Groupama, **aura la plupart des droits** comprenant **l'édition de chaque étape du processus** d'un équipement.

Au début du projet, il y avait un processus par équipement, par la suite le processus sera gardé, mais en réduisant les étapes possibles après discussion avec le client Groupama. Les différentes étapes d'origine possibles sont : *nouveau client* (à la création d’un nouveau client), *non éligible* (produit n’est pas éligible), *attente devis* (client est éligible, mais la date du R1 n’est pas encore passée), *devis en cours* (l’éligibilité est validée), *devis refusé* (client refuse le devis), *à installer* (le devis est signé par le client), *installation en cours* (une date d’installation a été renseignée), *installé* (le PV d’installation a été chargé), *actif* (la facture a été faite), *SAV* (SAV est en cours), *abandon* (le client n’est plus intéressé avant la facturation), *résilié* (le client a résilié son contrat).

On peut observer ici que les **étapes** sont mêlées les unes aux autres comme la partie du **processus** d'installation et le processus abandonné, par la suite, ces deux processus vont être scindés en deux suivant la situation. Ainsi que le **SAV**, qui n'est pas une étape de processus, mais un **statut de l'équipement**, qui va ramener le processus en arrière selon la gravité de la demande de SAV.

C'est un **projet web d'environ 11 000 €** qui doit être réalisé en **trois mois**, avec plusieurs **itérations** et une **intégration du client** au projet, avec des versions de démonstration chaque mois afin de permettre au client d'essayer les fonctionnalités disponibles. Il sera réalisé en **Laravel** 8 et en **Vue.js** 2, avec une base de données MySQL. Cependant, le choix de ces technologies n'est pas anodin et dépend des projets précédents. Mais en-dehors de l'héritage technologique, il a fallu considérer les technologies à disposition de l'agence pour proposer la meilleure solution au client.

## Choix des technologies

Les technologies utilisées sont en droite lignée de l'héritage des projets précédents, d'abord **Groupama Extranet**[@groupama-extranet] créé par l'ancien (+LEADDEV) de Useweb puis **Groupama GBH**[@groupama-gbh] par moi-même, basé sur l'architecture technique du premier projet afin de réutiliser ce qui avait été développé sur un projet qui était proche sur les fonctionnalités. Ainsi le projet actuel pour Groupama TLB reprend la même architecture, à savoir **Laravel**[@laravel] v8.x basé sur **PHP**[@php] *v8.0* pour le (+BACKEND) et **Vue.js**[@vuejs] *v2.6* alimenté par **Buefy**[@buefy] (bibliothèque Vue.js *v2.x* pour Bulma) pour le (+FRONTEND). Cela étant possible, car l'aspect (+SSR) n'a pas d'utilité dans un extranet.

L'utilisation de ces technologies vient des **autres projets gérés** par l'équipe de Useweb, qui sont souvent basés sur celles-ci. L'idée est de pouvoir faire maintenir ces projets **par une autre personne** que celle qui les a développés. Par conséquent, le choix des technologies possible est limité à une liste bien précise.

Il aurait été possible de **migrer une partie des technologies** vers une version **plus récente**, ce qui aurait nécessité un temps de développement supplémentaire, ce qui n'a pas été accepté par le manager, Michel TRAUTH en charge du projet. Cela a eu pour conséquence de ne pas pouvoir utiliser les dernières versions de Laravel et de Vue.js. Par conséquent, le **projet a été débuté avec des versions dépréciées** des technologies choisies. Une seule mise à jour conséquente a été réalisée, par nécessité des dépendances utilisées, celle de passer à Laravel *v8.x* à Laravel *v9.x* assez facilement, les changements majeurs étant faciles à cibler et à maintenir.

Comme le (+LEADDEV) de Useweb est parti, toute évolution de l'architecture technique a été gelée, étant donné la frilosité de la hiérarchie à mettre des changements en place qui auraient pu augmenter le temps initial de développement.

S'il avait été possible de faire évoluer ces technologies, pour parvenir à un projet plus stable et plus facile à maintenir, cela aurait pu suivre le projet d'évolution détaillé dans la partie [*3.5 Des pistes d'améliorations*](#des-pistes-damelioration).

> Au-delà de l'aspect technique qui sera abordé plus tard en détails, le projet a d'abord connu **une phase de gestion** et d'analyse qui a permis d'apporter plus de détails depuis les **spécifications fonctionnelles** du cahier des charges aux spécifications techniques. Nous avons déjà eu un aperçu des enjeux liés à la mise en place de cette plateforme, mais tout cela n'a pas été sans quelques difficultés.

[^2]: Ce qui est lié au premier extranet développé par Useweb pour Groupama, l'extranet partenariat.
[^3]: Couvre l'assistance habitation, les véhicules, les services à domicile, l'assistance voyage...
