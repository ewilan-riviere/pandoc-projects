# Annexes

## Présentation de Useweb

En alternance au sein de l'agence web **Useweb**[@useweb] depuis **octobre 2019**, j'ai le rôle de développeuse web (+FULLSTACK). Mon entreprise travaille surtout sur la réalisation de sites web sur mesure pour des clients, en particulier des entreprises, allant de 3 000 € à 300 000 €. Le but de Useweb est de proposer un suivi précis des demandes des clients afin de leur proposer une solution répondant au mieux à leurs besoins avec la possibilité d'avoir un suivi sur du long terme. Il arrive aussi que nos clients demandent des développements supplémentaires après plusieurs mois si l'outil leur convient, mais que leurs besoins ont évolué. Nous suivons donc nos principaux client sur plusieurs années dans le domaine du numérique et la plupart des clients de Useweb ont leurs sites web hébergés sur nos serveurs.

Nous sommes donc une PME d'une dizaine de personnes rachetée par Michael HYOT en 2010 du conseil d'administration de Secob, entreprise de comptabilité. Nous proposons plusieurs prestations : du **design alliant l'UI à l'UX**, le **développement web**, du **référencement naturel** et du **référencement payant**, la **création de site web** et des **social Ads**.

Présenter Useweb, c'est d'abord présenter SECOB (fondée en 1963), une entreprise de comptabilité comptant environ 300 employés basée à Cesson-Sévigné. Et c'est dans le pôle technologique de SECOB que fut créée Useweb en 1999, elle va côtoyer deux autres entreprises en interne de SECOB : Naviso et Sogescot, éditrices de logiciels pour les comptables. La petite agence web va vivre quelques problèmes de rentabilité jusqu'à son rachat par Mickael HYOT, associé de SECOB, en 2012 où il y aura une refonte de la gestion de l'entreprise. Son activité principale reste encore aujourd'hui la création de site web pour les entreprises allant de budgets de 5 000 € à 300 000 €, par exemple le site web de Laforêt[^laforet] (2019) ou celui de Domitys[^domitys] (2022).

Côté valeurs, SECOB prône l'égalité femme/homme, la diversité dans le recrutement, le dialogue social et le respect de l'articulation vie professionnelle/personnelle, ces valeurs étant partagées par les entreprises en interne dont Useweb. Elle est dirigée par des associés dont Christophe MEREL, le président et Stéphanie Gomes, associée principale. Son capital social est élevé à plus de 400 000 €. Concernant son historique, en 1999, la création de Useweb, en 2001, la création de Naviso, en 2007, le rachat de cabinet à Nantes, Pornic, Montaigu, Orleans et St Grégoire, en 2012, rachat de Useweb (créé en 1999), en 2013, rachat de Lorient et Paris, en 2021, création des agences de Montpellier et St Nazaire.

![Organigramme de Secob](assets/images/secob.jpg)

Useweb continue de cohabiter avec Naviso et Sogescot dans le pôle technologique même si elle est en partie indépendante du fait de son rachat par un associé de SECOB. Bien qu'elle ait le status de PME avec sa dizaine d'employés, elle bénéficie des avantages de la SECOB ce qui la protège des éventuels problèmes qu'elle a pu rencontrer durant son existence. La présence de deux autres entreprises du monde numérique à ses côtés permet l'échange de connaissances et la migration en interne des employés selon leurs choix de carrière. Ses problèmes de rentabilité ont diminué depuis son rachat et elle dispose aujourd'hui d'un capital social de 100 000 €.

En plus des valeurs de SECOB, le pôle technologique a lancé une nouvelle politique en 2021, celle de la Responsabilité Sociale des Entreprises afin de repenser ses pratiques dans le numérique, pour notamment commencer à envisager de créer des sites éco-responsables. Actuellement l'organisation interne permet un jour de télétravail par semaine depuis la crise sanitaire du COVID-19.

![Organigramme de Useweb](assets/images/useweb-team.jpg)

> Ci-dessous, il est fait mention de Michel TRAUTH comme manager, celui-ci a été remplacé par Stéphane BLIVET très récemment.

L'équipe est constituée de plusieurs parties qui communiquent les unes avec les autres pour chaque projet.

La partie référencement portée par Loren LANDRU afin d'améliorer le score de référencement naturel des sites proposés par Useweb mais également de publier des articles sur le sujet, elle est secondée par Sinan SIRKET qui est traffic manager.

La partie design dirigée par Camille BONIFACE, ce qui permet de proposer des maquettes novatrices afin de gagner les appels d'offre, Adèle FOUGERES travaille surtout sur l'aspect UI/UX afin de proposer aux utilisateurs une interface intuitive qui permettra d'améliorer leur expérience.

La partie technique est dirigée par Michel TRAUTH qui répartit les tâches entre les différents profils selon les compétences et les disponibilités de chacun.

Laurent Chevalier, développeur senior, travaille généralement sur des projets plus anciens et effectue de la maintenance. C'est aujourd'hui mon tuteur en remplacement de Michel TRAUTH. Mathieu MONNIER a une double responsabilité : développeur (+FULLSTACK) et assistant superviseur d'équipe pour seconder Michel TRAUTH. Et l'équipe : Ewilan RIVIERE, moi-même, ayant pour poste développeuse (+FULLSTACK) en alternance, Ambre VANNEUVILLE qui est développeuse (+FRONTEND) junior, Thomas RETY qui est développeur (+FULLSTACK) en alternance.

Les appels d'offre sont gérés par Caroline LEVASSEUR en relation avec Camille BONIFACE et Michel TRAUTH pour constituer un dossier à la fois technique et de maquettage pour proposer une réponse aux clients.

Enfin la direction est assurée par Mickael HYOT ainsi que par Agnès HUMBERDOT, directrice adjointe, qui dirigent aussi le pôle technologique et donc Naviso et Sogescot. Maryse PASQUIER assure toute la partie gestion de la facturation et des communications internes.

![Projet lors d'un appel d'offres](assets/images/project.jpg)

Nous allons prendre ici la communication interne lors d'un appel d'offre. Lorsque l'appel gagné (avec l'alliance de la partie technique, design et de C. LEVASSEUR), cela mène à la réalisation d'une maquette finale en prenant soin de contacter régulièrement le client pour que celui-ci puisse valider la partie graphique. En parallèle, Michel TRAUTH va constituer le cahier des charges lors de plusieurs réunions avec le client, puis il rédigera des spécifications fonctionnelles légères et l'équipe de développement travaillera directement avec celles-ci sans passer par des spécifications techniques[^techniques]. Lorsque tout sera terminé côté design, l'équipe de développement pourra commencer et les tâches seront réparties selon la nature du projet et la disponibilité de chacun, à travers un outil de gestion des tâches, Redmine.

Dès que le projet aura atteint une phase stable, une application de recette sera déployée afin de permettre au client de voir l'évolution de son projet et de faire des retours[^retours]. Une fois tous les points des spécifications effectués et validés, une phase de déploiement[^déploiement] en production se fait en relation avec le client, notamment selon si l'hébergement se fait du côté de Useweb ou de son côté.

Enfin, si le client le souhaite, un compteur de TMA[^tma] est mis en place afin de réalisation des évolutions sur plusieurs mois suivant la mise en ligne de la première version. C'est là que la partie référencement met en place des solutions pour améliorer le score de l'application auprès des moteurs de recherche.

Il est à noter que la direction influe rarement sur les projets et leur évolution, en-dehors des projets internes comme par exemple la réalisation des sites web de Naviso ou de SECOB.

L'exemple généraliste ici illustre une situation où tout s'est bien passé. Cependant, la communication est l'un des points faibles de Useweb puisqu'il arrive fréquemment que des fonctionnalités soient vendues alors que l'équipe de développement ne l'apprend qu'au dernier moment et doit réagir très rapidement ou qu'une fonctionnalité soit développée d'une certaine manière pour récupérer du temps sur un projet alors que celle-ci ne correspondra pas exactement à la demande du client.

## Extrait du cahier des charges de TLB

### Exemple d'US

**US 1. Écran de connexion**

- *en tant que* : utilisateur habilité
- *je souhaite* : pouvoir me connecter avec ma session Windows
- *afin de* : accéder à mon espace

**Description** :  
En tant qu’utilisateur autorisé dans un des Groupes AD pour chaque rôle (ADMIN, Utilisateur, Commercial et Apporteur), je suis automatiquement reconnu grâce à ma session Windows.

**Critères d’acceptation** :  

1. Je suis connectée automatiquement via ma session
2. J’accède uniquement aux infos qui correspondent à mon rôle

## Maquette

### Tableau de bord

![Tableau de bord](assets/images/screenshot-01.jpg)

### Liste des clients

![Liste des clients](assets/images/screenshot-02.jpg)

### Création d'un équipement

![Création d'un équipement](assets/images/screenshot-03.jpg)

### Ajout d'un SAV

![Ajout d'un SAV](assets/images/screenshot-04.jpg)

### Équipement complet

![Équipement complet](assets/images/screenshot-05.jpg)

## Application

Voici plusieurs captures d'écran de l'application en recette, les données visibles ont été générées avec des bibliothèques de données fictives.

### Tableau de bord

![Tableau de bord](assets/images/screenshot-recette-03.jpg)

### Liste des clients

![Liste des clients](assets/images/screenshot-recette-04.jpg)

### Un équipement

![Liste des clients](assets/images/screenshot-recette-05.jpg)

### Liste des équipements

![Liste des équipements](assets/images/screenshot-recette-02.jpg)

### Statistiques

![Statistiques](assets/images/screenshot-recette-01.jpg)

## Sécurité FACIE {#securite-facie}

Dans le cadre d'une amélioration de la sécurité, Groupama a contacté l'agence Useweb pour mettre en place FACIE (Formulaire d'Analyse des Contrats de l'Informatique Externalisée). Celui-ci prend en compte plusieurs questions afin de déterminer le niveau de sécurité de l'entreprise.

### Contrat et labellisation

- Un contrat est-il déjà passé avec le prestataire ?
- Un plan de réversibilité de la prestation a-t-il été prévu dans le contrat ?
- Le prestataire a-t-il initié un Plan d'Assurance Sécurité (PAS) pour la prestation ?
- Le prestataire est-il certifié ou labellisé (ISO 27001, PDIS, PCIDSS…) ?

### Politique de sécurité

- Disposez-vous d'une politique de sécurité du système d'information ?
  - Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Avez-vous une cartographie des risques de sécurité du Système d'Information ?
  - Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Un RACI est-il défini et mis en pratique (rôles et responsabilité de la DG, des Métiers, du CISO, du Contrôle Interne, de la DSI, de l'Audit Interne…) ?

### Organisation de la sécurité de l'information

- Les sujets sécurité sont-ils adressés au cours d'une instance de sécurité dédiée
- Le RSSI de l'organisation est-il l’interlocuteur unique de GMA sur les sujets de sécurité du dernier échange et de décisions.

### Gestion des actifs

- Disposez-vous d'un inventaire de tous vos actifs ?
- Disposez-vous d'une politique de gestion des actifs
- Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Avez-vous des mesures de protection des actifs postes de travail comme l'EDR, DLP, durcissement du poste, etc.
- Avez-vous défini des mesures de protection des actifs serveurs comme des WAF, SIEM, DLP, etc.

### Sécurité liée aux ressources humaines

- Les utilisateurs (salariés et tiers) sont-ils sensibilisés au risque informatique
- Sécurité physique et environnementale
- L'accès aux lieux de stockage de l'information fait-il l'objet de protection contre les intrusions physiques ?
- Décrivez les moyens de protection utilisés
- Des protections physiques des ordinateurs,  des matériels portatifs ou mobiles et des médias amovibles ont-elles été implémentées - (en cas de vol, mise aux rebus) ?
- Décrivez les moyens de protection utilisés
- Des mesures sont-elles prises concernant le lieu de traitement des données
- Des mesures sont-elles prises concernant le lieu de stockage des données
- Le lieu de traitement des données est-il situé en France
- Le lieu de stockage des données est-il situé en France ?

### Gestion de l'exploitation et des télécoms

- Les documentations et les procédures d'exploitation ont-t-elles été définies (gestion des changements, capacité… etc) ?
- La séparation des environnements de production et hors production est-elle effective
- Avez-vous mis en place une protection continue contre les attaques informatiques, les codes malveillants et les intrusions - logiques ?
- Disposez-vous d'une Politique de patch management sur le périmètre de la prestation
- Disposez-vous de mesures de sécurisation des réseaux de transmission, des systèmes de terminaux utilisateurs, des systèmes de - communication, des systèmes de messagerie électronique, des accès internet ?
- Avez-vous mis en place des mesures de cloisonnement réseau interne et périmétrique ?
- Disposez-vous d'une Politique de sécurité qui traite de la sous-traitance
- Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Avez-vous défini des mesures pour gérer et contrôler les prestations de service réalisés par des tiers pour le compte de - l'organisation
- Disposez-vous d'une politique de sauvegarde
- Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Détaillez les grands principes de sauvegarde (fréquence, support…)
- Disposez-vous d'un bastion de sécurité pour l'administration sur le périmètre de la prestation
- Utilisez-vous des équipements de sécurité
- Avez-vous défini une politique de transfert, stockage et manipulation de l'information
- Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Avez-vous mis en place des mesures pour assurer la disponibilité et la redondance des données
- Avez-vous mis en place des mesures pour assurer la confidentialité des données (chiffrement, cloisonnement des données…)
- Des mesures pour assurer l'intégrité des données sont-elles mises en place (dispositifs techniques notamment)
- Avez-vous défini des mesures pour assurer la traçabilité des actions et opérations réalisées dans le cadre de la prestation
- Des dispositifs pour un traitement des données sont-ils mis en place (registre des activités de traitement, sous-traitance, durée de - conservation…)
- L'ensemble du parc des postes de travail dispose-t-il d'une solution antivirale et d'une base de signature récente ?
- Des mesures de durcissement du socle sont-elles définies et appliquées ?
- Un processus de gestion des vulnérabilités est-il mis en place (identification des vulnérabilités, recherche et déploiement de - correctifs…) ?
- Les patchs fournis par les éditeurs sont-ils qualifiés et déployés sur tous les éléments du parc
- Contrôle d'accès aux applications et à l'information
- Avez-vous mis en place une politique de gestion des habilitations
- Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- La bonne application de cette politique de gestion des habilitations est-elle contrôlée ?
- Acquisition, développement et maintenance des systèmes d'information
- Des procédures permettant d'identifier et de prendre en compte les exigences de sécurité d'un projet informatiques sur tout son - cycle de vie ont-elles définies et déployées
- Le code est-il revu à cette occasion ?
- Avez-vous défini des exigences de sécurité dans le cadre du cycle de développement des SI
- Des procédures de contrôle et de revue technique des applications sont-elles réalisées
- Des tests de sécurité et de conformité du système sont-ils réalisés ?
- Leurs résultats font-ils l'objet d'un suivi ?
- De quand datent les derniers tests ?
- Sur quel périmètre ont-ils été réalisés ?
- Quels sont les axes d'amélioration identifiés à la suite de ces tests ?
- Disposez-vous d'une politique de développement sécurisé des logiciels et des systèmes
- Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Suivez-vous des normes de sécurité spécifiques pour les développements
- Avez-vous mis en en place des mesures pour assurer la protection des environnements de test
- Gestion des incidents liés à la sécurité de l'informatique
- Disposez-vous d'une procédure d'alerte et de gestion des incidents (notification, classification…)
- Les incidents et failles de sécurité font-ils l'objet d'un suivi et d'un reporting ?
- Aspects de la sécurité de l'information dans la gestion de la continuité d'activité
- Disposez-vous d'un Plan de Continuité d'Activité (PCA) avec un volet IT et d'un Plan de Reprise d'Activité (PRA)
- Disposez-vous d'un Plan de Secours Informatique (PSI)
- Ces plans sont-ils testés régulièrement ?

### Conformité et Audit

- Effectuez-vous régulièrement des audits (internes et externes) organisationnels et techniques de la Sécurité des Systèmes - d'Information (y compris des tests d'intrusion) ?
- De quand date le dernier audit (interne et externe) sur la Sécurité des systèmes d'Information ?
- À la suite de cet audit, des recommandations en termes de sécurité ont-elles été émises ?
- Utilisez-vous des sous-traitants pour effectuer la prestation pour GMA ?
- Effectuez-vous des audits de sécurité chez vos sous-traitants
- A-t-il émis des recommandations en termes de sécurité ?
- Disposez-vous d'une politique de conformité
- Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Disposez-vous d'une politique interne de protection des données personnelles
- Fournir la copie de la page de mise à jour du document ainsi que les pages de sommaire.
- Des audits portant sur le RGPD (internes et externes, audit spécifique ou dans le cadre d'un audit plus large) ont-ils été réalisés ?
- De quand date le dernier audit ?
- A-t-il émis des recommandations en termes de sécurité
- Le transfert des données du client hors Union européenne est-il interdit sans autorisation préalable écrite du client ?
- Disposez-vous une procédure détaillée en cas de violation des données personnelles dans le cadre de la prestation
- Des dispositifs de purge des données sont-ils prévus ?

[^laforet]: https://www.laforet.com
[^domitys]: https://www.domitys.fr
[^techniques]: cette situation est habituelle et est causée par la pression temporelle, chaque projet étant à finir au plus vite. Cela n'est pas sans causer quelques complications, notamment si le client revient sur certains points qui ont été interprétés côté équipe de développement parce qu'ils étaient flous dans le cahier des charges.
[^retours]: généralement, à ce moment le client peut revenir sur certains points précis qui ont pu être interprétés pour indiquer plus précisément ce qui était souhaité. Si c'est indiqué suffisamment en amont, il est souvent assez facile de modifier certains axes du développement pour correspondre aux besoins du client, même si cela peut générer un léger retard.
[^déploiement]: il arrive que le client commence à réagir qu'au moment du déploiement et trouve nombre de points qui lui pose problème, ce qui génère un retard conséquent et des frictions parfois assez intenses en interne comme pendant les réunions client.
[^tma]: il faut bien définir ce qui est une *évolution* de ce qui est une *anomalie*, ce qui est loin d'être simple, en particulier lorsque les spécifications sont floues et non validées.,
