# Remerciements {.unnumbered}

*Je remercie toutes les personnes qui ont permis l'aboutissement de ce projet et de mon alternance à Useweb avec l'ENI Ecole pendant ces trois années.*

*Je souhaiterais d'abord remercier Adrien BEAUDOUIN, le développeur en chef sur tous les projets liés à Groupama, qui m'a épaulée durant le projet précédent à celui-ci. Il n'a pas pu m'aider sur le projet présent, étant donné qu'il a décidé de quitter l'entreprise, mais il m'a été d'un grand secours face aux difficultés que j'ai pu rencontrer sur l'architecture qu'il avait créée pour répondre aux besoins du client (et qui a été reprise pour ce projet).*

*Je remercie également toutes les personnes qui m'ont accompagnée pendant ces trois années :*

*Mes tuteurs, Michel TRAUTH dans un premier temps et Stéphane BLIVET depuis quelques mois. Ils m'ont permis de mieux comprendre les différents aspects des projets sur lesquels j'ai pu travailler et m'ont aidée à mieux travailler dans le cadre de mon alternance.*

*Mais aussi Michael HYOT, dirigeant de Useweb pour ses mots d'encouragement et pour m'avoir accordé sa confiance et ainsi qu'une large indépendance dans l’exécution de missions valorisantes.*

*Adeline PENGUILLY, chef de projet chez Groupama, avec qui j'ai pu échanger de nombreuses fois pour mieux comprendre les différents aspects de l'application, toujours patiente et sympathique.*

*Tout comme l'équipe pédagogique de l'ENI Ecole, dont Élodie PIOC, responsable relation entreprise, pour son accompagnement durant ces trois années, elle a toujours été présente en cas de difficultés et Sylvain TROPÉE, responsable pédagogique, qui a soutenu activement toute ma promotion dans le but que cette dernière année se passe au mieux.*

*Et enfin, Nicolas DUMEZ, intervenant à l'ENI Ecole pour le mémoire, ses conseils de rédaction ont été très précieux.*
