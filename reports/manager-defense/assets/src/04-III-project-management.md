# Gestion du projet

C'est au travers de cet **héritage technique** que le projet a été mis en place. Le **cahier des charges** a été produit **par le client** le 23/06/2021, c'est **Adeline PENGUILLY** qui est Chef de projet / Product Owner chez Groupama Loire-Bretagne qui s'en est chargée. Cela a été le cas à chacun des projets que Useweb a eu avec ce client, où les **demandes** ont toujours été **très précises** et découpées en **différentes (+US)** (qui correspondent aux spécifications fonctionnelles). Cela afin de gérer le projet de manière agile avec différentes **itérations** où des groupes d'(+US) sont mises en **recette** pour que le client puisse vérifier que tout est conforme à ce qu'il souhaitait.

Ces mises en recette font l'objet de **réunions régulières** où une démonstration est réalisée, ce qui permet au client de voir l'utilisation des différentes fonctionnalités qu'il souhaitait et de poser ses questions si besoin. C'est donc un processus en **agilité** où toute demande est intégrée de fait au projet pour permettre une prise en charge rapide. Cela dans le but d'offrir une application **la plus qualitative** possible au client et de **l'intégrer au processus de développement**.

De plus, Useweb propose une **plateforme d'échange**, basée sur **Redmine**[@redmine], qui permet de **créer des tickets** correspondant d'abord aux différentes (+US) présentées dans le cahier des charges, afin de proposer une **cotation**, d'avoir un **suivi du temps passé**, d'une **date limite** et par qui le ticket est traité. Ainsi, cela donne une **vision globale** de l'avancée du projet et de voir facilement le moindre retard. Dans un but d'intégration au projet, le **client a un accès à ces tickets** et peut donc facilement suivre les avancées, et même **commenter** chaque ticket dans lors de la recette ou en cas de bug dans l'application (cas dans lequel, il pourra créer un nouveau ticket). Ces tickets peuvent être assignés à des personnes précises, que ce soit le client ou Useweb afin de générer une notification indiquant à la personne concernée que le ticket est en attente.

> C'est grâce à cette gestion granulaire des fonctionnalités qu'il est possible de suivre facilement les activités sur le projet et donc de réagir au plus vite.

Le cahier des charges a donc été analysé du côté de Useweb afin de détailler chaque (+US) pour en extraire les informations afin de construire les **spécifications techniques** qui lui sont liées et ainsi d'établir une **cotation** à chaque ticket. Cette cotation est basée sur l'expérience des personnes qui la font afin de trouver la proposition la plus juste pour établir un rétroplanning. Habituellement, celle-ci est réalisée par la ou les personnes qui vont développer ainsi que, au minimum, un (+LEADDEV) qui saura prévenir en cas de demande plus lourde qu'il n'y parait.

Chaque (+US) se présente donc sous la forme suivante : un tableau présentant la situation avec *en tant que*, *je souhaite*, *afin de*, une *description* pour donner des informations supplémentaires et les *critères d’acceptation* qui sont des conditions à remplir pour que la (+US) soit considérée comme terminée. C'est ceux-ci qui permettent de vérifier que l'(+US) est bien terminée et qu'elle répond aux **attentes du client**. C'est donc un élément très important pour la **qualité du projet**. La plupart des (+US) sont découpées suffisamment finement pour ne pas avoir à la subdiviser, cependant si cela s'avère nécessaire l'(+US) est découpée en plusieurs tickets. Bien que le cahier des charges du projet ne puisse être adjoint ici, certaines parties ont reçu l'autorisation de pouvoir être publiées partiellement dans le présent dossier, ainsi une (+US) type peut être retrouvée en annexe.

Par les (+US), il y a donc la **connexion** (avec différents rôles), la **gestion des clients** (liste, création, détails, mise à jour ou la résiliation), l'**ajout d'équipements** liés au client (avec le processus, la mise à jour, le SAV, la résiliation), la **mise en place de devis**, les **statistiques** et les **emails** selon les situations que rencontrent les utilisateurs. La plupart de ces demandes ne posent pas de problème particulier, la plus complexe étant la gestion du processus lié aux équipements qui comporte plusieurs états. Les problèmes sont apparus, car l'analyse du cahier des charges n'a pas été suffisamment fine et que certains éléments se contredisaient de manière subtile.

L'**analyse et la cotation ont été effectuées par une seule personne**, moi-même, car le (+LEADDEV) de Useweb avait quitté l'agence et que l'autre développeur était trop occupé pour proposer son avis. Si l'expérience qui était la mienne à ce moment a permis une **cotation correcte sur la plupart des (+US)**, il en a été certaines qui ont été **sous-estimées grandement en termes de complexité**. Comme l'agence était habituée aux cahiers des charges de Groupama et que ceux-ci ne comportaient pas de problèmes particuliers, l'analyse n'a pas été suffisamment poussée. Il y a notamment eu des points purement fonctionnels qui ne pouvaient pas être développés, car contradictoires.

## Synthèse des US

Les (+US) suivantes sont tirées du cahier des charges et ont été retouchée pour être publiées ici. Elles sont donc un peu différentes de celles présentes dans le cahier des charges.

Les rôles sont les suivants : **administrateur** (tous les droits sur la plateforme), **utilisateur** (technicien de Groupama, ayant plusieurs droits comme l'édition et la création), **commercial** (intervient sur certaines étapes d'un équipement), l'**apporteur** (intervient par rapport à un client).

> Certains commentaires sont disponibles sur quelques (+US) qui ne viennent pas du cahier des charges, mais qui sont une analyse rapide a posteriori qui sera détaillée plus après.

### Connexion

**US 1. Écran de connexion**

Quand un utilisateur autorisé dans un des Groupes AD pour chaque rôle (administrateur, utilisateur (technicien), commercial et apporteur), il est automatiquement reconnu grâce à sa session Windows.

**US 2. Page d’accueil**

Lorsque l’utilisateur se connecte, il arrive sur la page d’accueil avec les infos suivantes : son prénom, le nombre total de clients avec une répartition par étapes (l'apporteur a une vision que de ses clients uniquement, les autres utilisateurs voient tous les clients). Un formulaire pour créer un nouveau client est disponible, sauf pour le commercial. Le menu est constitué des rubriques suivantes : accueil, les clients, les utilisateurs (uniquement pour l’administrateur), paramètres, statistiques (uniquement pour l’administrateur).

### Clients

**US 3. Lister les clients**

Lorsque les utilisateurs sont sur la page de liste des clients, ils voient la liste de tous les clients. Le tableau se compose des colonnes suivantes : *date de création*, *n° client*, *numéro TR*, *nom - prénom du client*, *équipement*, *téléphone*, *email*, *CP Ville*, *département*, *étape* et le *bouton d’action* « Loupe ». En tant qu'apporteur, la liste des clients est uniquement les clients qui lui sont liés. Une pagination sera prévue avec la possibilité de modifier le nombre de clients affiché par défaut 20 par page.

> Ici la colonne *équipement* et *étape* sont impossibles, car un client peut avoir plusieurs équipements et l'étape est uniquement lié à l'équipement, pas au client. Par conséquent, il a été nécessaire de rajouter une page de liste d'équipements et un raccourci sur la fiche de chaque client vers les équipements qu'il possède.

**US 4. Créer un client**

Un client est défini par plusieurs informations : *civilité* (Madame ou Monsieur), *CSP* (Particulier/Professionnel/Collectivité/Agricole), *raison sociale* (saisie libre si la CSP n’est pas Particulier), *nom* (saisie libre toujours en majuscule), *prénom* (saisie libre seule la 1ère lettre en Majuscule), *adresse* (saisie libre), *code postal Ville* (champ numérique, 5 chiffres avec autocomplétion depuis l'API du gouvernement), *téléphone* (champ numérique), *email* (champ type mail), *IDGRC* (champ numérique, 5 chiffres), *produit* (vidéo production/alarme intrusion/contrôle d’accès/vidéo Gari/TAP/SAF Gari).

**US 5. Visualiser la fiche d’un client**

Sur un client, il est possible de voir sa fiche avec les différentes informations en fonction de son étape. Dès qu’une étape est validée, les administrateurs et les utilisateurs peuvent venir la modifier. Pour les apporteurs et les commerciaux, ils peuvent juste consulter une fiche, mais ne peuvent pas la modifier.

> Ici, l'*étape* semble liée à l'utilisateur, or l'étape concerne uniquement un équipement.

**US 6. Processus d’un client**

En consultant le détail d’un client, il est possible voir les différentes étapes avec la date de chaque changement. Il y a un processus par équipement, mais un client peut avoir plusieurs équipements.

Les différentes étapes possibles sont : *nouveau client* (création d’un nouveau client), *non éligible* (produit n’est pas éligible), *attente devis* (le client est éligible, mais la date du R1 n’est pas encore passée), *devis en cours* (lorsque l’éligibilité est valide), *devis refusé* (le client refuse le devis), *à installer* (devis signé par le client), *installation en cours* (lorsqu’une date d’installation a été renseignée), *installé* (le PV d’installation a été chargé), *actif* (la facture a été faite), *SAV* (un SAV est en cours), *abandon* (lorsque le client n’est plus intéressé avant la facturation), *résilié* (le client a résilié son contrat).

> Il s'agit effectivement d'un processus par équipement, mais les termes utilisés peuvent parfois être flous. Les étapes sont également mêlées les unes aux autres.

**US 7. Ajouter un équipement**

À tout moment, il est possible d'ajouter un équipement. Une liste déroulante permet ainsi de venir choisir parmi les équipements déjà listés. On peut ajouter deux mêmes types d’équipements.

**US 8. Découverte client**

Lorsqu’un client a l'étape « Nouveau », la partie Découverte client peut être complétée. Liste des champs : *éligible* (avec une liste déroulante avec autocomplétée des utilisateurs avec un profil commercial), *R1*/*R2* qui sont des champs date.

Un bouton « Annuler » annule la saisie et renvoie vers la liste des clients. Un bouton « Valider » enregistre les informations et si l’éligibilité est à OUI à l'étape passe à « Devis en cours » et ouvre la possibilité d’ajouter le RDV dans le calendrier Outlook. Si l’éligibilité est à NON, l'étape passe à « Non éligible »

**US 9. Devis**

Lorsqu’un client a l'étape « Devis en cours », la partie Devis peut être complétée. Liste des champs : *accepté*, si oui, *date de signature* (champs date avec par défaut la date du jour).

Un bouton « Annuler » annule la saisie et renvoie vers la liste des clients sans changer l'étape. Un bouton « Valider » enregistre les informations et si le devis est accepté l'étape passe à « À installer ». Si le devis n’est pas accepté, l'étape passe à « Devis refusé ».

**US 10. Installation**

Lorsqu’un client a l'étape « À installer », la partie Installation peut être complétée. Liste des champs : *prévu le* (champs date), *n° TR* (met par défaut un numéro TR disponible dans les paramètres).

Un bouton « Annuler » annule la saisie et renvoie vers la liste des clients sans changer l'étape. Un bouton « Valider » enregistre les informations et l'étape passe à « Installation en cours ». L’API Praxedo permet de passer de l'étape « Installation en cours » à « Installer ».

> Ici, l'API Praxedo devait fournir les informations concernant l'installation, il sera détaillé plus loin que ce n'est pas le cas.

**US 11. Facturation**

Lorsqu’un client a l'étape « Actif », la partie Facturation remonte de Sage via l’API Praxedo.

> Ces informations sont bien sur Sage, logiciel de facturation utilisé par Groupama, mais il n'est pas possible d'y accéder depuis l'API Praxedo.

**US 12. Exporter les clients**

Lorsque les utilisateurs sont sur la page “Les clients”, il est possible d'exporter la liste en conservant les filtres s’il y en a.

**US 13. Importer des clients**

Lorsque l’administrateur est sur la page “Les clients”, il est possible d'importer un fichier Excel afin de pouvoir rajouter les clients déjà existants

**US 14. Créer un SAV**

Le bouton « SAV » sur la fiche d’un client avec l'étape Actif, il est possible d'accéder à la fiche de renseignement avec les champs suivant pour chaque site.

- Si c’est un niveau 1 : un champ *commentaire* de type saisie libre s’affiche avec un bouton pour le valider ou un bouton pour l’annuler. L'étape du client reste Actif.
- Si c’est un niveau 2 : une *description*, une *date d’intervention*. L'étape du client passe en SAV et À planifier.
- Si c’est un niveau 3 : une *description*, une *date de RDV*, *nom du conseiller*. L'étape du client passe en SAV et Attente devis.

> Ici, l'étape du client est à nouveau soulignée alors qu'il s'agit de l'étape de l'équipement en réalité.

**US 15. Suivre un SAV**

Sur le détail d’un client ayant déjà eu un statut SAV ou SAV en cours, on voit la liste de ces interventions.

**US 16. Résilier un client**

Sur la fiche détail du client, il est possible charger une lettre de résiliation depuis la machine locale. Une fois chargée, il est possible de l'ouvrir ou la supprimer. Lorsque la lettre est chargée, l'étape passe à « Résilié ».

### Statistiques  

**US 17. Voir les statistiques**

Lorsque l’administrateur accède à ses statistiques, il voit sur son tableau de bord le nombre de clients global. Il peut filtrer soit sur une période en choisissant : *Année en cours*, *année dernière*, *Mois en cours*, *6 derniers mois* ou en sélectionnant une *date de début* et une *date de fin*, par *CR*.

Avoir le nombre de souscriptions par CSP, le taux de transfo par conseiller, le taux de transfo par CSP, le taux de transfo par apporteur. Le taux de type d’équipement, Le CA par commercial.

> Il n'y a pas de maquette fournie ici, la description est assez floue et nécessitera de nombreuses questions. En plus de la demande de filtre par *CR* (caisse régionale) qui existait sur les applications précédentes alors que ce n'est pas le cas ici.

### Email  

**EMAIL 1. Création de compte**

Lorsqu’un utilisateur, crée un nouveau compte utilisateur (manuel ou via import) le nouvel utilisateur reçoit un mail pour lui dire de créer son mot de passe.

> Inutile dans le cas de la connexion avec l'(+ADFS).

**EMAIL 2. Mot de passe oublié**

Lorsqu’un utilisateur clic sur le bouton « mot de passe oublié ? », il reçoit un mail pour lui dire de réinitialiser son mot de passe.

> Inutile dans le cas de la connexion avec l'(+ADFS).

**EMAIL 3. Nouveau client**

Lorsqu’un apporteur créé un client, Un mail est envoyé pour l’informer de la nouvelle création.

**EMAIL 4. Abandon**

Lorsqu’un client passe en Abandon, l'apporteur reçoit un mail pour l’informer.

**EMAIL 5. Installer**

Lorsqu’un client passe en étape Installer, l'apporteur reçoit un mail pour l’informer.

### Maquette

La maquette a été réalisée sur Adobe XD par le client sur la base graphique des projets précédents, afin de conserver une cohérence visuelle. Une sélection des visuels proposés est disponible en annexes et quelques visuels de l'application en recette sont également disponibles.

## Cohérence et interprétations

On peut ainsi citer, provenant de l'**(+US) 6. Processus d’un client** (lié l'(+US) 14 et l'(+US) 15), l'étape *SAV* d'un équipement qui s'est révélé être un **état impossible** puisque que la mise en SAV faisait basculer l'équipement dans une étape antérieure précise. Et une étape **SAV** ne pouvait pas se chevaucher avec une **étape** puisqu'une seule étape est possible à la fois, il s'agit simplement d'un **statut** global à l'équipement qui ne fait pas partie du processus, mais qui l'influence. Ainsi un équipement peut **entrer** ou **sortir** du SAV, mais c'est un état qui dure dans le temps.

Mais aussi, dans la même (+US), le fait que le **processus** soit lié **au client** et non à l'équipement. Ce qui est un **non-sens** dans l'application, **c'est l'équipement qui est concerné** par le processus et ses différentes étapes. Cela a mené à de nombreuses **contradictions** dans d'autres (+US) et aussi avec le client. Certains **emails** comme *EMAIL 1. Création de compte*, *EMAIL 2. Mot de passe oublié* sont **inutiles**, car la connexion se fera par (+ADFS) depuis la session Windows d'une machine. Ou encore le fait que **le client semble relié à un et un seul équipement** en plusieurs endroits du cahier des charges alors qu'en réalité il en a plusieurs. Pour finir, les **statistiques** qui sont demandées sont **très vagues** et avec des demandes qui concernent des aspects d'applications antérieures par exemple la gestion de *CR* (comprendre Caisse Régionale, car Groupama dispose de différentes caisses dans toute la France) qui n'existe pas ici, mais qui a été développé dans des projets précédents.

Ce sont ici de légers détails, mais qui, **accumulés**, ont créé des **problèmes** lors du développement puisqu'il a fallu avoir une **validation du client** concernant les points contradictoires. Et c'est également un des points de frictions, **notre référente** étant Adeline PENGUILLY, ce n'est que l'**intermédiaire** entre les **véritables clients en interne** et l'agence. Par conséquent, c'est ce qui a amené à ces **erreurs** dans **le cahier des charges**, car elle n'était **pas directement concernée** par l'application et que, contrairement aux projets précédents, elle ne serait pas amenée à l'utiliser. **Toute validation devait passer par elle-même** puis par **les personnes concernées** avant de faire remonter l'information jusqu'à Useweb, ce qui pouvait prendre une semaine ou plus. Cela a donc créé des **retards dans le développement** et des problèmes de compréhension entre les différents acteurs.

Les modifications successives ont donc été apportées au fur et à mesure des retours du client, ce qui n'a pas été sans créer des **problèmes de cohérence** entre ce qui a été validé d'un côté et ce qui sera impacté par cette validation. Pour l'agence, le développement de l'application ne pouvant être décalé pour des raisons de planning interne, il a fallu développer avec des relations entre entités qui n'étaient **pas stables** et **pas cohérentes**, avant d'attendre la validation du client pour pouvoir établir une base plus stable et donc revenir en arrière sur certains aspects.

> C'est à travers le cahier des charges qu'il a été possible d'analyser les enjeux liés à ce projet, mais cela aurait nécessité des approfondissements plus poussés pour pouvoir en tirer des conclusions plus précises.

Mais le plus impactant a été la mise en place de deux sujets différents : la **communication avec l'API Praxedo[@praxedo]** et la **connexion par (+ADFS)** via (+SAML).

### Des services externes

#### Praxedo

La communication avec une API externe ne représente **pas de difficulté majeure sur le papier**. Le cahier des charges détaillait la demande de connexion avec l'API Praxedo dans l'(+US) 10 où il est question de l'**installation d'un équipement**. L'étape de processus est influencé de cette manière "*L’API Praxedo permet de passer de l'étape « Installation en cours » à « Installer »*". L'idée est que lorsque **le technicien a installé l'équipement**, il l'indique sur Praxedo et **cela change l'étape de l'équipement** dans l'application. Mais cela n'a pas été aussi simple.

C'est après **une semaine de développement** très complexe infructueux qu'il a été établi qu'il y avait bien un problème avec cette API. Et c'est environ **deux mois plus tard** qu'il a été possible de consulter le référent technique de chez Groupama qui a donné des informations qu'il aurait été intéressant de connaitre en amont. La première étant que **le compte fourni ne permettait pas d'utiliser l'API** de Praxedo mais uniquement la documentation. La seconde était que **les informations qui devaient se trouver sur cette API n'y étaient simplement pas**, ce que l'agence ne pouvait pas savoir. Tout développement lié à cette API n'avait donc aucune utilité concernant l'application.

Par conséquent, la solution a été de **permettre le changement d'étape** d'équipement sur l'application et le **report d'informations liées à la facturation** qui auraient dû venir de l'API. Ces informations sont bien disponibles, mais sur un autre logiciel, **Sage**[@sage] qui, d'après le référent technique de Groupama, nécessiterait des aménagements qui seraient "*conséquents*" et qui ne valent pas le temps de mise en place pour l'intérêt.

#### ADFS

L'autre aspect qui a demandé beaucoup de temps, a été la **mise en place de l'(+ADFS)** via le (+SAML) qui consiste à permettre la connexion à l'application non plus via un identifiant et un mot de passe, mais uniquement depuis **une session Windows** afin de simplifier le processus du côté de Groupama. Il est utile de préciser que les projets précédents n'utilisaient pas cette méthode de connexion, ce qui nécessitait de la développer.

La première étape a été de se renseigner sur le sujet puis d'essayer de **contacter les responsables de la (+DSI)** de Groupama afin de comprendre ce qu'ils souhaitaient avoir à disposition. Cela n'a pas été sans quelques complications, le premier rendez-vous avec la (+DSI) ayant été expéditif où l'interlocuteur a simplement indiqué qu'il fallait mettre en place la connexion (+SAML) avant de nous envoyer un fichier XML comportant des informations qu'il fallait utiliser, le tout sans **aucun référent technique en (+SAML)** à l'agence.

Cela a donc représenté plusieurs jours de développement afin de **comprendre la manière dont le (+SAML)** fonctionnait, **sans pouvoir tester sur les machines des utilisateurs concernés** et sans informations précises sur ce qu'il fallait mettre en place. Ce fut après plusieurs emails qu'Adeline PENGUILLY a demandé à la (+DSI) de fournir à l'agence des documents accompagnés **de demandes plus précises** que ce que Useweb avait reçu au premier rendez-vous, notamment grâce à la documentation de Microsoft. Cela a permis de mettre en place la connexion (+SAML) et de la tester sur les machines des utilisateurs concernés. Cependant, il a fallu plusieurs jours de développement supplémentaires pour corriger les erreurs qui se sont produites lors de la mise en place de cette connexion.

> Les détails techniques sont disponibles dans *4.2 Fonctionnalités principales*.

Le client n'a donc pas été sans poser quelques problèmes, néanmoins la structure de Useweb y avait également une part de responsabilité.

## Entre client et gestion interne

Le **cahier des charges n'a donc pas été remis en question** lors de la première analyse qui a été faite conjointement par Michel TRAUTH, manager au début du projet, et moi-même, développeuse sur le projet. Les **problèmes de cohérence n'ont été détectés que lorsque le développement a commencé**. Cela n'aurait pas été un problème spécifique **si le client était joignable facilement**, cependant notre seule interlocutrice était Adeline PENGUILLY, qui n'était **pas la cliente finale**, mais l'intermédiaire. Elle avait une connaissance projet théorique, mais pas technique. C'est cela qui a généré les différents **problèmes de compréhension** qui ont ponctué la gestion de projet, car, visiblement, le cahier des charges n'a pas été consulté par le client technique en interne. Pour **toute question**, elle devait consulter les concernés, ce qui prenait du temps à chaque demande, d'**une à deux semaines** alors que le planning de l'agence comprenait nombre de projets en cours.

**Plusieurs sujets ont été mis de côté au début**, comme l'(+ADFS), ou débutés avec des informations partielles comme la connexion à Praxedo, et dans les deux cas, cela a créé des problèmes notables. Ce n'est pas par manque de demande la part de Useweb, mais un **manque de coordination globale**, lorsqu'il était possible d'accéder à un élément technique, celui-ci était **partiel** et **hors contexte** selon les différentes sources. Il aurait été possible de mieux gérer ces informations parfois contradictoires avec un **(+LEADDEV)** ayant l'expérience de la gestion de projet et du client, mais celui-ci a décidé de quitter l'entreprise peu avant. En ajoutant à cela, qu'il n'y avait qu'une seule personne chargée du développement sur le projet et pas de (+CODEREVIEW), qui consiste à revoir le code avec un autre développeur, cela a créé un **effet tunnel[^4]** assez néfaste. Un **manque de recul** qui n'a fait qu'augmenter avec le départ de Michel TRAUTH, qui était chargé de la **communication** avec le **client** et du management sur le projet. Il a été remplacé tardivement par Stéphane BLIVET, qui a dû prendre le projet en cours de route. Et entre-temps, il a été **difficile de gérer le projet**, la communication client, les problèmes techniques et le planning des autres projets.

Les **réunions** avec le **client** n'étaient **pas assez régulières** et quand il y en avait, certaines questions techniques ne pouvaient pas être réglées par Adeline PENGUILLY. Mais il est important de noter que les **projets antérieurs** venaient **parasiter** ces temps dédiés car il y a eu des évolutions à réaliser rapidement sur ces projets, au détriment de TLB. Et Useweb avait également des problèmes sur des projets où il a fallu **intervenir en urgence**, ce qui a mis de côté TLB pendant quelques semaines. La **demande originale de mise en production** en juin 2022 **n'a pas pu être tenue**, c'est en Octobre 2022 qu'une version stable a pu être proposée.

> Si les enjeux restent bien présents, les contraintes se font sentir de plus en plus.

Il est à considérer certains aspects qui auraient pu mieux se passer ou des pistes à améliorer afin de rendre le projet plus solide et maintenable.

## Des pistes d'amélioration {#des-pistes-damelioration}

Il s'agit de considérations après avoir développé le projet et avoir assisté à certains problèmes inhérents aux décisions historiques liées aux projets précédents et au client.

### Une gestion de projet plus structurée

La **gestion de projet a été fluctuante** et a été influencée par les **évènements** qui se sont produits à l'**agence**, entre le changement de manager, l'absence de (+LEADDEV), un client parfois difficile à joindre, un cahier des charges incohérent... Il aurait été possible de limiter grandement le **retard accumulé** sur le projet et de rendre cette **gestion moins stressante** en changeant quelques points dont une analyse plus approfondie du cahier des charges. Mais une partie des problèmes était inhérente à l'agence avec des départs et des arrivées de différentes personnes liées au projet d'une façon ou d'une autre, ce qui n'a pas été sans l'impacter.

Les **outils utilisés** n'étaient peut-être pas aussi adaptés au projet qu'il aurait fallu, il serait intéressant de considérer l'utilisation d'outils comme **Airtable**[@airtable] ou **Jira**[@jira] pour améliorer la gestion de projet et les interventions du client dans le processus.

Le **manque d'accès au client** final et l'obligation de passer par une **personne intermédiaire** a sans doute représenté le plus gros problème, nombre de questions ont reçu une réponse trop tardivement ou une réponse qui n'était pas la bonne, ce qui a réellement **impacté négativement le projet**.

### Évolution technique

Tout projet finit par avoir une **dette technique**, plus on laisse les dépendances d'un projet en l'état, plus celle-ci s'accumule. Mais avec le projet TLB, la dette technique était déjà présente à l'origine même. Les technologies choisies ont été celles des projets précédents pour **gagner du temps** et éviter de développer à nouveau ce qui existait déjà, et c'est une manière de procéder qui est habituelle dans les développements de ce type. Le problème a été de choisir de **ne pas faire évoluer les technologies** et de surtout garder exactement les mêmes dépendances, et cela, même si celles-ci sont obsolètes et ont des failles de sécurité.

Le changement de version de *PHP*[@php] de la *v8.0* à la *v8.1* aura permis d'utiliser les **énumérations** natives, mais le **serveur de déploiement** ne permettait pas de le faire, étant trop ancien. Le passage de *Vue.js v2.x* à *Vue.js v3.x* aurait permis une **maintenabilité** sur le long terme, mais le kit d'interface *Buefy* qui est utilisé sur le projet ne permettait pas ce changement, il faudrait pour cela utiliser *Oruga*, le kit d'interface de *Buefy* pour Vue.js *v3.x*. Une solution de compromis aurait été de passer à *Vue.js v2.7* avec l'utilisation de Vite.js.

Une autre possibilité aurait été de passer à *Inertia* en gardant *Vue.js v2.x* ou en passant à la version actuelle, avec un (+FRONTEND) **directement embarqué** dans le (+BACKEND) **sans** l'utilisation d'une **(+API)**. *Livewire* peut également proposer une solution intéressante, avec la possibilité de placer beaucoup de composants dans une **bibliothèque externe** et donc d'avoir une maintenabilité améliorée.

Pour les inconvénients, il s'agirait d'abord d'un temps initial de développement plus long, de repenser les projets précédents dans le but d'unir toutes les applications selon une architecture technique commune et maintenable, avant de considérer la cohérence graphique si la bibliothèque Buefy n'est pas conservée.

> Une analyse technique plus poussée de ces améliorations est disponible dans la partie [*4.3 Frameworks et outils*](#gestion-des-evolutions).

### Business Intelligence

L'**informatique décisionnelle**, ou (+BI) (Business intelligence), a pour but d'**exploiter les données brutes** d'une organisation afin d'en faire ressortir des **informations utiles pour la prise de décisions**. En utilisant des sources très diversifiées comme des fichiers Excel, des bases de données SQL / NoSQL... pour les traiter et les analyser, on peut obtenir des informations très précises sur les activités de l'organisation. Cela permet de mieux comprendre ces activités et de **prendre des décisions plus éclairées** à propos de ces mêmes activités.

L'idée est d'offrir des outils très simples à manier comme un tableau de bord présentant des **graphiques** et des **indicateurs** clés de performance (KPI) qui permettent de suivre l'évolution de l'activité de l'organisation. Ces données sont formatées pour être mise à jour rapidement et offrir des **représentations parlantes et faciles à comprendre**. Très utiles pour les décideurs, mais aussi pour les employés qui peuvent suivre l'évolution de leur activité et de leur performance. Une fois que les données sont assez nombreuses et assez variées, il est possible d'utiliser la (+BI) comme un outil de **décision prédictive**, c'est-à-dire de prédire l'évolution de l'activité de l'organisation en fonction des données passées.

Ainsi, l'utilisation de la (+BI) sur le projet de TLB pourrait apporter une véritable plus-value quant à l'**amélioration des processus** qui sont utilisés en plusieurs points de l'application. Cela en analysant précisément les points qui peuvent être améliorés, ceux qui fonctionnent comme attendu et enfin ceux qui manquent pour aider ceux qui ne fonctionnent pas de façon optimale. Si cela permet de mieux comprendre les processus, il s'agit à terme de faire mieux comprendre aux techniciens de Groupama les **besoins des clients** pour mieux y répondre. Et à l'avenir cela permettrait de développer de **nouvelles possibilités** pour Groupama, selon l'analyse des données.

En allant encore plus loin, on peut considérer le **big data**, mais les données de l'application de TLB ne sont pas assez nombreuses pour que cela soit suffisamment intéressant. Il s'agirait de **mettre en place la (+BI) sur toutes les applications** de Groupama, ce qui permettrait de faire des **analyses plus précises** et plus poussées. Puis **mettre en place de la big data** afin de pouvoir faire des analyses prédictives sur l'**ensemble des données de Groupama**, dans un avenir proche.

> Après une gestion de projet parfois chaotique, avec un projet où les demandes sont contradictoires, on peut percevoir les différentes contraintes qu'il a fallu affronter pour en arriver là et les compromis qu'il a fallu prendre pour pouvoir avancer. Les enjeux restent forts et présents mais, il faut des efforts pour les faire ressortir des demandes client. Nous allons voir comment la technique a pu transformer ces enjeux pour y apporter une réponse et comment elle a souffert des contraintes du projet.

[^4]: Expression utilisée pour expliciter l'effet d'enfermement sur un projet qui conduit à un manque de recul.
