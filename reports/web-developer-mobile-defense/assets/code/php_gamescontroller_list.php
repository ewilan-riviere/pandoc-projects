if($request->hasfile('screenshot1'))
{	
    $image = $request->file('screenshot1');
    $filename = 'screenshot_01.jpg';

    $image_resize = Image::make($image->getRealPath());
    
    if ($image_resize->width() != 1280) {
        $image_resize->resize(1280, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
    if ($image_resize->height() != 720) {
        $image_resize->crop(1280, 720);
    }
    if (!file_exists('image/database/games/'.$gamesinfo->game_id.'/')) {
        mkdir('image/database/games/'.$gamesinfo->game_id.'/', 0777, true);
    }
    
    $image_resize->save(public_path('image/database/games/'.$gamesinfo->game_id.'/' .$filename));
    $gamesimage->screenshot1 = "1";
    $gamesimage->save();
}