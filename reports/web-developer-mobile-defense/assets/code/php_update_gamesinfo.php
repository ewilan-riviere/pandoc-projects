$gamesinfo = GamesInfo::find($game_id);
$gamesinfo_id = $gamesinfo->id;

$gamespegi = GamesPegi::where('info_id', '=', $gamesinfo_id)->first();
$gamesinfotag = DB::table('games_info_tag')->where('games_info_id', '=', $gamesinfo_id)->get();
$gamestag = GamesTag::all();
$gamesimage = GamesImage::where('info_id', $gamesinfo_id)->first();
// $gamesimage = DB::table('games_image')->where('info_id', $gamesinfo_id)->get();

$validatedInfo = $request->validate([
    'game_id' => 'required',
    'description' => 'nullable|string|max:3000',
    'release' => 'nullable|date',
    'franchise' => 'nullable|string|max:255',
    'developer' => 'nullable|string|max:255',
    'official_website' =>  'nullable|url|max:255',
    'forum_link' => 'nullable|url|max:255',
    'review_link' => 'nullable|url|max:255',
]);
GamesInfo::whereId($game_id)->update($validatedInfo);

$validatedPegi = $request->validate([
    'pegi_age' => 'nullable|string|max:10',
    'bad_language' => 'nullable|integer|max:1',
    'discrimination' => 'nullable|integer|max:1',
    'drugs' => 'nullable|integer|max:1',
    'fear' => 'nullable|integer|max15',
    'gambling' => 'nullable|integer|max:1',
    'in_game_purchase' => 'nullable|integer|max:1',
    'sexual' => 'nullable|integer|max:1',
    'violence' => 'nullable|integer|max:1',
]);
$gamepegi = GamesPegi::where('info_id', $gamesinfo_id)->update($validatedPegi);