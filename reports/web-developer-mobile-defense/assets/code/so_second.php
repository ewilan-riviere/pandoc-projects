$medium = Image::make($file);

$medium->resize(null, 500, function ($constraint) {
    $constraint->aspectRatio();
});

$medium->resize(500, null, function ($constraint) {
    $constraint->aspectRatio();
});             
$medium->save( public_path('/uploads/artistUploads/medium-' . $filename , 90) );