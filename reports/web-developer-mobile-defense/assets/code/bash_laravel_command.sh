# Copier le .env.example en .env
cp .env.example .env
# Modifier le .env DB_DATABASE, DB_USERNAME,
# DB_PASSWORD, AUTH_MAIL, AUTH_PASSWORD
vim .env
# Installer les dependances de composer
composer install
# Telecharger les dependances de NPM
npm install
# Generer la cle pour .env
php artisan key:generate
# Effectuer la migration de la base de donnees et la remplir
php artisan migrate --seed
# Lancer Mix
npm run dev
# Lancer le serveur en local
php artisan serve