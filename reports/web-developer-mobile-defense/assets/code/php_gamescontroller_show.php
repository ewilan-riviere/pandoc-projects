/**
* Display details of games in given collection
*
* @param string $game_id
*
* @return View
*/
public function show(string $game_id) {
    $collection = $this->getGamesCollection($game_id);
    $params['games'] = $games = $collection->games;
    $params['title'] = $games[0]->game_name;
    $params['base_url'] = 'https://www.dotslashplay.it/scripts';

    // Define variables
    $gamesinfo = GamesInfo::where('game_id', $game_id)->get();
    $gamesimage = null;
    $gamespegi = null;
    $gamesconfig = null;
    $games_tag = null;
    $gamesinfotag = null;

    // Link all Model with $gameinfo_id of GamesInfo
    if (isset($gamesinfo[0]->id)) {
        $gamesinfo_id = $gamesinfo[0]->id;
        $gamesimage = GamesImage::where('info_id', $gamesinfo_id)->get();
        $gamespegi = GamesPegi::where('info_id', $gamesinfo_id)->get();
        $gamesconfig = GamesConfig::where('info_id', $gamesinfo_id)->get();
    }
    
    return view('website.jeux-show')->with($params)->with(
        [
            'games_tag' => $games_tag,
            'gamesinfotag' => $gamesinfotag,
            'gamesinfo' => $gamesinfo,
            'gamesimage' => $gamesimage,
            'gamespegi' => $gamespegi,
            'gamesconfig' => $gamesconfig
        ]
    );
}