@if (isset($gamesimage[0]->screenshot1))
    @if ($gamesimage[0]->screenshot2 != null)
        @if ($gamesimage[0]->screenshot3 != null)
            <div id="main">
                <div class="slider">
                    <ul>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                                }}" />
                        </li>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_02.jpg")
                                }}" />
                        </li>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_03.jpg")
                                }}" />
                        </li>
                    </ul>
                </div>
            </div>
        @else
            <div id="main">
                <div class="slider">
                    <ul>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                                }}" />
                        </li>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_02.jpg")
                                }}" />
                        </li>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_02.jpg")
                                }}" />
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    @else
        <div id="main">
            <div class="slider">
                <ul>
                    <li>
                        <img src="{{ 
                                asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                            }}" />
                    </li>
                    <li>
                        <img src="{{ 
                                asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                            }}" />
                    </li>
                    <li>
                        <img src="{{ 
                                asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                            }}" />
                    </li>
                </ul>
            </div>
        </div>
    @endif
@else
    @if (isset($gamesimage[0]->banner))
        <img class="my-2" src="{{ 
                asset("image/database/games/".$gamesinfo[0]->game_id."/banner.jpg")
            }}" alt="{{ 
                $games[0]->game_name
            }}" />
    @endif
@endif
</div>