# SOLEIL

## Chapitre 1

— Cette branche est solide, jeune apprenti. Elle ne cassera pas.

— Pas sous ton poids, c'est certain. Je pèse toutefois vingt kilos de plus que toi. Peut-être trente.

— Alors il faut te mettre au régime.

— Vingt kilos de muscles.

— Et de langue. Tais-toi un peu et grimpe !

Salim sourit. Si Ellana le lui avait demandé, il se serait juché sur une branche deux fois plus fine sans émettre la moindre protestation, mais il appréciait trop leurs joutes verbales pour s'en priver.

Assise au sommet d'un arbre proche, Ellana le regardait gravir l'immense rougeoyeur qui surplombait le lac. Ses mouvements étaient fluides, ses prises assurées, à aucun moment depuis qu'il avait quitté le sol il n'avait atermoyé et, même si l'escalade ne présentait pas de difficulté particulière, elle était fière de ce qu'il était devenu.

Son apprentissage avait pris un virage à la fin de leur périple chez les Fils du Vent, presque un an plus tôt. Une pièce essentielle s'était mise en place et les formidables capacités qu'elle avait toujours pressenties en lui s'étaient enfin épanouies.

À moins, et l'honnêteté la poussait à considérer cette hypothèse avec attention, que ce soit elle qui ait changé à ce moment-là et, assumant son rôle de maître marchombre, lui ait permis d'assumer son statut d'apprenti.

Quoi qu'il en soit, il ne cessait de progresser.

Ce qui rendait d'autant plus indispensable la surprise qu'elle lui réservait.

Il avait atteint la branche qu'elle lui avait désignée et s'y avança, bras écartés pour assurer son équilibre, sans manifester d'hésitation. Grand, élancé, il était solidement bâti et, si les années élargiraient encore ses épaules et offriraient du volume à son torse, il faisait déjà preuve d'une belle prestance. Il n'était d'ailleurs pas rare que les jeunes femmes, et les moins jeunes, se retournent sur son passage lorsqu'il déambulait en ville.

— Tu commences par une simple vrille, lança Ellana.

— Je commence ? releva-t-il.

Elle haussa les sourcils.

— Tu ne crois quand même pas que je suis montée jusqu'ici pour te regarder plonger une seule fois ?

— Non, en effet.

Il se plaça face au vide, inspira profondément et s'élança.

Un plongeon parfait, agrémenté d'une vrille tout aussi parfaite. Il entra dans l'eau à la verticale, se cambra pour éviter de toucher le fond et regagna la rive en quelques brasses puissantes.

— Pas mal, lui lança Ellana lorsqu'il fut remonté au sommet du rougeoyeur. Pense à soigner ta cambrure et tends bien la pointe des pieds lorsque tu es en l'air. Un saut périlleux maintenant.

Pendant plus d'une heure, Salim enchaîna plongeon sur plongeon, puis Ellana lui demanda de se jeter dans le vide les yeux fermés et de ne réagir qu'à son injonction. Une injonction qu'elle se plut à donner de plus en plus tard, le poussant à accomplir des prodiges pour toucher la surface de la rivière de façon correcte.

Salim exultait. Il découvrait chaque jour davantage le bonheur d'avancer sur la voie du marchombre. Aux maîtres-mots d'ouverture et d'harmonie, Ellana lui avait permis d'ajouter celui de plénitude et il se demandait souvent, avec un frisson de délectation, combien il lui en restait à découvrir.

Elle finit par s'estimer satisfaite et tandis que, pour la cinquantième fois, il sortait de la rivière, elle s'avança à son tour au-dessus du vide.

Salim la vit s'élancer, si fine et aérienne que, pendant un instant, il crut qu'elle s'envolait. Pas de fioriture dans son plongeon mais un état de grâce. Bras ouverts, dos cambré, jambes tendues, ses longs cheveux noirs flottant derrière elle, elle fendit l'espace avec l'élégance d'un rêve.

Elle entra dans l'eau sans provoquer la moindre éclaboussure, si ce n'est dans le cœur de Salim.

«Merci, songea-t-il avec ferveur. Merci pour la voie que tu ouvres devant moi.»

— Non, Salim, ce soir nous ne rentrons pas à la maison.

Ils avaient retrouvé leurs montures et chevauchaient en direction des collines proches, alors que le soleil approchait de l'horizon.

— Encore des exercices ? s'enquit-il d'une voix joyeuse.

Elle le regarda en souriant.

— Il fut un temps où tu aurais évoqué cette possibilité sur un autre ton, remarqua-t-elle.

— Ce temps est révolu. Alors ? Des exercices ?

— Non, une visite à un ami.

— Aoro?

— Oui. Edwin et Ewilan sont à Al-Jeit pour quelques jours et l'auberge du Monde me manque. Tu n'as rien contre ?

— Non, au contraire. Oûl est le meilleur cuisinier de Gwendalavir et les caresses de la rivière m'ont donné faim.

— Les caresses de la rivière ?

La peau sombre de Salim ne suffit pas à masquer le rouge qui empourpra ses joues. Feignant de n'avoir rien remarqué, Ellana poursuivit :

— C'est drôle, lorsque je plonge, j'ai l'impression que c'est moi qui caresse la rivière et non le contraire. Cela dit, tes mots sonnent juste. Il faudra que je prête attention à qui caresse qui.

Salim s'empourpra de plus belle. De plaisir cette fois.

L'auberge du Monde n'était plus très loin et ils venaient de traverser un village niché dans une combe resserrée lorsque Ellana tira sur les rênes de Murmure.

— Des hommes sont embusqués là-bas, indiqua-t-elle à Salim en désignant un coude que formait la piste devant eux et la végétation dense qui le bordait.

— Comment le sais-tu ?

— Les oiseaux qui se sont envolés à l'instant et un reflet sur du métal. Une lame ou une pièce d'armure.

— Des voleurs ?

— Je doute que ce soit le boulanger du coin.

— Sil' Afian n'a-t-il pas éliminé de la région les bandits qui y pullulaient ?

— La mauvaise herbe repousse toujours.

— Depuis l'épisode de la Grande Dévoreuse, j'ai un peu de mal avec la mauvaise herbe, déclara Salim. Qu'est-ce qu'on fait ?

— Je t'attends ici.

— Ce qui signifie ?

— Ce qui signifie que tu vas à la rencontre des hommes qui se cachent là-bas. Je n'avais pas prévu de leçon pour ce soir mais puisque l'occasion se présente...

— Et si je me fais tuer ?

— C'est que tu es un maladroit qui a mérité ce qui lui arrive. Allez ne traîne pas, Aoro n'aime pas quand je suis en retard.

## Chapitre 2

Renonçant à discuter, Salim fit avancer son cheval d'un claquement de langue. Le soleil venait de basculer derrière les collines et l'obscurité était en passe de ravir le paysage à la lumière.

— Avec un peu de chance, marmonna-t-il, ces fichus oiseaux se seront envolés pour rien et le reflet métallique... eh bien, n'aura pas été un reflet métallique.

Le virage était proche. En se retournant, Salim constata qu'Ellana avait fait demi-tour et avait disparu.

Il hésita un instant à l'imiter puis renonça. S'il la contrariait, elle pouvait s'avérer plus redoutable qu'une poignée de bandits.

— Sauf qu'ils ne sont peut-être pas une poignée mais une bande entière, reprit-il à voix basse, et qu'il ne s'agit peut-être pas de bandits mais de tueurs assoiffés de sang.

Malgré l'adrénaline qui commençait à envahir son organisme, il sourit devant son imagination.

— Du calme, dit-il, et arrête de parler seul, les marchombres ne font pas ce genre de chose.

Les taillis se déchirèrent et deux hommes bondirent au milieu de la piste. Ils étaient armés de courtes lances que terminaient de larges pointes barbelées et si la nuit tombante empêchait de discerner le détail de leurs traits, leur attitude était clairement belliqueuse.

— Ton cheval, ordonna l'un d'eux. Ton cheval ou tu es mort.

Salim se laissa glisser à terre.

— C'est bien, renchérit le second bandit, tu es raisonnable.

— Raisonnable ? répéta Salim. C'est bien la première fois que quelqu'un me trouve raisonnable. Tu es sûr d'utiliser le mot adéquat ?

Surpris, les deux hommes se concertèrent du regard.

— Qu'est-ce que tu racontes ? cracha le premier.

— Je suis descendu de cheval non pour vous l'offrir, mais afin de poursuivre mon apprentissage dans de bonnes conditions, expliqua Salim avec affabilité.

Tout en parlant, il scrutait les fourrés pour déceler la présence d'un éventuel troisième lascar.

— Je suis en effet un apprenti, poursuivit-il, convaincu que les deux hommes étaient seuls. On pourrait presque dire un apprenti jardinier.

— Un jardinier ?

— Oui, un jardinier. Le type qui s'occupe de la mauvaise herbe.

— Ça suffit ! vociféra un des hommes. Tu la fermes, tu recules sans mouvement brusque et tu dégages ! On est chargés de récupérer ton cheval, pas de te tuer, mais si tu insistes...

— Justement, lança Salim, j'insiste.

Il avait beau plaisanter, il prenait sa situation très au sérieux. Un combat n'est jamais gagné d'avance lui avait enseigné Ellana et sur ce point, comme sur tant d'autres, il savait que la marchombre avait raison.

En outre, et cela l'incitait à la prudence, les deux hommes qui lui faisaient face étaient subtilement différents des bandits qu'il avait eu l'occasion de croiser jusqu'ici.

Leurs armes d'abord, ces lances habituellement réservées aux gardes d'Al-Vor. Neuves et identiques, ce n'étaient pas des armes de bandits de grand chemin. Leurs vêtements ensuite. Des tuniques propres, taillées dans le même tissu blanc, presque des uniformes. Leurs déclarations enfin. Que signifiait «chargés de récupérer ton cheval» ?

Autant de questions qui auraient mérité des réponses mais Salim ignorait comment les obtenir.

— Est-ce que... commença-t-il.

Une lance fusa vers sa gorge, signe que le temps des paroles avait cédé la place à celui du combat.

Salim évita souplement la lame barbelée, empoigna la hampe de la lance et tira d'un coup sec. Surpris, l'homme qui la tenait fit un pas en avant. Le coude de Salim le cueillit au plexus solaire avant de s'abattre sur sa nuque lorsqu'il se plia en deux.

_«Un combat est un seul geste. Qu'il dure une seconde ou une heure. Qu'il t'oppose à un ennemi ou à dix. Un seul geste, un seul souffle.»_

Son adversaire n'était pas encore à terre que Salim bondissait. Son talon percuta le nez du deuxième bandit avant qu'il ait pu brandir son arme. Sous l'impact, l'homme partit en arrière, lâcha sa lance et s'effondra dans les taillis.

Il se releva presque aussitôt et, sans demander son reste, s'enfuit dans la nuit.

Les yeux de Salim se posèrent sur son comparse qui se redressait péniblement. Un instant, il envisagea de le questionner pour assouvir sa curiosité mais son estomac se rappela à son souvenir en gargouillant. Discuter ne servirait qu'à lui faire perdre du temps alors qu'il mourait de faim.

— C'est par là, fit-il en désignant la forêt.

Le bandit, qui peinait à conserver son équilibre, lui jeta un regard effaré.

— Dépêche-toi, insista Salim. Je n'ai pas que ça à faire.

Les fourrés bruissèrent et Salim se retrouva seul au milieu de la piste.

Il ramassa les lances abandonnées sur le sol et les attacha derrière sa selle. Il mettait le pied à l'étrier lorsque Ellana revint vers lui.

— Pas mal, lui lança-t-elle.

— Tu as déjà qualifié ainsi mes plongeons dans la rivière, lui rétorqua-t-il avec une pointe d'agacement. Est-il envisageable qu'un jour tu montres davantage d'enthousiasme devant mes performances ? Sans aller jusqu'à t'extasier, bien sûr.

— De l'enthousiasme ? releva-t-elle.

— Oui, que pour une fois tu m'offres un avis du genre : Bravo, Salim, tu t'es vraiment bien débrouillé.

— Ce n'est pas ce que j'ai dit ? fit-elle mine de s'étonner.

— Non. Tu as dit pas mal.

— Tu chipotes. Allez, accélère le mouvement, j'ai faim.

— Est-ce que tu as au moins regardé de quelle façon j'ai éliminé ces types ? s'enquit Salim alors qu'il se hissait en selle.

— De loin.

Il écarta les bras pour montrer son incompréhension.

— Te rends-tu compte à quel point ton attitude est frustrante ? s'exclama-t-il. Je peux comprendre que, pour mon entraînement, tu m'aies laissé les affronter seul, même si tu n'avais aucun moyen de savoir s'ils n'étaient pas vingt, armés de sabres et d'arcs. En revanche, tu aurais pu faire l'effort de t'approcher, non ? Juste pour être en mesure de me livrer des commentaires pertinents après le combat ou, on ne sait jamais, pour intervenir pendant le combat.

— Pas eu envie de prendre des risques.

— Quoi ?

Elle le regarda, un sourire énigmatique sur les lèvres, puis répéta en articulant chaque mot :

— Pas eu envie de prendre des risques.

Alors qu'il écarquillait les yeux de stupéfaction, elle lui adressa un clin d'œil et talonna Murmure.

— En route, lança-t-elle, je t'ai dit que j'avais faim !
