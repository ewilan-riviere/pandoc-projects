# Préface {#préface .unnumbered}

Ce rapport est rédigé en LaTeX entre avril et mai 2019 pour le projet de
certification `./play.it` développé par **Ewilan RIVIERE** et commandé
par **Antoine Le Gonidec**. Ce projet a été supervisé par la **Code
Académie** et son équipe de formateurs et formatrices : **Erwann
Duclos**, **Frédéric Hannouche**, **Solène Huault**, **Fabrice
Pluriel**. Ce dossier est composé de l'expression des besoins, du cahier
des charges et du dossier professionnel. Au long du développement, le
cahier des charges s'est vu différer sur quelques points de l'expression
des besoins, j'ai suivi les demandes de mon client pour les
fonctionnalités qu'il considérait comme prioritaires par rapport aux
autres.

C'est un projet que j'ai appris à connaître et à aimer, et si le
développement a connu des moments délicats, cela n'a fait qu'ajouter à
l'attachement que j'ai pour `./play.it`. C'est grâce à ce développement
que j'ai appris à appréhender le framework *Laravel* et à améliorer ma
compréhension du *PHP*, ce qui n'a fait que renforcer mon attirance pour
le développement web back-end et me rendre plus sûre de moi pour mon
projet professionnel.

Je remercie chaleureusement **l'équipe de la Code Académie**, citée
ci-dessus, qui ont su m'accompagner, me conseiller et me rassurer durant
toute ma formation et lors du développement de `./play.it`. **Mon
commanditaire**, cité ci-dessus, qui a su m'accompagner et diriger mon
travail avec sagesse, gentillesse et patience. Mais aussi des apprenants
de la Code Académie : **Quentin Juhel** pour son savoir-faire concernant
les clés étrangères dans *Laravel*, **Nora Hennebert** pour son aide
inestimable lorsque l'algorithmie de certaines parties du projet me
dépassait, **Hugo Schindelman** pour avoir réussi à me faire comprendre
comment lier des tables dans la base de données *Laravel*.

**Ewilan RIVIERE**

# Dossier de certification

## Présentation

### Résumé

`./play.it` est un projet communautaire autour du jeu vidéo sur Linux,
le but étant la création de scripts (fichiers .sh) pour installer
facilement des jeux vidéo provenant des plateformes de distributions
sans DRM[^1] (comme GOG et Humble Bundle mais pas Steam) pour jouer
facilement sous Linux.

Quand le projet fut lancé, il y avait très peu de scripts proposés car
très peu de jeux étaient supportés et tout était géré directement dans
le code, sans base de données. Mais avec les années, le nombre de jeux
augmenta et la liste en conséquence, ce qui aboutit à plus de 400 jeux
gérés où les informations sont codées en dur. Cette situation rend la
mise à jour des informations des jeux particulièrement difficile et
compliquée.

La personne ayant initié le projet, Antoine Le Gonidec, avait besoin
d'un nouveau site web, l'idée est de gérer les jeux directement en base
de données. Cela permet de modifier leurs informations plus facilement
et en ajouter à des jeux qui n'en ont pas grâce à une interface
utilisateur agréable. Plus encore, le site web doit être suffisamment
attirant pour que des personnes habituées de Windows ne soient pas
frileuses à l'idée de passer sous Linux pour jouer.

### Liste des compétences REAC du projet

#### Développer la partie front-end d'une application web

- Maquetter une application

- Développer une interface web statique et adaptable

- Développer une interface utilisateur web dynamique

#### Développer la partie back-end d'une application web

- Créer une base de données

- Développer les composants d'accès aux données

- Développer la partie-back-end d'une application web ou web mobile

## Expression des besoins

### Contexte

#### Explications concernant le féminin dominant

Dans notre société, il est important de prendre en compte le fait que le
langage formate un produit. Il a été décidé que pour ./play.it, la
question du genre des personnes utilisant le site web n'est pas
anecdotique. Il est important de promouvoir la place des personnes qui
ne sont pas des hommes dans le monde du jeu vidéo et de l'informatique,
ainsi cette expression de besoins accorde les termes genrés au féminin
dominant pour souligner cette position.

#### Historique et situation actuelle

Le site de ./play.it est accessible pour les personnes souhaitant jouer
à des jeux sans DRM[^2] sous Arch Linux ou Debian[^3], grâce à des
scripts. Ainsi cela simplifie grandement l'installation des jeux et
permet de toucher un plus large public de joueuses sous Linux.

Si le site existe déjà, son design repose sur des bases qui ne sont plus
aujourd'hui très pratiques pour représenter le nombre, croissant, de
jeux proposés par le projet.

Actuellement, play.it a été présenté sur les forums de Debian Facile,
debian-fr.org, debian-fr.xyz, ubuntu-fr et linuxMint et les problèmes
que les internautes rencontrent sont remontés sur ces sujets, ce qui
nécessite un suivi constant de la part '../../../title-web-developer-mobile/assets/src'de l'équipe.

#### Commanditaire du projet

Le commanditaire est le créateur du projet ./play.it, Antoine Le
Gonidec, qui l'a lancé en 2016 et qui continue de le maintenir avec
l'aide d'une équipe de volontaires.

#### Présentation du projet

Le projet est d'améliorer l'aspect graphique du site de ./play.it pour
le rendre plus facile d'utilisation, notamment pour les personnes
débutant sous Linux. Une base de donnée sera nécessaire également afin
de simplifier la rédaction des fiches et la maintenance du site.

#### Financement

C'est un projet bénévole.

### Public cible

Le public primaire est d'abord les joueuses venant de Windows, habituées
à un environnement différent de Linux et souvent peu à l'aise avec les
commandes, le but est donc de présenter les scripts de façon attractive
pour les pousser à essayer.

Le public secondaire est davantage tourné vers les joueuses
expérimentées de Linux (qui peuvent parfois avoir le JavaScript
désactivé dans leur navigateur) qui sont habituées aux commandes, elles
recherchent des informations disposées de façon claire avec un forum où
rechercher dans une situation où elles rencontrent des problèmes.

Le site de ./play.it doit donc répondre à ces deux publics, considérant
leurs contraintes et satisfaisant les deux.

### Description de l'application

Tout le design sera maquetté puis soumis à la validation du
commanditaire avant la réalisation. L'idée serait d'améliorer l'aspect
graphique et de trouver d'autres manières de présenter le contenu, de
façon suffisamment ludique pour attirer les habituées venant de Windows
qui n'utilisent pas le terminal ou à chercher comment installer un jeu
sous leur OS.

#### Synthèse des fonctionnalités

|                                   Principales | Secondaires                                                            | Optionnelles                                                                                                   |
| --------------------------------------------: | :--------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- |
|                   Design agréable et intuitif | Barre de menu intuitive et fixe                                        | Fiches de jeux : parties collapsables pour chaque partie, fond d'écran du jeu concerné et un carousel d'images |
|                               Base de données | Glossaire                                                              | Section pour les jeux libres pour les promouvoir                                                               |
| Templates pour la rédaction des fiches de jeu | Triage des jeux : alphabétique, récents, mis à jour, plus visités, tag | Forum pour permettre à la communauté d'échanger sur ./play.it                                                  |
|   Pas de JavaScript (fonctionnalités vitales) | Fonds d'écran des jeux vidéo concernés                                 | Lien RSS pour les mises à jour                                                                                 |
|                     Traductions multi-langues | Sections collapsables2 pour chaque lettre dans la liste                | Reviews                                                                                                        |

#### Fonctionnalités principales

- Amélioration globale de l'aspect esthétique : recherche d'un design
    plus attractif, expliciter les tutorials de manière plus agréable
    pour que les novices s'y retrouvent

- Une base de données qui va répertorier toutes les informations :
    d'un côté une API[^4] et de l'autre le site web de ./play.it

- Templates[^5] pour la rédaction des fiches de jeu :

    1. Description : titre, description, âge requis, tags, site
        officiel, date de sortie, carousel

    2. Installation : scripts de ./play.it, liens de téléchargements
        pour acheter le jeu légalement sans DRM, sections séparées selon
        la distribution Linux

    3. Commandes : liste des commandes à taper pour obtenir un jeu
        installé grâce aux scripts de ./play.it

- Internationalisation des informations avec d'abord du français et de
    l'anglais puis d'autres langues par la suite

#### Fonctionnalités secondaires

- Une barre de menu intuitive et sticky[^6] pour aider la navigation
    des internautes.

- Un glossaire pour expliquer certains termes techniques comme
    \"logiciel libre\", \"DRM\" ou les distributions de Linux mais aussi
    présenter certaines bases concernant l'utilisation de Linux (de
    l'installation de l'OS à l'utilisation basique du terminal).

- La possibilité de trier les jeux :

    1. alphabétique

    2. récents

    3. mis à jour

    4. les plus visités

    5. par tag - les tags seront explicités dans la section glossaire
        du site

- Des fonds d'écran des jeux vidéo concernés apparaîtrait de manière
    aléatoire sur le fond du site pour contrer l'effet rébarbatif que
    peut représenter des tutorials d'installation.

- La liste des jeux étant trop longue, ajouter la possibilité
    d'un menu collapsable[^7] pour chaque partie afin de simplifier la
    navigation

- Légèreté pour que le site se charge rapidement - le JavaScript étant
    relativement réduit, cela aidera à un chargement plus rapide

#### Fonctionnalités en plus

- Chaque fiche de jeu aurait des menus collapsable pour chaque
    catégorie, un fond d'écran du jeuconcerné et un carousel d'images du
    jeu

- Une section pour les jeux libres afin de les promouvoir

- Un forum ainsi qu'un lien RSS

- Une page pour les reviews (avec des commentaires et des notes) afin
    d'avoir un lien communautaire en plus du forum

Toutes ces fonctionnalités ne pourront pas être développées pour le
projet de certification car il n'y aura pas assez de temps mais
l'intérêt est d'en faire le plus possible.

Il faudrait également constituer une base de donnée afin de rendre la
rédaction des pages de chaque jeu (écrites pour le moment en dur dans le
code) plus simple. Ainsi l'ajout de nouveaux jeux et la gestion du site
seraient plus pratiques.

Il est important de noter que le JavaScript pourra être utilisé mais il
est nécessaire que le site soit totalement fonctionnel sans, afin qu'une
partie du public ciblé (qui peut avoir comme habitude de désactiver le
JavaScript dans leur navigateur) puisse y accéder sans aucun problème.
Le JS ne pourra qu'apporter quelques améliorations visuelles mais aucune
d'ordre fonctionnelle.

### Responsive

Bien que le projet de ./play.it est d'abord fait pour les personnes
utilisant des écrans de PC (1280 pixels), le site sera designé de façon
à ce que l'écran utilisé ne pose pas de problème.

#### Accessible PC

Le projet est conçu, à la base, pour un écran de 1280 pixels avec le
design au service de l'ergonomie.

#### Accessible tablette

Pour les écrans de tablette (960 pixels), en mode paysage, le projet
sera très proche de son visuel sur écran large. Et pour le mode
portrait, ce sera proche de celui de l'écran de smartphone.

#### Accessible smartphone

Le projet sera conçu pour être accessible, alliant ergonomie et design,
davantage adapté à l'ergonomie pour l'écran de smartphone (320 pixels).

### Compatibilité

Le site sera compatible avec (en ordre de priorité) :

- Mozilla Firefox (version 59.0.2 ou supérieure)
- Google Chrome (version 72.0.3626 ou supérieure)
- Safari
- Opera
- Microsoft Edge

### Espace public

#### Introduction

Présente le site pour les internautes ne le connaissant pas avec les
informations globales concernant la raison d'être de ./play.it, les
fonctionnalités principales de ce projet ainsi que des gifs présentant
la facilité de l'installation d'un jeu avec ./play.it.

Un menu sera visible dans la page (en burger[^8] et latéral) pour
permettre aux internautes d'accéder directement aux fonctionnalités
souhaitées mais laissant la place au design.

#### Accueil

Explications sur le fonctionnement des scripts et l'utilisation du site,
l'accès aux forums des partenaires, l'IRC et Git. Des explications pour
l'accès au jeu installé grâce aux scripts seront également disponibles.

#### Liste de jeux

Liste des jeux classés par ordre alphabétique par défaut avec un menu au
début tel que \"\# A B C\...\" permettant d'accéder plus rapidement aux
lettres souhaitées. Un menu collapsable pour chaque partie afin d'éviter
que la page soit trop longue, un tri sera aussi possible selon les jeux
récents, les mises à jour, les tags\...

La première section sera déroulée pour montrer à l'utilisatrice que
c'est possible, chaque section ouverte refermera la précédente afin de
ne pas obliger à un défilement trop important.

#### Archives françaises

Liste des archives disponibles pour les jeux trop vieux qui n'ont plus
de ressources pour la langue française, les archives ont pu être
fournies par des personnes ayant encore les supports utilisés au moment
de la sortie de ces jeux.

#### Jeux libres

Les jeux libres étant des projets conséquents sur le long terme, cette
page a pour but de les référencer.

#### Forum

Un forum où les problèmes rencontrés par les internautes pourront être
mieux centralisés que sur les forums des partenaires. Cette partie sera
développée dans un deuxième temps.

Les sujets répondant aux problèmes rencontrés par les utilisatrices
seront visibles par toute personne naviguant sur le forum.

### Espace privé

#### Liste de jeux · Connexion d'admin

Cette interface sécurisée permettra d'ajouter de nouvelles fiches qui
pourront être relues avant d'être validées et accessibles par les
internautes.

#### Archives · Connexion d'admin

Cette interface sécurisée permettra d'ajouter de nouvelles archives pour
les internautes.

#### Forum · Connexion d'utilisatrice

Les sujets les plus sensibles comme les ceux où les membres se
présentent ou ont des discussions informelles ne seront visibles que par
les internautes connectées au forum.

La possibilité de poster des nouveaux sujets ainsi que d'avoir des messages privés sera réservée aux personnes connectées.

#### Forum · Connexion de modératrice

Les droits de modération seront réservés à une équipe restreinte de bénévoles qui pourront rappeler les internautes à l'ordre, modifier leurs messages ou les supprimer. Les modératrices auront également accès à une section de sujets réservés à l'équipe de play.it.

## Cahier des charges

#### Fonctionnalités

- Affichage de la liste totale des jeux récupérée depuis l'API : *la liste totale compte plus de 400 jeux, ce qui peut représenter une masse assez indigeste pour l'utilisateur.*
- Affichage des jeux par lettre : *ainsi la liste est séparée en plusieurs sections, l'utilisateur peut plus facilement naviguer parmi les jeux.*
- Recherche : *accessible depuis toutes les pages, elle permet de rechercher des jeux par leur nom et renvoit à une page présentant les résultats.*
- Fiche de jeu : *les fonctions en gras sont directement liées à la base de données locale, les autres sont gérées par l'API. Tous les jeux ne disposent pas d'autant d'informations, seuls les jeux dont tous les champs ont été remplis proposent ce qui suit. Par défaut un jeu proposera uniquement les informations provenant de l'API.*
    1. Titre : *peut être remplacé par une image en .jpg avec le titre utilisant la police du jeu, ce qui permet une identification plus rapide.*
    2. **Bannière** : *disponible par défaut si le jeu n'a pas d'impressions d'écran sinon la bannière est juste affichée dans la liste des jeux.*
    3. **Carousel d'impressions d'écran** : *au nombre de trois au maximum, elles défilent en continu sur la fiche de jeu.*
    4. **Description**
    5. **Franchise**
    6. **Tags**
    7. **Date de sortie**
    8. **Développeur** et son **site web**
    9. Plateforme d'achat sans DRM
    10. **Topic du forum** : *le forum n'est pas encore créé, par conséquent ce champ a été fait pour un développement futur.*
    11. **Reviews** : *les reviews ne sont pas encore disponibles, ce champ est donc disponible pour un développement futur.*
    12. **Configuration** : **processeur**, **mémoire**, **graphismes** et **espace disque**
    13. **PEGI** : **âge requis** et **contenus problématiques**
    12. Scripts d'installation
- Édition des informations de chaque fiche de jeu (pour celles qui dépendent de la base de données locale) : *disponible uniquement si l'utilisateur est connecté*
- Suppression de toutes les informations de chaque fiche de jeu (pour celles qui dépendent de la base de données locale) : *disponible uniquement si l'utilisateur est connecté*
- Connexion pour accéder aux droits administrateurs : *les identifiants sont créés par la personne qui gère le serveur.*

#### Dépendances

Ce projet utilise **Laravel** (*v5.8*) qui utilise du **PHP** (*v7.3*),
Laravel est géré par **Composer** (*v1.8.4*) et quelques dépendances
sont gérées par **NodeJS** (*v11.15*). L'initialisation du dépôt git
Laravel
([framagit.org/vv221/play.it-website](https://framagit.org/vv221/play.it-website))
demande les commande suivantes :

``` {.Bash language="Bash"}
# Copier le .env.example en .env
cp .env.example .env
# Modifier le .env DB_DATABASE, DB_USERNAME,
# DB_PASSWORD, AUTH_MAIL, AUTH_PASSWORD
vim .env
# Installer les dependances de composer
composer install
# Telecharger les dependances de NPM
npm install
# Generer la cle pour .env
php artisan key:generate
# Effectuer la migration de la base de donnees et la remplir
php artisan migrate --seed
# Lancer Mix
npm run dev
# Lancer le serveur en local
php artisan serve
```

#### Charte graphique

Actuellement, la partie graphique du site web est simple pour me
permettre de me concentrer sur les fonctionnalités, **Bootstrap** a été
largement utilisé pour les couleurs ce qui permet de ne pas perdre les
visiteurs qui sont habitués à ces couleurs très souvent utilisées. La
couleur principale est un blanc-gris avec des couleurs bien ciblées pour
mettre en évidence les fonctionnalités disponible :

- Barre de recherche : icône de recherche en vert
- Liste de jeux : bannières pour les jeux
- Navigation : titre en gras si l'utilisateur est sur la page concernée
- Modification d'information : vert pour l'ajout, rouge pour la suppression

Les maquettes sont disponibles dans les Annexes,

#### Navigation

![pageflow de ./play.it](assets/images/play-it-pageflow.jpg){#pageflow
width="100%"}

La navigation sur le site web est représentée par le schéma ci-dessus
([2.3](#pageflow){reference-type="ref" reference="pageflow"}). Le
premier contact de l'utilisateur avec le site web l'amènera à la page
d'introduction présentant rapidement le projet de `./play.it` et offrant
la possibilité de rejoindre n'importe quelle page à l'aide d'un menu
disposé en barre latérale rétractable à l'aide d'une icône en position
fixe qui va défiler en même temps que l'utilisateur. Pour les autres
pages, le menu est une barre en haut de l'écran qui défilera en même
temps que l'utilisateur pour lui permettre de changer de page à tout
moment.

Les pages Introduction, Accueil, Liste des jeux et Connexion sont
disponibles, les autres ne sont pas encore utilisables actuellement.

##### Page d'accueil

La page d'accueil présente plus en détail le projet et propose un
tutoriel détaillé pour l'utilisation de `./play.it` avec chaque étape
nécessaire pour installer un jeu vidéo grâce aux scripts. Une seconde
partie indique sur quels forums se rendre pour parler des problèmes
rencontrés par l'utilisateur. La dernière partie rasemble les dépôts de
développement pour les utilisateurs intéressés par la partie open source
et pour aider au développement du site web.

##### Page de liste des jeux

La page de la liste des jeux permet d'avoir accès à tous les jeux
récupérés depuis l'API de `./play.it` et triés par ordre alphabétique.
Un panneau latéral permet d'avoir accès aux jeux selon leur première
lettre. Les jeux disposant d'informations complémentaires, disponibles
en base de données, auront une bannière pour permettre une
identification plus rapide pour l'utilisateur.

##### Page de fiche de jeu

Après avoir sélectionné un jeu, l'utilisateur arrive sur la fiche du
jeu, présentant de base le titre, la plateforme d'achat et les scripts
d'installation (récupérés depuis l'API) selon la distribution Linux. Des
informations supplémentaires peuvent être visible si elles ont été
ajoutées via le bouton d'ajout d'informations, celui-ci est visible
uniquement si l'utilisateur est connecté. Ces informations seront
stockées dans la base de données et dépendront du site web et non de
l'API. Les informations supplémentaires peuvent être supprimées avec le
bouton concerné (accessible si l'utilisateur est connecté et suivi d'une
page de confirmation pour s'assurer que l'utilisateur n'a pas cliqué
dessus par inadvertance).

##### Page d'ajout d'informations

Lors de l'ajout d'informations, l'utilisateur se voit proposer un
formulaire dans lequel il pourra rentrer toutes les informations
optionnelles concernant le jeu sélectionné. Tous les champs sont
optionnels et peuvent être de nouveau modifiés par la suite via le même
formulaire.

##### Page de recherche

La barre de recherche accessible directement depuis la barre de
navigation permet de recherche des jeux par leur nom et mène directement
à une page présentant le résultat de la recherche effectuée. Cette
fonction utilise directement l'API et sa fonction de recherche.

#### Compte administrateur

Suite à une demande du client, le compte utilisateur utilisé ne pourra
compter qu'un seul compte et les informations seront renseignées dans le
.env du dépôt pour des questions de sécurité. La possibilité de créer un
nouveau compte a été suspendue. Les informations de connexion pourront
être données à des personnes de confiance selon la volonté du client.

#### Langages et Frameworks

Les langages utilisés sont le **PHP** *7.3*, le **HTML** *5*, le **CSS**
*3* (avec **SASS**), le **JavaScript** (pour des fonctionnalités
optionnelles) et **MySQL** pour la base de données.\
**Laravel** (*v5.8*) est utilisé pour la partie back-end et front-end
(avec **Blade**), **Bootstrap** (*v4.3*) est utilisé pour accompagner
les feuilles de styles propres au site web, **jQuery** (*v3.4.1*) est
utilisé pour les fonctionnalités JavaScript. Je me connecte à l'API
*api.dotslashplay.it* qui est réalisée en **Lumen** *v5.8* par mon
client.

L'absence d'utilisation d'un framework JS, souvent utilisé aujourd'hui
pour la partie front-end dans les applications web récentes, est dû à la
demande expresse du client de ne pas utiliser de JavaScript pour les
fonctions vitales. Par conséquent, c'est **Blade** qui a dû être utilisé
pour réaliser cette partie, les fonctionnalités habituelles du front-end
comme les carousels menus rétractables ont été réalisés en CSS 3.

#### UML

##### Base de données

![Base de données](assets/images/bdd-concepteur.jpg){#pageflow
width="100%"}

### Spécifications fonctionnelles

|  NOM | CATÉGORIE          | FONCTIONNALITÉ                                                                                                                                                                                                                                                       |   RT   |
| ---: | :----------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | :----: |
| CU-1 | COMPTE UTILISATEUR | ACCÈS APPLICATION : un utilisateur nonconnecté pourra avoir accès à l’application webpour consulter des informations mais ne pourrapas accéder aux fonctions de modification deces informations.                                                                     | AUTH-0 |
| CU-2 | COMPTE UTILISATEUR | CONNEXION : depuis la barre de navigation,l’utilisateur a accès à un lien qui le mène vers lapage de connexion où il peut se connecter s’ila les identifiants du compte administrateur.                                                                              | AUTH-1 |
|      |                    |                                                                                                                                                                                                                                                                      |        |
| LJ-1 | LISTE DES JEUX     | AFFICHAGE : depuis la barre de navigation,l’utilisateur a accès à un lien vers la page dela liste des jeux où il aura la liste complète desjeux gérés par le projet.                                                                                                 | READ-1 |
| LJ-2 | LISTE DES JEUX     | AFFICHAGE PAR NOM : depuis la page dela liste des jeux, l’utilisateur aura accès à unebarre de menu latérale où il pourra sélectionnerla lettre qu’il souhaite pour n’afficher que lesjeux commençant par celle-ci.                                                  | READ-2 |
| LJ-3 | LISTE DES JEUX     | ACCÈS FICHE DE JEU : l’utilisateur a accèsaux fiches de chaque jeu, rassemblant toutesles informations du jeu concerné : descriptions, images et scripts d’installation.                                                                                             | READ-3 |
| LJ-4 | LISTE DES JEUX     | RECHERCHE : l’utilisateur peut utiliser labarre de recherche disponible dans la barre denavigation pour recherche un jeu par son nom, ce qui renvoie sur une page où tous les jeux répondant à ce terme de recherche sont regroupés.                                 | READ-4 |
| FJ-1 | FICHE DE JEU       | AJOUT D’INFORMATION : un utilisateur au-thentifié pourra ajouter des informations à unjeu qui n’en possède pas.                                                                                                                                                      | CREA-1 |
| FJ-2 |                    | MODIFICATION D’INFORMATION : un utili-sateur authentifié pourra modifier des informa-tions existantes si le jeu en possède.                                                                                                                                          | UPDT-1 |
| FJ-3 |                    | SUPPRESSION D’INFORMATION : un utili-sateur authentifié pourra supprimer toutes lesinformations optionnelles (seulement les infor-mations descriptives et les images mais pas lesscripts qui ne sont pas gérés par la base de don-nées locale) si le jeu en possède. | DEL-1  |
|      |                    |                                                                                                                                                                                                                                                                      |        |
|  N-1 | NAVIGATION         | NAVIGATION : l’utilisateur peut naviguer entreles différentes pages grâce à la barre de naviga-tion.                                                                                                                                                                 | NAV-0  |

::: {.center}
::: {.longtable}
\|c\|c\|L\|c\| & & &\
CU-1 & & & AUTH-0\
CU-2 & & & AUTH-1\
\
LJ-1 & & & READ-1\
LJ-2 & & & READ-2\
LJ-3 & & & READ-3\
LJ-4 & & & READ-4\
\
FJ-1 & & & CREA-1\
FJ-2 & & & UPDT-1\
FJ-3 & & & DEL-1\
\
N-1 & NAVIGATION & & NAV-0\
:::
:::

### Spécifications techniques

::: {.center}
::: {.longtable}
\|c\|M\|c\| & &\
\
AUTH-0 & & CU-1\
AUTH-1 & & CU-2\
\
READ-1 & & LJ-1\
READ-2 & & LJ-2\
READ-3 & & LJ-3\
READ-4 & & LJ-4\
\
CREA-1 & & FJ-1\
\
UPDT-1 & & FJ-2\
\
DEL-1 & & FJ-3\
\
NAV-0 & & N-1\
:::
:::

## Réalisations du projet

#### Fonction list() du GamesController

``` {.php language="PHP"}
if($request->hasfile('screenshot1'))
{ 
    $image = $request->file('screenshot1');
    $filename = 'screenshot_01.jpg';

    $image_resize = Image::make($image->getRealPath());
    
    if ($image_resize->width() != 1280) {
        $image_resize->resize(1280, null, function ($constraint) {
            $constraint->aspectRatio();
        });
    }
    if ($image_resize->height() != 720) {
        $image_resize->crop(1280, 720);
    }
    if (!file_exists('image/database/games/'.$gamesinfo->game_id.'/')) {
        mkdir('image/database/games/'.$gamesinfo->game_id.'/', 0777, true);
    }
    
    $image_resize->save(public_path('image/database/games/'.$gamesinfo->game_id.'/' .$filename));
    $gamesimage->screenshot1 = "1";
    $gamesimage->save();
}
```

Cette fonction permet de récupérer la liste des jeux depuis l'*API*
grâce à la fonction **getGamesList()** définit plus haut dans le
controller (qui lui même fait appel à l'*API*, *api.dotslashplay.it*).
Cette liste ainsi obtenue est traitée dans une boucle FOR qui parcourt
tout le tableau et juste avant cette boucle, une variable *\$character*
est créé comme étant vide ainsi qu'un tableau *\$alpha*, vide aussi.
Dans cette boucle, une boucle IF ELSE commence à classer les jeux selon
la première lettre de leur game_id, récupérée comme ceci : game_id\[0\],
(qui correspond au nom du jeu ayant été normalisé) si elle est
différente de la variable *\$character* qui a été définie comme vide et
qui prend la valeur correspondant à chaque lettre de l'alphabet dans les
boucles suivantes.

Si la première lettre du game id n'est pas un *integer*, celle-ci est
attribuée à la variable *\$character* qui est ajoutée au tableau
*\$alpha* comme une clé, créé précédemment. Et si c'est un *integer*,
cela crée une clé number dans le tableau *\$alpha*.

Lorsque la boucle reparcourt la liste des jeux, elle classe les jeux
selon la première lettre de leur game_id dans le tableau *\$alpha* selon
les clés créées précédemment.

Cette fonction permet d'obtenir un tableau classé par lettre qui sera
utile pour la liste des jeux par lettre. Dans la suite de la fonction,
la liste est envoyée en paramètre de la fonction ainsi que d'autres
paramètres dont celui lié aux images, permettant de retrouver la
bannière des jeux s'ils en ont une. Tout cela étant envoyé vers la vue
*jeux* du dossier *website*.

#### Fonction show() du GamesController

``` {.php language="PHP"}
/**
* Display details of games in given collection
*
* @param string $game_id
*
* @return View
*/
public function show(string $game_id) {
    $collection = $this->getGamesCollection($game_id);
    $params['games'] = $games = $collection->games;
    $params['title'] = $games[0]->game_name;
    $params['base_url'] = 'https://www.dotslashplay.it/scripts';

    // Define variables
    $gamesinfo = GamesInfo::where('game_id', $game_id)->get();
    $gamesimage = null;
    $gamespegi = null;
    $gamesconfig = null;
    $games_tag = null;
    $gamesinfotag = null;

    // Link all Model with $gameinfo_id of GamesInfo
    if (isset($gamesinfo[0]->id)) {
        $gamesinfo_id = $gamesinfo[0]->id;
        $gamesimage = GamesImage::where('info_id', $gamesinfo_id)->get();
        $gamespegi = GamesPegi::where('info_id', $gamesinfo_id)->get();
        $gamesconfig = GamesConfig::where('info_id', $gamesinfo_id)->get();
    }
    
    return view('website.jeux-show')->with($params)->with(
        [
            'games_tag' => $games_tag,
            'gamesinfotag' => $gamesinfotag,
            'gamesinfo' => $gamesinfo,
            'gamesimage' => $gamesimage,
            'gamespegi' => $gamespegi,
            'gamesconfig' => $gamesconfig
        ]
    );
}
```

Cette fonction permet de retrouver toutes les informations de chaque
jeu, par leur game_id, par l'*API* et la base de données locale. La
fonction **getGamesCollection()** définie plus haut dans le
**GamesController** permet de récupérer les informations du jeu de
l'*API* par son game_id, ainsi les informations basiques telles que le
nom, la plateforme d'achat et les scripts sont affichés sur chaque fiche
de jeu.

Les informations supplémentaires sont chargées par la partie suivante,
la variable *\$gamesinfo* définit le jeu à sélectionner selon son
game_id dans le modèle **GamesInfo** si le jeu dispose d'une entrée dans
la table de ce modèle. Les autres variables sont définies comme *null*
après celles-ci sont définies comme telles par défaut au cas où le jeu
ne dispose pas d'entrée dans la base de données, ce qui provoquerait des
bugs car ces variables sont définies dans la boucle IF et sont envoyées
en paramètres pour la vue.

Dans la boucle IF, les modèles liés à **GamesInfo** sont enregistrés
dans chacune des variables en sélectionnant selon l'id récupéré depuis
**GamesInfo**. Les modèles **GamesImage**, **GamesPegi** et
**GamesConfig** sont récupérés et stockés dans des variables selon l'id
du jeu de **GamesInfo**. Les variables sont envoyées vers la vue
*jeux-show* dans le dossier *website* pour être affichées dans la fiche
de jeu (description, images, vidéo, configuration, PEGI\...).

Pour voir d'autres examples, se référer à dans les Annexes.

## Jeu d'essai

Example d'ajout d'informations sur le jeu Divinity Original Sin. En
cliquant sur *Ajouter des informations*, l'utilisateur accède à une page
où se trouve un formulaire présentant plusieurs champs, certains
disposant de limitations (comme URL pour le champs du site du
développeur ou uniquement des images pour le téléversement d'images du
jeu). Si les champs sont correctement remplis, aucun n'étant
obligatoire, la fiche est mise à jour et l'utilisateur est renvoyé sur
celle-ci avec un message exprimant que la mise à jour est effectuée.

![Fiche de base](assets/images/divinity-1.jpg){#fiche-base}

![Formulaire](assets/images/formulaire.jpg){#formulaire}

![Fiche complétée](assets/images/divinity-3.jpg){#fiche-completee}

Si le formulaire n'est pas rempli correctement, un message d'erreur est
renvoyé, soit par l'input, soit par Laravel. Ici, l'utilisateur a tenté
d'envoyer une image qui n'était pas sous un format accepté.

![Erreur à cause du format de
l'image](assets/images/divinity-4.jpg){#erreur-format-image}

## Description de la veille effectuée

Le projet m'a été confié par Antoine Le Gonidec qui avait des demandes
bien claires, dont celle de travailler avec Laravel et d'utiliser Blade
pour le frond-end et d'utiliser le framework pour réaliser le back-end,
tout en utilisant l'API pour récupérer les données de base. La question
de la sécurité est importante mais, grâce à Laravel, celle-ci est gérée
en interne, pour peu que le développement respecte les options proposées
par Laravel. J'ai donc utilisé les moyens proposés par Laravel pour les
formulaires comme le CSRF et l'assignation de masse d'Eloquent pour
limiter au maximum les risques d'injection SQL, ce qui est la base de la
sécurité. Les routes sécurisées sont protégées par l'authentification de
Laravel.

La création de compte a été désactivée et le seul compte accessible est
défini dans le *.env* par l'administrateur du site web. Les informations
sont ensuite envoyée dans un seeder (où on ne voit pas les identifiants)
qui va créer le compte utilisateur en base de données en effectuant un
*Hash* sur le mot de passe par Laravel. Les identifiants du compte sont
donc sécurisés autant qu'il est possible de le faire, ils sont
modifiables sur le serveur si on relance le seeder et cela correspond à
la demande du client.

## Description de la situation de travail

Lorsque mon client m'a demandé de ne pas utiliser du JavaScript pour les
fonctions vitales, j'ai dû innover pour trouver des possibilités pour
tout ce qui utilise d'habitude du JS. Pour le carousel, j'ai dû le faire
en CSS 3 avec une limite de trois images au maximum qui tournent
continu. Si l'utilisateur ajoute des images qui n'ont pas le même ratio,
le carousel va fonctionner mais afficher les images en se basant sur
celle qui a le plus grand ratio et laisser des bandes blanches lorsque
les images au ratio plus petit défilent.

Cela posait un problème esthétique que je ne pouvais pas ignorer et j'ai
dû utiliser *Image Intervention* qui est une librairie pour
redimensionner les images par Laravel. J'ai recherché sur plusieurs
sites anglophones dont Stack Overflow et la documentation de
*Intervention Image* pour trouver comment redimensionner les images en
me basant sur une dimension donnée en gardant le ratio de l'image, même
s'il fallait la rogner par la suite. Tout cela afin que le carousel
fonctionne correctement.

Une autre possibilité aurait été d'utiliser du jQuery pour afficher
l'image uploadée et demander à l'utilisateur de la recadrer d'après un
ratio prédéfini mais cela demandait du JavaScript et je devais avoir une
verison de la fonctionnalité qui pouvait être utilisée sans JS.
Actuellement, la fonctionnalité n'utilise que du PHP et redimensionne
automatiquement les images à un ratio de 1280px de large puis va rogner
l'image si la hauteur dépasse les 720px. Mais dans une verison future,
je pense ajouter la possibilité de rogner soi-même l'image, ce qui
demandera du JavaScript, mais avec la possibilité de traiter par défaut
l'image si le JS a été désactivé.

## Extrait du site traduit

L'extrait de site traduit est basé sur la situation exprimée dans le ,
redimensionner une image en gardant son ratio lors de sa sauvegarde
suite à un upload.

Référence
[stackoverflow.com/questions/40467909/resizing-with-a-max-width-height-while-keeping-the-original-ratio-using-interven](https://stackoverflow.com/questions/40467909/resizing-with-a-max-width-height-while-keeping-the-original-ratio-using-interven)
:

**ORIGINAL :** **Resizing with a max width/height while keeping the
original ratio using Intervention Image in Laravel**

*Message \#1 of Felix Maxime (7 Nov 2016 at 14:44)*

I want to constrain an image only if they exceed either maximum
dimensions while keeping the original ratio intact.

So let's say my parameters are a max height and width of 600. An image
of 1000x1000 would become 600x600, simple enough. An image of 2000x1000
would become 600x300. This would mean that the highest of the two values
becomes 600, while the other gets constrained proportionally. Something
like this *\*Bloc de code\**\
What would be the best way to go about this?

EDIT:

As per the comments, I tried this: *\*Bloc de code\**\
This does not work. Only the first resize is applied, which in this case
is width. HOWEVER, turns out the original code does work. I simply
assumed it wouldn't, but it does.

------------------------------------------------------------------------

\
*Message \#2 of Saumya Rastogi (7 Nov 2016 at 14:48)*

As according to the Image Intervention Docs, you can do this in 3 simple
ways *\*Bloc de code\**\
Hope this helps\...

Commentaires I mean, not really. Are you saying that I should first
constrain the height then the width? In a 2000x1000 scenario with 600
max, would this bring it to 600x300? (*Felix Maxime*)

\@FelixMaxime Yup in the case of defined width, the width parameter is
compromised and the solution would be 600x300 whereas if the height is
defined and width is given as null then the height param is compromised
to 1200x600. (*Saumya Rastogi*)

It doesn't work. It only does the first one (in this case, width).
(*Felix Maxime*)

------------------------------------------------------------------------

\
**TRADUCTION :** **Redimensionner avec une hauteur/largeur maximum en
conservant le ratio original en utilisant *Intervention Image* dans
Laravel.**

*Message \#1 de Felix Maxime (7 Nov 2016 à 14:44)*

Je souhaite contraindre image à respecter des dimensions maximales
seulement si la hauteur ou la largeur les excèdent, tout en gardant son
ratio orginal intact.

Disons que mes paramètres sont une hauteur et une largeur de 600px
maximum. Une image de 1000x1000px sera donc de 600x600px, facile. Une
image de 2000x1000 sera donc de 600x300. Cela signifie que la plus
grande valeur deviendra 600px alors que l'autre sera contrainte à
respecter les proportions. Quelque chose comme ça : \*Bloc de code\*\
Quel est la meilleure technique pour y parvenir ?

EDITION :

En me basant sur les commentaires, j'ai essayé ça : *\*Bloc de code\**\
Cela ne fonctionne pas. Seule la première dimension est appliquée, qui
dans ce cas est la largeur. Cependant, le code original fonctionne. Je
suis simplement parti d'une constat que non, mais c'est le cas.

------------------------------------------------------------------------

\
*Message \#2 de Saumya Rastogi (7 Nov 2016 à 14:48)*

En me basant sur la documentation d'Image Intervention, tu peux le faire
de trois façons différentes *\*Bloc de code\**\
J'espère que cela va t'aider\...

Commentaires Pas vraiment. Dis-tu que je devrais contraindre la hauteur
plutôt que la largeur ? Dans un scénario 2000x1000 avec 600px maximum,
tu amènerais ça comment à 600x300 ? (*Felix Maxime*)

\@FelixMaxime Ouai, dans le cas d'une largeur définie, le paramètre de
la largeur est compromis, ce qui donne 600x300 alors que si la hauteur
est définie et que la largeur est donnée comme null alors le paramètre
de hauteur est compromis à 1200x600. (*Saumya Rastogi*)

Cela ne fonctionne pas. Sauf dans le premier cas (du coup, la largeur).
(*Felix Maxime*)

------------------------------------------------------------------------

\

# Annexes

## Réalisations du projet - Présentation supplémentaire

#### Resize dans la Fonction update() du GamesController

``` {.php language="PHP"}
$gamesinfo = GamesInfo::find($game_id);
$gamesinfo_id = $gamesinfo->id;

$gamespegi = GamesPegi::where('info_id', '=', $gamesinfo_id)->first();
$gamesinfotag = DB::table('games_info_tag')->where('games_info_id', '=', $gamesinfo_id)->get();
$gamestag = GamesTag::all();
$gamesimage = GamesImage::where('info_id', $gamesinfo_id)->first();
// $gamesimage = DB::table('games_image')->where('info_id', $gamesinfo_id)->get();

$validatedInfo = $request->validate([
    'game_id' => 'required',
    'description' => 'nullable|string|max:3000',
    'release' => 'nullable|date',
    'franchise' => 'nullable|string|max:255',
    'developer' => 'nullable|string|max:255',
    'official_website' =>  'nullable|url|max:255',
    'forum_link' => 'nullable|url|max:255',
    'review_link' => 'nullable|url|max:255',
]);
GamesInfo::whereId($game_id)->update($validatedInfo);

$validatedPegi = $request->validate([
    'pegi_age' => 'nullable|string|max:10',
    'bad_language' => 'nullable|integer|max:1',
    'discrimination' => 'nullable|integer|max:1',
    'drugs' => 'nullable|integer|max:1',
    'fear' => 'nullable|integer|max15',
    'gambling' => 'nullable|integer|max:1',
    'in_game_purchase' => 'nullable|integer|max:1',
    'sexual' => 'nullable|integer|max:1',
    'violence' => 'nullable|integer|max:1',
]);
$gamepegi = GamesPegi::where('info_id', $gamesinfo_id)->update($validatedPegi);
```

Cette boucle IF est dans la fonction **update()**, si l'utilisateur
ajoute une prmeière impression d'écran, celle-ci est redimensionnée si
sa largeur n'est pas égale à 1280px, en gardant son ratio. Puis elle est
rognée à 720px de haut pour faire en sorte qu'elle ait une taille
standard pour rentrer dans le carousel. Ainsi toutes les impressions
d'écran sont vérifiées pour qu'elles aient toutes les mêmes dimensions
pour que le carousel affiche les images correctement.

La suite du code permet de créer le répertoire avec le nom du game_id
pour sauvergader l'image dans le dossier souhaité lors de l'upload,
l'image est aussi renommée *screenshot_01.jpg* pour qu'un autre upload
écrase la précédente.

#### Carousel en CSS dans la vue fiche-show

``` {.php language="PHP"}
@if (isset($gamesimage[0]->screenshot1))
    @if ($gamesimage[0]->screenshot2 != null)
        @if ($gamesimage[0]->screenshot3 != null)
            <div id="main">
                <div class="slider">
                    <ul>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                                }}" />
                        </li>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_02.jpg")
                                }}" />
                        </li>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_03.jpg")
                                }}" />
                        </li>
                    </ul>
                </div>
            </div>
        @else
            <div id="main">
                <div class="slider">
                    <ul>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                                }}" />
                        </li>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_02.jpg")
                                }}" />
                        </li>
                        <li>
                            <img src="{{ 
                                    asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_02.jpg")
                                }}" />
                        </li>
                    </ul>
                </div>
            </div>
        @endif
    @else
        <div id="main">
            <div class="slider">
                <ul>
                    <li>
                        <img src="{{ 
                                asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                            }}" />
                    </li>
                    <li>
                        <img src="{{ 
                                asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                            }}" />
                    </li>
                    <li>
                        <img src="{{ 
                                asset("image/database/games/".$gamesinfo[0]->game_id."/screenshot_01.jpg")
                            }}" />
                    </li>
                </ul>
            </div>
        </div>
    @endif
@else
    @if (isset($gamesimage[0]->banner))
        <img class="my-2" src="{{ 
                asset("image/database/games/".$gamesinfo[0]->game_id."/banner.jpg")
            }}" alt="{{ 
                $games[0]->game_name
            }}" />
    @endif
@endif
</div>
```

Le carousel a dû être réaliser en CSS comme c'était une fonction
importante du site web. Les boucles IF ELSE permettent de gérer
plusieurs cas : si l'utilisateur n'a ajouté qu'une seule impression
d'écran, deux, trois ou aucune afin d'éviter tous les bugs potentiels.
Si une seule impression d'écran a été ajoutée, le carousel tourne en
boucle sur la même, s'il y en a deux, le carousel répète la deuxième
deux fois sur les trois et enfin s'il y en a trois, le carousel tourne
sur les trois en continu.

S'il n'y a aucune impression d'écran, c'est la bannière qui est
affichée, si elle est présente, dans le cas contraire, rien ne s'affiche
sous le titre du jeu.

#### Enregistrement des informations dans la fonction update() du GamesController

``` {.php language="PHP"}
$gamesinfo = GamesInfo::find($game_id);
$gamesinfo_id = $gamesinfo->id;

$gamespegi = GamesPegi::where('info_id', '=', $gamesinfo_id)->first();
$gamesinfotag = DB::table('games_info_tag')->where('games_info_id', '=', $gamesinfo_id)->get();
$gamestag = GamesTag::all();
$gamesimage = GamesImage::where('info_id', $gamesinfo_id)->first();
// $gamesimage = DB::table('games_image')->where('info_id', $gamesinfo_id)->get();

$validatedInfo = $request->validate([
    'game_id' => 'required',
    'description' => 'nullable|string|max:3000',
    'release' => 'nullable|date',
    'franchise' => 'nullable|string|max:255',
    'developer' => 'nullable|string|max:255',
    'official_website' =>  'nullable|url|max:255',
    'forum_link' => 'nullable|url|max:255',
    'review_link' => 'nullable|url|max:255',
]);
GamesInfo::whereId($game_id)->update($validatedInfo);

$validatedPegi = $request->validate([
    'pegi_age' => 'nullable|string|max:10',
    'bad_language' => 'nullable|integer|max:1',
    'discrimination' => 'nullable|integer|max:1',
    'drugs' => 'nullable|integer|max:1',
    'fear' => 'nullable|integer|max15',
    'gambling' => 'nullable|integer|max:1',
    'in_game_purchase' => 'nullable|integer|max:1',
    'sexual' => 'nullable|integer|max:1',
    'violence' => 'nullable|integer|max:1',
]);
$gamepegi = GamesPegi::where('info_id', $gamesinfo_id)->update($validatedPegi);
```

Le formulaire permettant d'ajouter des informations pour les jeux est
traité par la fonction **update()** **GamesController**. Je récupère
l'id du jeu dans la base de données grâce à son game_id (qui est de base
dans le formulaire en non-modifiable), pour sélectionner les entrées
dans les tables liées **GamesPegi**, **GamesTag**, **GamesImage**.

Puis les données sont traitées par *validate* de Laravel afin que les
champs correspondent à ce qui est attendu en base de données pour
**GamesInfo** puis les données validées sont *update()* par le modèle.
**GamesPegi** est traité de la même manière, tout comme les modèles qui
sont liés, seules les images sont traitées d'une manière différente pour
vérifier qu'elles ont toutes le même ratio.

## Maquette

![pageflow de ./play.it](assets/images/intro.jpg){#pageflow
width="100%"}

![pageflow de ./play.it](assets/images/accueil.jpg){#pageflow
width="100%"}

![pageflow de ./play.it](assets/images/jeux.jpg){#pageflow width="100%"}

[^1]: digital rights management (ou gestion des droits numériques) ont
    pour objectif de contrôler l'utilisation qui est faite des œuvres
    numériques. Pour les jeux vidéo, cela restreint souvent leur
    utilisation via une plateforme spécifique comme avec Steam ou
    Origin.

[^2]: digital rights management (ou gestion des droits numériques) ont
    pour objectif de contrôler l'utilisation qui est faite des œuvres
    numériques. Pour les jeux vidéo, cela restreint souvent leur
    utilisation via une plateforme spécifique comme avec Steam ou
    Origin.

[^3]: Arch Linux et Debian sont des distributions de GNU/Linux.

[^4]: interface de programmation applicative (souvent désignée par le
    terme API pour application programming interface) est un ensemble
    normalisé de classes, de méthodes ou de fonctions qui sert de façade
    par laquelle un logiciel offre des services à d'autres logiciels.

[^5]: modèles pour les fiches de chaque jeu avec une interface
    permettant de les remplir facilement pour les ajouter à la base de
    données.

[^6]: élément qui reste collé sur le haut de la page web pour que
    l'utilisatrice puisse y avoir accès de n'importe où.

[^7]: sections qui sont ouvrables/fermables comme des intercalaires dans
    un classeur dans une même page afin que le contenu ne prenne pas
    trop de place.

[^8]: type de menu qui s'ouvre via un bouton qui généralement ressemble
    à trois lignes superposées, indiquant qu'il est possible d'avoir un
    menu1 burger : type de menu qui s'ouvre via un bouton qui
    généralement ressemble à trois lignes superposées, indiquant qu'il
    est possible d'avoir un menu.
