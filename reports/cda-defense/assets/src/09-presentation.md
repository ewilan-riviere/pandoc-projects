# Présentation de la fonctionnalité principale

La fonctionnalité principale de l'application ne peut pas être réellement définie, on distingue davantage plusieurs fonctionnalités d'importance similaire mais la fonctionnalité principale pour le client est la souscription à un fond d'investissement.

Si un fond disponible à la souscription existe, celui-ci apparait dans la liste des fonds en cours de commercialisation sur la page d'accueil. Un bandeau rouge indique que c'est un fond disponible, si l'utilisateur tape dessus, il n'aura pas les détails du fond comme pour un fond auquel il a déjà souscrit mais un formulaire de souscription.  

## Accès à un nouveau fond

![Un nouveau fond est disponible](assets/images/feature/feature-1.jpg)

Lorsque l'utilisateur arrive sur un nouveau fond, il peut lire le texte de présentation et télécharger le teaser en PDF qui donne des informations sur le fond et pourquoi l'investisseur pourrait être intéressé. S'il continue, il arrive sur le formulaire de souscription.

## Souscrire à un fond

![Remplir le formulaire de souscription](assets/images/feature/feature-2.jpg)

L'utilisateur devra remplir tout le formulaire qui possède plusieurs champs : des sélecteurs, des champs de texte et un champs de date. Une fois cela fait, il peut valider sa souscription et si tout se passe bien, il arrivera sur la page de fin de la souscription avec la possibilité de télécharger le bon de souscription et de retour à la liste des fonds.

## Valider la souscription

![Envoyer le bon de souscription](assets/images/feature/feature-3.jpg)

Le nouveau fond sera disponible dans la liste des fonds de l'investisseur, si celui-ci tape dessus, il verra les détails du fond et également deux boutons disponibles tant qu'il n'aura pas envoyé le bon de souscription signé. Il peut le télécharger s'il ne l'a pas fait depuis la page précédente, celui-ci est généré depuis le back-end. Il le récupère sur son téléphone et le signe par ses propres moyens. Il peut ensuite le téléverser et s'il est validé cela sera indiqué, sachant que les administrateurs peuvent consulter ce fichier depuis le back-office et valider manuellement bien que la validation soit effective par défaut.

Cette fonctionnalité présente

- plusieurs vues
- un formulaire complexe
- deux communications avec le back-end via une requête POST
- des téléchargements multiples de fichiers PDF depuis le back-end
- un rechargement des informations suite à une action asynchrone
- des notifications de téléchargement
