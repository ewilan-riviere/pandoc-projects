# Liste des compétences du REAC

## Activité type 1

**« Concevoir et développer des composants d'interface utilisateur en intégrant les recommandations de sécurité »**

- **Maquetter une application** : *NON COUVERT*, les maquettes de ce projet ont été designées par la designeuse de Useweb
- **Développer des composants d’accès aux données** : *COUVERT*, Laravel propose un ORM permettant de gérer les données à travers le concept MVC
- **Développer la partie front-end d’une interface utilisateur web** : *COUVERT*, Laravel avec la partie Vue.js embarquée et l'application Nuxt.js
- **Développer la partie back-end d’une interface utilisateur web** : *COUVERT*, Laravel avec le concept MVC proposant des modèles et des contrôleurs permettant de persister les données et de créer une API

## Activité type 2

**« Concevoir et développer la persistance des données en intégrant les recommandations de sécurité »**

- **Mettre en place une base de données** : *COUVERT*, MySQL avec la mise en place de migrations via Laravel avec des seeders[^1]. La base de données existait déjà lors de la reprise du projet, cependant il a été nécessaire de mettre en place de nouvelles tables et d'insérer de nouvelles colonnes dans les tables existantes, toutes les relations d'une base de données relationnelle sont représentées (Many to Many, Many to One, One to One)
  
## Activité type 3

**« Concevoir et développer une application multicouche répartie en intégrant les recommandations de sécurité »**

- **Concevoir une application** : *COUVERT*, Flutter pour créer une application mobile cross-platform Android et iOS
- **Développer des composants métier** : *COUVERT*, Laravel avec ses modèles répondant aux demandes du client dans le domaine de l'investissement et Flutter avec l'adaptation aux modèles du back-end pour récupérer les données selon les besoins mobile
- **Construire une application organisée en couches** : *COUVERT*, Laravel par son concept MVC et Flutter par ses modèles, ses appels API et ses vues. Flutter est souvent utilisé avec le concept BLoC[^5] afin de simplifier le développement, sur le projet présent, ce concept n'a pas été mis en place car le temps nécessaire à l'apprentissage du framework n'a pas laissé assez de marge pour appréhender le concept BLoC, qui pourra être mis en place à l'avenir.
