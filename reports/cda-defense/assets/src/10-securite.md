# Sécurité {#chapter-securite}

## Présentation

La sécurité a été au coeur du projet car les données utilisées sont privées et concernent la finance, par conséquent on ne peut se permettre de voir des données compromises. Laravel étant le framework utilisé pour le back-end, il était superflu de chercher à créer une sécurité par nous-mêmes, autant utiliser les paquets proposés par Laravel car ceux-ci ont été validés par la communauté. Le caractère open-source de Laravel nous permet de nous assurer que plusieurs développeurs ont pu voir et valider la manière dont le framework sécurise les données et donc faire émerger plus facilement les failles qui pourraient exister. Si le framework propose plusieurs paquets, c'est le plus récent qui a été sélectionné : **Sanctum**. Cette sécurité existera côté front-end et côté application mobile avec deux systèmes différents, tous deux liés à Sanctum.

> **Extrait de la documentation de Laravel** : Laravel Sanctum fournit un système d'authentification léger pour les SPA (applications à page unique), les applications mobiles et les API simples basées sur des jetons. Sanctum permet à chaque utilisateur de votre application de générer plusieurs jetons d'API pour son compte. Ces jetons peuvent se voir attribuer des capacités ou des champs d'application qui spécifient les actions que les jetons sont autorisés à effectuer.

Concernant les autres failles de sécurité, elles sont gérées par Laravel de manière automatique lors du traitement des données qui peuvent provenir des formulaires utilisés.

- CSRF (*cross site request forgery*) : pour gérer les fausses requêtes effectuées depuis les formulaires Laravel propose un jeton dans chaque appel asynchrone, toutes les routes sont protégées par défaut, on peut les retirer manuellement de cette protection si c'est voulu.
- XSS (*cross site script*): pour gérer du JavaScript qui serait inséré dans les formulaire, Laravel va *purifier* les données avec une méthode `validate()` lors de leur récupération, tout caractère lié à du code sera remplacé ou supprimé.
- SQL injection : Eloquent, l'ORM de Laravel, utilise le `PDO binding` pour protéger les requêtes effectuées, tant que le développeur n'utilise pas de requêtes avec `DB` qui permet de faire du SQL.
- HTTPS : l'API est disponible en HTTPS, le certificat SSL permet de s'assurer du chiffrement des données.

Pour le front-end et l'application, leur sécurité découle de celle du back-end :

- Application : un *sas* a été mis en place, tant qu'aucun jeton n'est disponible, l'utilisateur ne peut pas accéder à l'accueil, il doit se connecter d'abord. Et si c'était quand même le cas, il n'aura accès à aucune donnée sensible puisque celles-ci sont récupérée par appel asynchrones en utilisant un jeton pour l'authentification à **chaque** appel. En cas de compromission de compte, il est possible de détruire le jeton si besoin.
- Front-end : créé en Nuxt.js, le paquet **nuxt/auth** a été utilisé, c'est un paquet officiel de la communauté pour gérer l'authentification avec plusieurs frameworks différents.

## Recherche effectuée

La recherche principale a d'abord été l'installation de Sanctum, si la [documentation officielle](https://laravel.com/docs/8.x/sanctum#installation) indique un simple ajout de dépendance à Composer, ce n'est pourtant pas aussi simple :

```bash
composer require laravel/sanctum
```

Pour utiliser Sanctum, il était nécessaire de mettre à jour la version de Laravel utilisée dans le projet, comme expliqué sur [cette réponse de StackOverflow](https://stackoverflow.com/questions/67046976/laravel-composer-error-while-install-sanctum-conflict-with-contracts), qui était sur la version `5.8` alors que Sanctum ne pouvait fonctionner qu'en `6.0`. Le changement de version étant sur une majeure, il était possible de rencontrer certains changements qui pouvaient casser certaines fonctionnalités.

Toutefois, la mise à jour du framework n'a pas créé de conflits et l'installation de Sanctum a pu se réaliser sans problèmes. Cependant, il a été choisi de ne pas ajouter **Fortify** qui est un paquet généralement associé avec Sanctum permettant de générer les routes d'authentification et les gérer par des contrôleurs par défaut. Cette dépendance demandait une version bien plus récente de Laravel qui entrainait des problèmes majeurs. S'ils n'étaient pas insurmontables, le manque de temps a conduit à créer des routes et des contrôleurs manuellement en utilisant Sanctum comme **Middleware**.

## Conclusion

Si l'ajout de Sanctum n'est pas réellement un problème tant que la version de Laravel n'est pas trop ancienne, le vrai travail réside dans la création des routes et des contrôleurs mais aussi dans l'utilisation concrète de Sanctum en tant que protection d'une part au travers du web front-end mais aussi de l'application avec dans chacun des cas, une authentification différente.
