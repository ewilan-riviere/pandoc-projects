# Résumé du projet

L'application Foncière AALTO est un produit demandé par Foncière AALTO qui est une entreprise d'investissements et gestions d'actifs immobilier. Celle-ci accompagne des investisseurs privés et institutionnels pour qu'ils investissent dans des biens, en proposant des solutions de placements immobiliers, apportant des conseils et des accompagnements pour la gestion d’actifs. Le but de ces investissements est d'optimiser le patrimoine et assurer des revenus complémentaires, voir de proposer à ses membres de devenir des associés. L'entreprise gère un parc total de 200 000 m² de locaux d’activités et d’immeubles de bureaux, sur l’ensemble de la France. Les locataires sont des entreprises de renommée nationale et internationale.

Foncière AALTO est le département investissement du groupe CAP Transactions @cap-transactions. Ce groupe intervient sur l’ensemble de la chaîne immobilière et propose des solutions sur-mesure quels que soient les actifs.

---

Un premier projet @fonciere-aalto pour Foncière AALTO a été développé en Wordpress par une entreprise différente de Useweb, celle-ci s'est occupée de gérer un intranet en 2018 disponible uniquement pour les gestionnaires de l'entreprise mais aussi de ses investisseurs. La partie gestionnaire présente un back-office proposant des tables pour gérer les données : fonds d'investissement, actifs, investisseurs... et la partie investisseur qui propose la liste des fonds d'investissement auxquels l'investisseur participe, ainsi que les données financières associées, et leurs informations personnelles. A noter que les deux interfaces étaient embarquées par le même projet, développé en Laravel 6 (PHP), ce système sera modifié par la suite.

Si Foncière AALTO possédait déjà un intranet responsive, il désirait également une application mobile afin de pouvoir proposer une expérience utilisateur plus efficience sur ce support (en particulier avec la proposition de notifications push). Il y a eu un temps de débat et d'analyse pour choisir la technologie qui serait utilisée avant de choisir Flutter soutenu par le développeur Adrien Beaudouin et moi-même.

Ce projet m'a apporté des compétences dans le développement d'application mobiles avec Flutter dont l'efficience est proche des applications natives mais avec l'immense avantage d'être portable sur Android & iOS avec la même base de code. C'était un domaine qui m'intéressait depuis longtemps et Flutter s'est montré très agréable à prendre en main proposant une communauté dynamique et vivante.

La position de ne pas avoir de personne ressource attitrée en cas de problème a été assez perturbant au début, et j'ai donc eu besoin de temps pour appréhender le framework Flutter et j'ai eu tout à apprendre par moi-même, avec l'aide de mon équipe sur des points conceptuels si nécessaire. Tout cela a donc fortement renforcé mon autonomie, mon appétance de nouvelles technologies et ma compétence de recherche pour trouver des solutions.

En plus de devoir développer cette application, j'ai développé une API depuis le projet originel afin que les données dispensées sur l'interface de l'investisseur puissent être récupérée sur l'application, l'interface web étant interne à Laravel aucune API n'a eu besoin d'être développée dans un premier temps (elle sera découplée par la suite). De plus, du fait de la sensibilité des données, cette API devait être nécessairement sécurisée. Et additionnellement à cela, j'ai eu des modifications à apporter sur l'interface disponible pour les gestionnaires en ajoutant certaines fonctionnalités ce qui m'a amenée à modifier le schéma de la base de données utilisée.

Après le développement de l'application, j'ai été amenée à créer une interface investisseur séparée du projet Laravel directement en NuxtJS et utilisant la même API que l'application. La maintenance et l'ajout de nouvelles fonctionnalités à l'application continuent encore aujourd'hui.

- Application Android @fonciere-aalto-android disponible sur Google Play
- Application iOS @fonciere-aalto-ios disponible sur l'App Store
