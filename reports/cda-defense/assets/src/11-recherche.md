# Recherche

## Besoin d'information

Dans Flutter, il est nécessaire de redéfinir les modèles qui seront récupérés depuis l'API. Sans être une copie des modèles en back-end, on retrouve tout de même une partie des modèles existants avec leurs relations. Ce qui différencie le PHP du Dart est surtout le typage fort que l'on retrouve en Dart, par conséquent si le PHP peut fournir une valeur qui peut varier depuis son API, comme un *double* qui peut être un *int* par exemple, ce n'est pas le cas du Dart.

Ceci étant le premier problème, il s'agit de forcer PHP à fournir toujours le même type de valeur en utilisant des méthodes natives pour s'assurer du type de chaque attribut. De plus, définir le modèle dans l'application n'est pas suffisant, il faut définir la récupération des données depuis une API en JSON. Cela demande de créer une méthode depuis du JSON vers du Dart et du Dart vers du JSON. Si l'on effectue cela manuellement, il faut reprendre chaque attribut à chaque fois, un modèle comprend donc quatre fois le même attribut :

- Son type avec son nom
- Dans le constructeur du modèle
- Dans la méthode JSON -> Dart
- Dans la méthode Dart -> JSON

Cela entraine bien sûr un problème majeur, tout changement de l'API génère rapidement des problèmes en profondeur où le modèle doit être modifié mais aussi ses méthodes. Et cela se complique en cas de valeurs nulles car les premières versions de Flutter n'étaient pas résistantes à la nullité des valeur et cela devait être testé ou générait une erreur.

Pour rendre la gestion des modèles plus fluide et la récupération des données moins capricieuse, il fallait trouver une solution pour gérer les méthodes de récupération des données mais aussi pour mieux gérer la potentielle valeur nulle de certaines valeurs.

## Solution

La [documentation officielle](https://flutter.dev/docs/development/data-and-backend/json) de Flutter indique deux possibilités pour gérer la sérialisation des données :

>Cet article couvre deux stratégies générales pour travailler avec JSON :
>
>- Sérialisation manuelle
>- Sérialisation automatisée par génération de code
>
>La complexité et les cas d'utilisation varient d'un projet à l'autre. Pour les petits projets de validation de concept ou les prototypes rapides, l'utilisation de générateurs de code peut s'avérer superflue. Pour les applications comportant plusieurs modèles JSON plus complexes, l'encodage à la main peut rapidement devenir fastidieux, répétitif et se prêter à de nombreuses petites erreurs.

Le projet se situe donc dans le deuxième cas, avec de nombreux modèles complexes. La documentation indique qu'il faut ajouter la dépendance `json_serializable` afin de pouvoir générer les méthodes de sérialisation avec des annotations gérées par `json_annotation`. Le tout étant exécutable par `build_runner` permettant l'utilisation d'une commande depuis le terminal : `flutter pub run build_runner watch --delete-conflicting-outputs`.

## Mise en oeuvre

Si l'on prend le modèle `Local`, on peut observer comment fonctionne la sérialisation automatique. L'ajout de `part '../../../title-cda/assets/src/Local.g.dart'` indique au `json_serializable` le nom du fichier à créer, l'annotation `@JsonSerializable()` est placée sur la classe concernée, `_$LocalFromJson` et `_$LocalToJson(this)` sont des méthodes générées directement par le `build_runner` qu'il faut appeler manuellement. Le fichier généré présentera l'exécution des méthodes précédentes qui se basent sur les attributs de la classe. Il est possible d'ajouter des détails très différents en utilisant `JsonKey()` au-dessus d'un attribut comme par exemple, indiquer que le nom de l'attribut dans l'API est différent de celui de l'objet Dart.

```java
part '../../../title-cda/assets/src/Local.g.dart';

@JsonSerializable()
class Local {
  Local({
    required this.id,
    this.label,
    this.type,
    this.surface,
    // ...
  });

  int id;
  String? label;
  String? type;
  int? surface;
  // ...

  factory Local.fromJson(Map<String, dynamic> json) => _$LocalFromJson(json);
  Map<String, dynamic> toJson() => _$LocalToJson(this);
}
```

Maintenant tous les modèles du projet possèdent une sérialisation automatique et le moindre changement n'entraine pas une remise en cause de la sérialisation. On peut également ajouter que le projet a été passé en `null-safety` comme l'indique le `?` pour chaque attribut `String? label` qui permet de pousser Dart à considérer que ces attributs peuvent être null et par conséquent d'adapter la sérialisation.
