# Bibliographie

::: {#refs}
:::

# Annexes

## Schémas supplémentaires de la base de données {#annexes-schemas-database}

Ces tables sont générées par Laravel pour les besoins du framework avec les `jobs` (requêtes asynchrones), l'historique de la base de données avec `migrations` et toute l'authentification avec `sessions`, `password_resets`... Ces tables n'ont pas été modifiées et sont gérées entièrement par le framework.

![Tables générées](assets/images/database/database-generated.jpg)

## IHM

### Formulaire de connexion

```java
TextEditingController mailController = TextEditingController(text: '');
TextEditingController passwordController = TextEditingController(text: '');
bool saveCredentialsController = false;

class LoginScreen extends StatefulWidget {
  final bool saveCredentials;

  const LoginScreen({Key? key})
      : super(key: key);
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _node = FocusScopeNode();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _node.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Form(
        key: _formKey,
        child: FocusScope(
          node: _node,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        'Adresse mail',
                        style: GoogleFonts.montserrat(
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    padding: const EdgeInsets.all(8),
                  ),
                  AppInput(
                    controller: mailController,
                    formatters: [
                      LowerCaseTextFormatter(),
                    ],
                    textInputType: TextInputType.emailAddress,
                    onEditingCompleteMethod: _node.nextFocus,
                    textInputAction: TextInputAction.next,
                    autofocus: false,
                  ),
                ],
              ),
              // ...
              Align(
                alignment: Alignment.centerLeft,
                child: Stack(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: TextButton(
                        child: Text(
                          'Mot de passe oublié ?',
                          style: GoogleFonts.montserrat(
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                              shadows: [
                                Shadow(
                                  blurRadius: 1.0,
                                  color: Colors.white,
                                  offset: Offset(1, 1),
                                ),
                              ]),
                        ),
                        onPressed: () {},
                      ),
                    ),
                    Positioned(
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      child: Container(
                        color: Colors.transparent,
                        child: AnimatedContainer(
                          duration: Duration(seconds: 2),
                          curve: Curves.easeIn,
                          child: Material(
                            color: Colors.transparent,
                            child: new InkWell(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        ForgotPasswordScreen(),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              // ...
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: DiagonalButton(
                  callback: () {
                    _attemptLogIn(context);
                    FocusScope.of(context).unfocus();
                  },
                  widget: Icon(
                    Icons.arrow_forward,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
}
```

**Version courte de l'interface de connexion**

### Front-end : page des fonds

```html
<template>
  <div v-if="companies">
    <h1 class="text-center text-lg-left">Vos investissements</h1>

    <div v-if="companies.length">
      <nuxt-link
        v-for="company in companies"
        :key="company.id"
        class="company-link position-relative"
        :to="{ name: 'investments-id', params: { id: company.id } }"
      >
        <company-card :company="company"></company-card>
      </nuxt-link>
    </div>

    <div v-else>Vous n'avez aucun investissement en cours ou en attente</div>

    <div v-if="checkSubscriptionValid">
      Les SCI en attente de documents nécessitent un téléversement du bon de
      souscription dans l'espace dédié à la SCI et / ou des documents
      administratifs comme votre Carte Nationale d'Identité ou votre RIB dans
      l'espace dédié à vos informations.
    </div>
  </div>
</template>

<script>
import companyCard from '~/components/blocks/company-card.vue'
export default {
  name: 'Investments',
  auth: 'auth',
  components: { companyCard },
  async asyncData({ app }) {
    try {
      const companies = await app.$axios.$get(`companies`)

      let checkSubscriptionValid = companies.data.filter((company) => {
        return !company.shares.is_validate
      })
      checkSubscriptionValid = checkSubscriptionValid.length > 0

      return {
        companies: companies.data,
        checkSubscriptionValid,
      }
    } catch (error) {
      console.error(error)
      return {
        companies: [],
        checkSubscriptionValid: false,
      }
    }
  },
  data() {
    return {
      connectionAfterUpdate: this.$auth.$state.user.investor
        .informations_verified,
    }
  },
}
</script>
```

### Maquettes

![Pages de modification d'informations](assets/images/design/design-forms.jpg)

![Pages d'un article, d'une souscription](assets/images/design/design-more.jpg)

### Aperçus des autres IHM

![Détail d'un fond](assets/images/screenshots/company.jpg)

![Détail d'un actif immobilier](assets/images/screenshots/local.jpg)

![Informations de l'investisseur](assets/images/screenshots/form-informations.jpg)

![Formulaire de souscription](assets/images/screenshots/form-subscription.jpg)

![Détail d'un article](assets/images/screenshots/article.jpg)

[^1]: les seeders dans Laravel permettent de générer des données avec du *lorem ipsum* pour le texte et des valeurs fictives afin de remplir la base de données pour offrir une application fonctionnelle sans aucune donnée client.

[^2]: Modèle Vue Contrôleur, un concept de développement permettant de séparer efficacement les composants d'accès aux données, les contrôleurs permettant de gérer comment les données sont réparties et les vues où sont affichées les données pour l'utilisateur.

[^3]: *Long Time Support*, utilisé pour indiquer que cette version continuera de recevoir les mises à jour de sécurité les plus importantes pendant une longue période, tout projet sur le long terme voit sa maintenance bien plus simple à réaliser s'il est commencé avec une version LTS du framework utilisé.

[^4]: À l'instant où ce dossier est rédigé, en mai 2021, la version 2.0 de Flutter est sortie, apportant son lot de changements et l'application se doit de pouvoir être utilisée avec la dernière version stable de la 2.0 afin de pouvoir facilement être maintenue à long terme.

[^5]: Comme proposé sur ce dépôt: <https://github.com/felangel/bloc> *"L'objectif de cette bibliothèque est de faciliter la séparation de la présentation et de la logique commerciale, afin de faciliter la testabilité et la réutilisation."*

[^6]: un jeton est une suite assez longue de chiffres et de lettre qui est générée aléatoirement liée à un utilisateur de la base de données.
