# Gestion de projet

## Planning et suivi

La première partie du projet s'est passée de septembre 2020 à janvier 2021, les diagrammes de Gantt ci-dessous qui représentent les tâches avec leur suivi s'arrêtent en novembre 2020 car les cours à l'ENI dans le cadre de l'alternance, les vacances et les pauses pour d'autres projets ont été retirés du diagramme.

J'ai été la seule développeuse à être sur le projet, si j'ai pu obtenir des conseils de la part '../../../title-cda/assets/src'd'autres collègues ceux-ci ne sont pas intervenus directement sur le code. De plus aucune personne ressource n'était présente dans l'entreprise, mon apprentissage a dû se faire en même temps que le développement de l'application. Mon manager m'a demandé d'évaluer le temps que je passerai sur chacune des tâches sachant que la partie back-end représentait nombre de tâches qui étaient nouvelles pour moi, en plus de comprendre un projet pré-existant et que pour l'application mobile je n'avais que aucun recul pour pouvoir planifier le temps nécessaire.

![Gantt de la partie back-end](assets/images/gantt/gantt-01.jpg)

Pour le back-end, le planning que j'avais fait a été plus ou moins suivi avec quelques décalages. Le décalage majeur de deux semaines est dû au client qui a décidé de revenir sur certaines fonctionnalités pour faire quelque chose de différent au point où la base de données en était affectée, ce qui a pris du temps de développement pour des fonctionnalités qui ont été laissées de côté et du temps pour répondre aux nouveaux besoins. Le client ayant un retour de sprint toutes les une à deux semaines, le décalage n'a pas été si important mais cela a impacté le temps de développement.

![Gantt de la partie recette](assets/images/gantt/gantt-02.jpg)

Pour l'application mobile, mon manager a compris rapidement que j'aurai besoin de temps pour comprendre les fondamentaux de Flutter avant de pouvoir fournir un prototype viable. Et même quant ça été le cas et que j'ai pu installer première version sur un smartphone, j'ai dû revenir de nombreuses fois sur le code écrit pour l'améliorer et séparer les différentes couches de façons à obtenir un code maintenable sur la longueur. Aujourd'hui, l'application propose un code qui est assez propre mais celui-ci n'intègre pas encore le *BLoC pattern* qui est le concept de développement qui est souvent conseillé sur Flutter. Certaines améliorations, notamment en ce qui concerne la récupération des données API et le stockage dans une base de données locale sont également à explorer, sans pour autant laisser les tests de côté car l'application n'en possède pas actuellement. Si ces dernières possibilités n'ont pas encore été mises en place, c'est pour une question de temps, et si mon manager m'en laisse suffisament, j'espère grandement pouvoir ajouter ces options qui amélioreraient l'application.

![Gantt de la partie application](assets/images/gantt/gantt-03.jpg)

## Pratiques de pilotage

Le développement a été réalisé en mêlant cycle en v et agilité mais séparé suivant différentes étapes, le client a eu un rendu toutes les une à deux semaines et s'il validait, on passait à la suite. Mon manager a été mon référent en cas de questions sur les fonctionnalités à développer ou les problèmes rencontrés (non techniques) quand l'équipe de développeurs était là pour apporter son aide et ses conseils sur les questions techniques. Le client était représenté par une seule personne durant la plus grande partie du développement et celle-ci transmettait les retours nécessaires par l'intermédiaire de mon manager.

![Matrice RACI du projet](assets/images/matrice-raci.jpg)

**R**esponsible (qui exécute la tâche), **A**ccountant (qui approuve), **C**onsulted (qui conseille), **I**formed (qui est informé)

- Développeuse : Ewilan RIVIERE
- Équipe de développeurs : Adrien Beaudouin, Mathieu MONNIER
- Manager : Michel TRAUTH
- Client : Valentin Ratier

La matrice RACI est liée au domaine du management, elle indique les rôle et les responsabilités de chacun sur le projet, les activités sont définies et séparées selon les acteurs qui sont présents.

Les risques les plus importants étaient le respect du temps et les problèmes technique, les deux étant liés à la même question : la découverte de Flutter sans aucune personne ressource. Mon manager a pris en compte ces difficultés, le projet était suffisamment flexible sur la question du temps pour le client (même s'il a attendu plus qu'il ne l'aurait cru) et donc la question technique était la plus importante. Aucun développeur de l'équipe n'avait d'expérience dans le développement d'application mobile et le choix de la technologie a été fait en consultations du lead développeur, du manager et de moi-même. Le plus important était le cross-plateforme car il n'y avait pas de ressources pour développer la même application sur deux plate-formes. La première proposition a été de partir sur du **Native Script** (comme l'équipe fait du Vue) mais la jeunesse de la technologie nous a poussé à considérer des alternatives :

- React Native refusé car l'entreprise ne fait pas de React
- Xamarin refusé car le C# n'était pas pratiqué dans l'entreprise
- Cordova et Ionic refusé à cause de la qualité de l'application plutôt mauvaise
- Flutter présente de nombreux avantages : le dynamique de sa communauté, son efficience et sa solidité. Cette technologie m'a été conseillée par ma soeur qui développe avec celle-ci depuis plus d'un an, le lead développeur l'a expérimenté et l'a validé, enfin le manager m'a demandé mon avis et validé également ce choix de technologie.

Accepter Flutter, c'était apprendre le Dart, le langage utilisé par Flutter, qui est un mélange entre plusieurs concepts d'autres langages très pratiqués ce qui donne une impression de familiarité avec le fonctionnement de Flutter. Cela a demandé tout de même plusieurs jours avant que je puisse avancer efficacement sur ce framework en étant obligée de me former par moi-même sur des questions mobiles que je ne connaissais pas, étant un domaine différent du web.

Cela a été un choix payant de la part '../../../title-cda/assets/src'du manager car l'application obtenue a été qualitative sur l'expérience utilisateur, et, en-dehors, de quelques *refactorings* nécessaires la maintenance à long terme peut se faire sans problème.

## Objectifs de qualité

Pour ce projet, le plus important était le cross-plateforme Android et iOS, la solidité de l'application et l'expérience utilisateur liée a été le second point le plus important, d'où le refus des technologies web pour développer l'application. Enfin le but était de choisir une technologie qui possède une communauté importante et qui continuera à évoluer dans les années à venir. Et il semble que Flutter réponde à ces besoins.

Suite à ce projet, le client a exprimé sa satisfaction, celui-ci considère que l'application répond à ses besoins et demande des ajouts de fonctionnalités après avoir expérimenté l'application. Sur l'apprentissage personnel et l'apport à l'entreprise, on peut également considérer cela comme étant positif car cette expérience ajoute l'option de proposer le développement d'une application mobile à d'autres clients.

On peut considérer que l'application gagnerait à s'améliorer sur plusieurs aspects :

- Amélioration du traitement des réponses de l'API
- Ajout d'une base de données locale pour garder les données accessibles sans appel API en cas de déconnexion
- Ajout de tests unitaires et fonctionnels pour éviter la régression
- Mise en place du concept de développement BLoC qui permet de séparer l'application en couche selon des règles plus précises qu'actuellement

L'ajout de ces améliorations rendra l'application plus solide pour l'utilisateur qui verra moins de bugs et pour le développeur qui pourra faire des ajouts de fonctionnalités et de la maintenance de façon plus fluide.
