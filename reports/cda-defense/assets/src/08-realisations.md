# Réalisations

## Présentation

Les réalisations sont autant du côté back-end que du côté application mobile, les IHM de l'application seront prioritaires par rapport aux IHM de l'application web qui a aussi été concernée par les modifications, plus d'informations dans les annexes.

## Base de données

La base de données pré-existait quand je suis arrivée sur le projet, la première version de 2018 utilisait une base déjà complexe avec toutes les relations possibles présente ainsi que des relations polymorphiques. J'ai dû modifier plusieurs tables existantes et en ajouter certaines.

### Fonds d'investissement : `real_estate_companies`

La table `real_estate_companies` s'est vue améliorée suivant les demandes du client. Les champs ajoutés ont été `headquaters`, `siren`, `rcs`, `subject`, `use`, `notary` pour les informations administratives liées au fond d'investissement. De plus `status` permet de gérer statut du fond, `subscription_limit` limite la date de souscription, `presentation` ajoute un texte de présentation, `subscription_is_launch` est un booléen indiquant si la souscription a été lancée et est disponible pour les investisseurs.

```php
Schema::table('real_estate_companies', function (Blueprint $table) {
    $table->dropColumn('shares_quantity');
    $table->dropColumn('share_price');

    $table->json('shares_quantity');
    $table->json('share_price');
    $table->json('capital');
    $table->string('headquaters');
    $table->string('siren');
    $table->string('rcs');
    $table->string('subject');
    $table->string('use');
    $table->string('notary');
    $table->string('notary_address');
    $table->json('bank');
    $table->string('status');
    $table->date('subscription_limit');
    $table->text('presentation');
    $table->boolean('subscription_is_launch');
});     
```

**Extrait des migrations de la table `real_estate_companies` de Laravel.**

### Investisseurs : `investors`

La table `investors` a été modifiée pour ajouter le booléen `receive_options` afin d'empêcher l'envoi d'e-mails à un investisseur si celui-ci le souhaite. Pour `notary`, `accountant`, `bank`, `lawyer`, ce sont des champs JSON qui permettent d'ajouter tous les champs souhaités pour chacun des contacts liés à l'investisseur. Le booléen `informations_verified` permet de forcer l'investisseur à vérifier ses informations si c'est sa première connexion.

```php
Schema::table('investors', function (Blueprint $table) {
    $table->boolean('receive_options');
    $table->json('notary');
    $table->json('accountant');
    $table->json('bank');
    $table->json('lawyer');
    $table->boolean('informations_verified');
});
```

**Extrait des migrations de la table `investors` de Laravel.**

### Étapes des actifs : `locals_steps`

Les actifs immobiliers de la table `locals` possèdent des étapes en One To Many et j'ai donc ajouté la table `locals_steps` avec la clé étrangère liée à un seul actif. Chaque étape possède un numéro `step_number`, un titre `title`, un texte `text` et un liste de liens YouTube sous forme de JSON.

```php
Schema::create('locals_steps', function (Blueprint $table) {
    $table->bigIncrements('id');
    $table->integer('step_number');
    $table->string('title');
    $table->text('text');
    $table->json('youtube_links');
    $table->unsignedInteger('local_id')->index();
    $table->timestamps();

    $table->foreign('local_id')
        ->references('id')
        ->on('locals')
        ->onDelete('cascade');
});
```

**Extrait des migrations de la table `locals_steps` de Laravel.**

### Souscriptions : `subscriptions`

Dans un premier temps, il a été nécessaire de créer une table `subscriptions` pour gérer les souscriptions car le client souhaitait une validation des souscriptions avant que celles-ci ne passent dans la table pivot déjà existante `investor_real_estate_company` relation Many To Many entre `real_estate_companies` et `investors`, cette table était donc une table temporaire avant de valider ou non la souscription. Le client a changé d'avis par la suite rendant cette table inutile en soit car le client voulait une validation automatique des souscriptions. Par conséquent, les informations de cette table ont dû être intégrées à la table pivot existante.

On y retrouve des informations de suivi sur le clic sur certains éléments importants, sur l'accompagnement financier du projet par Foncière AALTO et sur le fait que l'utilisateur ait envoyé une proposition ou non. La table pivot a été transformée, passant d'une table pivot classique à une table pivot polyvalente proposant de nombreuses informations en plus de la relation Many to Many entre les deux entités.

Il est à noter que le changement d'avis du client s'est fait après une mise en production ce qui a rendu délicat la suppression de la table afin de n'en garder qu'une seule pour simplifier le nouveau processus.

```php
Schema::create('subscriptions', function (Blueprint $table) {
    $table->bigIncrements('id');
    $table->string('email')->index();
    $table->string('token');
    $table->boolean('is_valid');
    $table->timestamp('end_at');
    $table->unsignedInteger('investor_id');
    $table->unsignedInteger('real_estate_company_id');
    $table->unsignedInteger('wished_shares_quantity');
    $table->date('consultation_at');
    $table->date('teaser_opened_at');
    $table->unsignedInteger('responsible_user_id');
    $table->boolean('is_active');
    $table->timestamps();

    $table->foreign('investor_id')
        ->references('id')->on('investors')
        ->onDelete('cascade');
    $table->foreign('real_estate_company_id')
        ->references('id')->on('real_estate_companies')
        ->onDelete('cascade');
    $table->foreign('responsible_user_id')
        ->references('id')->on('users')
        ->onDelete('cascade');
});
Schema::table('investor_real_estate_company', function (Blueprint $table) {
    $table->boolean('with_funding');
    $table->boolean('investor_send_subscription');
    $table->date('consultation_at');
    $table->date('teaser_opened_at');
});
Schema::drop('subscriptions');
```

**Extrait des migrations de la table `subscriptions` de Laravel.**

## Composants d'accès aux données

De nombreux composants d'accès ont été développés, certains existaient et devaient être mis à jour et d'autres ont dû être créés. Tous les composants ont été créé en suivant les règles du framework Laravel et donc le MVC. Chaque entité est par conséquent représenté par un modèle, une classe métier, disposant de la classe `Model` de Laravel représentant la table en base de données (telle que définie dans les migrations).

L'accès aux données se fait par l'ORM de Laravel, Eloquent, en utilisant une syntaxe simple afin de récupérer les données souhaitées, ainsi `RealEstateCompany::all()` permet de récupérer toutes les entités `RealEstateCompany` depuis un contrôleur. Les relations de la base de données doivent être spécifiées dans le modèle à travers les classes proposées par Laravel. Mais des relations plus spécifiques peuvent être placées directement dans le modèle à des fin de raccourci pour définir un état relationel bien particulier pour le retrouver dans les contrôleurs par la suite.

Ci-dessous `$fillable` représente les champs disponibles en base de données, `locals()` et `subscriptions()` représentent les relations du modèle qui sont également présentent en base de données. Les deux méthodes `investors()` et `getCurrentInvestorAttribute()` représentent des relations bien particulières déjà représentées par `subscriptions()` pour la relation Many to Many habituelle, mais ici on récupère des champs bien spécifiques de la table pivot selon des conditions bien particulières. D'une part seules les souscriptions validées sont représentées avec `has_subscribed` à `true` et pour la seconde relation, `investor_id` est aussi récupéré pour ne prendre les relations que de l'utilisateur courant connecté.

Les **traits** utilisés permettent d'ajouter des options supplémentaires qui ne dépendent pas de ce modèle mais qui peuvent être ajoutées. Ainsi `HasOwner` permet de définir un `User` qui est le propriétaire de la `RealEstateCompany`, `RequestMediaTrait` permet d'ajouter des médias qui peuvent être de différente nature dans une table `medias` avec une relation polymorphique (gérée par spatie/laravel-medialibrary) et `Bulkable` permet d'effectuer des actions groupées sur plusieurs `RealEstateCompany` depuis le back-office.

```php
<?php

class RealEstateCompany extends Model implements BulkActions, HasMedia
{
    use HasOwner;
    use RequestMediaTrait;
    use Bulkable;

    protected $fillable = [
        'name',
        'total_investment_amount',
        'shares_quantity',
        'share_price',
        'share_slices',
        // ...
    ];

    public function locals()
    {
        return $this->hasMany(Local::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(InvestorRealEstateCompany::class);
    }

    public function investors(): BelongsToMany
    {
        return $this->belongsToMany(Investor::class)
            ->using(InvestorRealEstateCompany::class)
            ->wherePivot('has_subscribed', '=', 1)

            ->as('shares')
            ->withPivot(
                // ...
            )
            ->first();
    }

    public function getCurrentInvestorAttribute()
    {
        return $this->belongsToMany(Investor::class)
        ->using(InvestorRealEstateCompany::class)
        ->wherePivot('has_subscribed', '=', 1)
        ->wherePivot('investor_id', Auth::user()->investor_id)

        ->as('shares')
        ->withPivot(
            // ...
        )
        ->first();
    }
}
```

## Contrôleurs

Plusieurs contrôleurs spécifiques à l'API ont été créés dans `app/Http/Controllers/Api`, toutes les routes sont définies dans `routes/api.php`. La plupart sont en méthode `GET` mais certaines sont également en méthode `POST` ou `DELETE`. Les contrôleurs précisent habituellement une méthode `index()` permettant de récupérer la liste des entités liées à la route avec un nombre d'informations limité pour l'affichage sous forme de liste dans une vue par exemple, la méthode `show()` permet de récupérer l'entité avec toutes les informations qui lui sont liées. Pour gérer ces données, la classe de `Resource` a été utilisée native à Laravel, celle-ci permet de créer des classes spécifiques pour choisir quelles données seront disponibles dans le JSON de l'API, selon le niveau d'information souhaité on peut intégrer les relations ou attributs qui sont nécessaires afin de rendre le JSON plus rapide à charger.

Dans cet exemple, la méthode `index()` récupère l'utilisateur connecté grâce à la classe `Auth` de Laravel, puis en récupère la relation `investor` pour récupérer les données de l'investisseur et, depuis l'investisseur, on récupère les `realEstateCompanies` (relation Many To Many) où `has_subscribed` est à `true`, ce qui veut dire uniquement les fonds d'investissement qui sont validés. On trie ensuite les `RealEstateCompany` par date de création avant de l'envoyer à la `Resource` qui va le convertir au format JSON en injectant les données souhaitées.

Pour la méthode `show()`, on récupère simplement l'entité `RealEstateCompany` par son `id` qui a été passé par un paramètre de route et cette entité est envoyée à la Resource pour être convertie en JSON avec les données souhaitées.

```php
<?php

// ...

class CompanyController extends Controller
{
    public function index(Request $request)
    {
        $investor = Auth::user()->investor;

        $companies = $investor->realEstateCompanies()
            ->where('has_subscribed', '=', true)
            ->get();

        $companies = $companies->sortByDesc(function ($c) {
            return $c->shares->created_at;
        });

        return CompanyLightResource::collection($companies);
    }

    public function show(Request $request, int $id)
    {
        $company = RealEstateCompany::find($id);

        return CompanyResource::make($company);
    }
}
```

**CompanyController.php** situé dans `app/Http/Controllers/Api`

```php
Route::get('/companies', 'Api\CompanyController@index');
Route::get('/companies/{id}', 'Api\CompanyController@show');
```

**api.php** situé dans `routes`, ici on retrouvera les données sur la route *<http://localhost:8000/api/companies>* dans le cas où un serveur local est lancé avec la commande `php artisan serve`

## IHM

Seules deux interfaces sont détaillées ici, pour en voir plus, rendez-vous dans les **[Annexes](#annexes)**.

![Formulaire de connexion et accueil](assets/images/screenshots/home.jpg)

### Formulaire de connexion

Pour consulter le détail du code permettant d'afficher de l'interface de connexion, rendez-vous dans les **[Annexes](#annexes)**, seule celle-ci a été insérée ici car Flutter étant très verbeux, le code pour une interface est assez long.

Pour la connexion, on met en place des `TextEditingController` pour tous les champs de texte qui sont liés aux `TextFormField` qui permettent de saisir des données à l'aide du clavier virtuel qui s'affiche si l'utilisateur tape sur le champs. Nous avons deux champs : l'email et le mot de passe, ce dernier étant en `obscureText` et par conséquent chaque caractère est remplacé pour des questions de confidentialité. Lorsque l'utilisateur remplit le champs email, une flèche en bas du clavier permet de passer au mot de passe directement grâce à un `FocusScopeNode` qui permet de suivre l'utilisateur et lui proposer le champs suivant dans un formulaire ayant plusieurs champs. Ainsi le mot de passe proposera un clavier avec un bouton permettant de valider le formulaire sans avoir à taper sur le bouton de validation du formulaire. De plus on veillera à ajouter d'autres options permettant d'utiliser plus efficacement l'application comme `TextInputType` afin de spécifier le type de clavier comme un clavier "email" avec un *@* disponible facilement ou l'autofocus afin d'activer le clavier virtuel dès que l'utilisateur arrive sur le formulaire.

Lors de la validation, on verifie que les données sont bien toutes présentent et on lance la connexion en faisant appel à une méthode de communication API afin d'en récupérer un jeton pour la connexion. Si tout se passe bien, l'utilisateur est redirigé sur l'interface d'accueil et s'il y a un problème, un message avec le widget `SnackBar` informera l'utilisateur qu'il y a un problème, selon le code HTTP retourné par l'API pour avoir des messages précis pour les erreurs.

### Accueil

L'accueil présente un grand nombre de widgets et par conséquent, nous nous occuperons seulement de la liste des fonds qui est retournée. L'accueil étant un widget `Stateful`, il est possible de faire appel à des widgets asynchrones, ainsi nous prévoyons `Future<List<Company>> futureCompanies` et lors de `initState()` du widget, on injecte les données dans la variable `futureCompanies`.

```java
class _CompanyListState extends State<CompanyList> {
  late Future<List<Company>> futureCompanies;

  @override
  void initState() {
    super.initState();
    futureCompanies = CompanyRepository().fetchCompanies(context);
  }

  // ...
}
```

**Initialisation du composant**

La variable `futureCompanies` sera utilisée dans un `FutureBuilder` qui s'occupera de charger les données asynchrones après les widgets statiques ait fini de charger leurs données. Durant le chargement, une animation d'attente est affichée puis remplacée par le widget proposant les données récupérées depuis l'API.

```java
FutureBuilder<List<Company>>(
  future: futureCompanies,
  builder: (BuildContext context, AsyncSnapshot snapshot) {
    if (snapshot.hasData) {
      List<Company> companies = snapshot.data;
      return Container(
        width: 300.0,
        child: CompanyCard(company: company),
        alignment: Alignment.center,
      );
    } else if (snapshot.hasError) {
      return CircularProgressIndicator();
    }
  },
);
```

**FutureBuilder pour afficher des données asynchrones**

L'appel API est effectué depuis une autre classe pour gérer spécifiquement tous les appels externes. La classe a été définie dans l'application et des méthodes de sérialisation sont mise en place pour récupérer les données depuis le JSON et les transformer en un objet Dart, chaque objet est récupéré et injecté dans une `List` définie au début de la méthode. S'il y a une erreur dans la récupération des données, une liste vide est envoyée ne causant pas d'erreur pour l'utilisateur.

```java
Future<List<Company>> fetchCompanies(context) async {
    final _api = ApiProvider.api;
    var _token = await ApiProvider().getToken();
    final _url = Uri.parse('$_api/companies');
    var companies = <Company>[];

    try {
      var response = await http.get(
        _url,
        headers: {
          'Authorization': 'Bearer $_token',
          'Accept': 'application/json'
        },
      );

      var jsonData = jsonDecode(response.body)['data'];
      List<dynamic> jsonDataList = jsonData;
      for (var i = 0; i < jsonDataList.length; i++) {
        var company = Company.fromJson(jsonDataList[i]);
        companies.add(company);
      }
    } catch (e) {
      debugPrint(e.toString());
    }

    return companies;
}
```

**Appel API pour récupérer la liste des fonds d'investissement**

## Mécanismes de sécurité

Toute la sécurité de l'API est gérée par les `middlewares` de Laravel et précisément le système de sécurité `Sanctum`, le plus récent et le plus sécurisé. Le principe est de placer les routes contenues dans `api.php` dans un groupe de routes gérées par le `middleware` `auth:sanctum`. Ainsi toutes les routes protégées ne seront accessibles que par un utilisateur connecté et la classe `Auth` de Laravel dans les contrôleurs est un rempart supplémentaire car les informations récupérées dépendent de l'utilisateur connecté. Seules les routes de `login` et de `logout` sont publiques afin de pouvoir se connecter.

Le principe de Sanctum est de fournir, lorsque la connexion s'est effectuée positivement, un jeton[^6] qui permettra de s'authentifier auprès de l'API (via `Authorization header`) qui pourra retrouver l'utilisateur concerné. Ces jetons ont une longue durée de vie mais ils peuvent être supprimés manuellement et forcer la déconnexion de l'utilisateur.

De plus pour les applications web, Sanctum propose un système de cookies pour gérer l'authentification, cela protège efficacement contre les attaques CSRF (Cross Site Request Forgery) et XSS (Cross Site Scripting) qui sont détaillées dans la partie [**Sécurité**](#chapter-securite). Si l'application mobile ne peut pas utiliser cette sécurité supplémentaire, la partie front-end de l'application web, elle, l'utilise.

Le contrôleur valide les données pour s'assurer qu'elles sont sans danger et correspondent à ce qui est attendu. L'email sera d'abord testé, une erreur indiquera si celle-ci n'existe pas puis le mot de passe en utilisant la méthode `Hash::check()` de Laravel comparant le hashage des deux mots de passe. Un mot de passe invalide entrainera une erreur spécifique mais si tout se passe bien, un token sera généré et envoyé en réponse.

```php
class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email'       => 'required|email',
            'password'    => 'required',
            'device_name' => 'required',
        ]);

        try {
            /** @var User $user */
            $user = User::where('email', $request->email)->firstOrFail();
        } catch (\Throwable $th) {
            return abort(404, 'E-mail not valid');
        }
        $investor = Investor::whereId($user->investor_id)->firstOrFail();

        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => 
                    ['The provided credentials are incorrect.']
            ]);
        }

        return $user->createToken($request->device_name)->plainTextToken;
    }

    public function logout(Request $request)
    {
        $user = Auth::getUser();
        $user->tokens()->delete();

        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();
    }
}
```

**AuthController.php s'occupant des routes de login et de logout**

```php
Route::post('/login', 'Api\AuthController@login');
Route::post('/logout', 'Api\AuthController@logout');

Route::group(['middleware' => 'auth:sanctum'], function () {

    // ...
    
    Route::get('/companies', 'Api\CompanyController@index');
    Route::get('/companies/{id}', 'Api\CompanyController@show');
});
```

**Dans api.php, les routes de connexion sont publiques alors que les routes exposant des données sensibles sont protégées**

### Connexion depuis l'application

Depuis l'application, un formulaire récupère l'email et le mot de passe de l'investisseur afin de tenter de connecter l'utilisateur, on récupère le `deviceId` qui est l'identité de l'appareil qu'on envoie en plus au back-end où la connexion est gérée avec Laravel Sanctum. On tape donc sur la route API `/login` en méthode `POST` et si le statut de la réponse est bien `200`, on récupère le jeton renvoyé par le back-end afin de pouvoir communiquer avec les routes protégée et donc de récupérer les informations de l'utilisateur. Le `token` est stocké dans `SharedPreferences` qui permet de garder des données localement sur l'appareil et de les retrouver dans d'autres méthodes de l'application de façon asynchrone.

L'application est redémarrée par la suite avec `Phoenix`, car l'application est séparée en deux partie. Dès son lancement une méthode vérifie si le `token` existe et s'il est valable et donc connectera l'utilisateur à l'application, dans le cas contraire, l'utilisateur ne peut accéder qu'au formulaire de connexion. La bibliothèque pour gérer les appels API est `http` du paquet Pub `http`.

```java
Future<http.Response?> attemptLogIn({
        required String email,
        required String password,
    }) async {
    try {
      final _url = Uri.parse('$_api/login');
      final _deviceId = await ApiProvider().getDeviceId();

      var response = await http.post(
        _url,
        body: {
          'email': email,
          'password': password,
          'device_name': _deviceId,
        },
        headers: {
          'Accept': 'application/json',
        },
      );

      if (response.statusCode == 200) {
        var prefs = await SharedPreferences.getInstance();
        await InvestorRepository().fetchInvestor();
        var token = response.body;
        await prefs.setString('token', token);
        await prefs.setBool('isLoggedIn', true);
      }

      return response;
    } catch (e) {
      debugPrint(e.toString());
    }
}
```

**Méthode de connexion**
