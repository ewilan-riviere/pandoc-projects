# Remerciements {.unnumbered}

Mes remerciements vont tout d'abord à mon manager Michel TRAUTH et mon équipe de développement : Adrien Beaudouin et Mathieu MONNIER ainsi qu'à Camille BONIFACE qui a designé l'application.

L'entreprise Useweb m'a très bien accueillie, j'ai été aidée avec pédagogie en cas de problème, on m'a fait confiance pour développer des projets différents mais en particulier pour le projet présentée par ce rapport qui était le premier projet Flutter de l'entreprise.  
Si tout se passe bien, je vais continuer en bac+5 en alternance à l'ENI et avec Useweb pour approfondir le développement web et le développement mobile.

Je remercie ma soeur, Violette RIVIERE, qui a su me conseiller Flutter pour développer l'application en me détaillant les intérêts de ce framework par rapport aux autres technologies et j'ai pu alimenter ainsi le débat dans mon entreprise et l'analyse sur le choix de la technologie pour réaliser mon projet.

L'ENI a été très présente pendant ces deux ans et malgré les circonstances d'enseignement à distance, l'école a su proposer des modules de qualités sur des technologies différentes et intéressantes. Elle a aussi su me soutenir dans mon parcours profesionnel, étant à l'écoute et pro-active dans l'aide qu'elle pouvait m'apporter.

Enfin je remercie les membres du jury qui ont accepté d'évaluer mon projet.
