# En résumé {.unnumbered}

Dans le cadre de mon alternance Conceptrice Développeuse d'Applications à l'ENI de Chartes de Bretagne, j'ai développé un projet, l'application mobile Foncière AALTO, dans l'entreprise Useweb à Cesson-Sévigné.

Ce rapport décrit en détails la réalisation de ce projet durant mon alternance, réalisé en 2020 sur plusieurs mois (entre la première version, la maintenance et les évolutions). Pour cette application j'ai été amenée à découvrir le développement mobile avec le framework Flutter et le langage Dart afin de réaliser une application mobile pour un client historique de l'entreprise Useweb.

J'ai été accompagnée dans ce projet par mon manager, Michel TRAUTH, et l'appui de développeurs expérimentés : Adrien Beaudouin et Mathieu MONNIER. Je n'ai pas eu l'occasion d'avoir un-e développeur-euse expérimenté-e en Flutter pour m'accompagner car j'étais la seule développeuse de l'entreprise a me lancer dans le développement d'une application mobile avec ce framework mais l'expérience et l'aide des développeurs de mon équipe a toutefois été motivante pour moi.

Ce rapport conclu les deux années que j'ai passées à Useweb en parallèle de ma formation à l'ENI.

## Abtract {.unnumbered}

As part of my vocational as an Application Designer and Developer at ENI Chartes de Bretagne, I developed a project, the mobile application Foncière AALTO, in the company Useweb in Cesson-Sévigné.

This application was developed in Flutter and Dart, it allows an investor linked to Foncière AALTO to connect to his personal space to consult his financial information, he or she can consult the details of each investment fund but also consult the latest financial news. If he wishes, he can modify his personal information, his financial contacts and his administrative documents in PDF format. If a new fund is available, they will receive a notification and can subscribe if they wish by filling in a form, downloading a PDF subscription form and sending it back from their smartphone. He can also download his financial statement in PDF if he wishes.

The application is connected to an API that depends on a back-office managed by the administrators where the data is retrieved and displayed in detail in tables and graphs. If the user wishes, he/she also has access to a web space where he/she can connect to perform the same actions as on his/her phone.

## Entreprise Useweb {.unnumbered}

Useweb est une agence de développement web qui a été rachetée par Michael HYOT en 2010, celui-ci fait parti du conseil d'administration de Secob, une entreprise de comptabilité. C'est sous sa direction que l'entreprise est devenue rentable en développement des site web sur mesure pour des entreprises souhaitant améliorer leur image numérique. Depuis plusieurs années, Useweb est spécialisée dans la création de site web en Nuxt.js avec un back-office permettant la mise à jour des données pour le client, souvent créé avec Laravel. Toute une équipe s'occupe du référencement naturel pour s'assurer que les créations de l'entreprise soient bien positionnées dans les résultats. C'est sous la demande de son client Foncière AALTO que l'entreprise a décidé de se lancer dans la création d'application mobile avec Flutter.
