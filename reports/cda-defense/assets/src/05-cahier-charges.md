# Cahier des charges

## Présentation

Le projet de Foncière AALTO remonte à 2018 et a été développé par Adrien Beaudouin, le client s'est montré satisfait de la plateforme et a commencé à l'utiliser. C'est au cours de l'année 2020 que le client a recontacté Useweb dans le but d'apporter quelques améliorations à la plateforme et également de commander une application mobile.

Commencé en septembre 2020, le projet peut être divisé en deux : la modification du back-office existant et la création d'une application mobile. Ce dossier est centré sur l'application mobile et la présentation des fonctionnalités qui y sont liées mais la création d'une API a été nécessaire et les modifications du back-office ont impacté l'application. Par conséquent les modifications du back-office sont également traitées dans le cahier des charges. Des informations supplémentaires sont disponibles dans les [**Annexes**](#annexes) si elles concernent la partie back-office. Le projet est actuellement fini pour sa première version, à savoir les modifications du back-office et le développement d'une application disponible aujourd'hui sur les stores Android et iOS.

## Expression des besoins

Le client Foncière AALTO souhaite une amélioration du back-office afin d'ajouter des fonctionnalités sur des entités déjà existantes et d'en créer de nouvelles pour améliorer l'expérience utilisateur. Si la partie web pour les investisseurs est responsive et donc accessible depuis un téléphone mobile, le client souhaite proposer une expérience mobile plus satisfaisante avec une application téléchargeable depuis le store de l'OS utilisé, Google Play ou l'App Store. Le but d'une telle application est de placer l'utilisateur dans un environnement très simple à utiliser pour récupérer ses informations financière, consulter les nouveaux fonds disponibles, recevoir des notifications, consulter ses fonds d'investissement et consulter les actualités proposées par Foncière AALTO.

Le public cible de cette application concerne uniquement les investisseurs de Foncière AALTO, si toute personne extérieure à l'entreprise peut télécharger l'application, il n'est pas possible de créer un compte depuis l'application elle-même. Toute création de compte se fait depuis le back-office par les administrateurs. L'utilisateur a besoin d'être un minimum à l'aise avec les applications mobiles afin de comprendre facilement comment naviguer.

L'application est compatible avec Android Lollipop 5.0 minimum (sorti en 2013) et iOS 9  minimum (sorti en 2015), à noter qu'une fonctionnalité (le téléversement de fichiers) n'est possible qu'à partir d'iOS 11.

## Fonctionnalités

Le back-office est amélioré en ajoutant des fonctionnalités ou en modifiant des fonctionnalités existantes.

- Ajout de champs pour un fond : texte, fichiers, date limite
- Multi-type d'investisseurs, ajout d'informations financières
- Amélioration de l'email de présentation d'une nouvelle offre avec un jeton
  - Pose d'options
  - Notifications à l'investisseur
  - Souscription à une offre
- Suivi de l'activité des investisseurs et téléchargement possible des fichiers téléversés
- Ajout d'étapes pour les actifs immobiliers : fichiers, images, vidéos, suivi disponible pour les investisseurs
- Formulaire de première connexion
- Activation des comptes investisseurs pour les administrateurs

L'application mobile a été développée en relation directe avec ce qu'offre la version web de la partie investisseur avec des fonctionnalités supplémentaires.

- Authentification sécurisée depuis une page de connexion
- Création d'une API et récupération des informations
- Afficher la liste des fonds avec une page de détails pour chaque fond
  - Chaque fond a plusieurs actifs immobiliers, une page de détail pour chaque actif sera disponible
- Afficher la liste d'actualités avec une page de détails pour chacune
- Un bouton permet de télécharger un PDF pour le relevé financier de l'investisseur
- Une section permet à l'investisseur de modifier ses informations personnelles, ses contacts financiers et ses documents
- Afficher la liste des souscriptions disponibles
  - Si l'investisseur est intéressé, il peut souscrire depuis la page de détails
  - Une notification push est reçue en cas d'une nouvelle souscription disponible

## Spécifications fonctionnelles

### Back-office

| Code     | Fonctionnalité du back-office                        |
| -------- | ---------------------------------------------------- |
| AAL_US1  | Lancement d’un nouveau fond d’investissement         |
| AAL_US2  | Nouveaux utilisateurs                                |
| AAL_US3  | Email de présentation d’une nouvelle offre           |
| AAL_US4  | Suivi de consultation de l’activité des utilisateurs |
| AAL_US6  | Suivi                                                |
| AAL_US9  | Documents liés aux investissements                   |
| AAL_US10 | Actif : avancement des travaux                       |
| AAL_US11 | Actif : Ajout de média aux étapes de l’avancement    |
| AAL_US12 | Consultation d’une nouvelle offre CAP INVEST         |
| AAL_US13 | Poser une option de souscription                     |
| AAL_US14 | Sélection de mon option de souscription              |
| AAL_US15 | Souscrire une offre                                  |
| AAL_US16 | Suivi de l’avancement d’un chantier                  |
| AAL_US17 | Application Mobile                                   |
| AAL_US18 | Première connexion                                   |
| AAL_US19 | Accès des comptes à la plateforme                    |

#### Lancement d’un nouveau fond d’investissement (code : AAL_US1)

En tant qu’administrateur, je peux créer un nouveau fond d’investissement afin de diffuser l’information auprès de l’ensemble des investisseurs. En plus du processus de création existant, l’administrateur doit pouvoir :

- Renseigner les champs nécessaires à l’édition du bon de souscription
- Renseigner un texte (obli.) et une image (facul.) de présentation
- Associer un pdf « Teaser »
- Une date limite pour les options de souscription
- Associer une série de fichiers pdfs, avec un titre pour chacun
- Prévoir plusieurs statuts pour les fonds => option, souscription, construction, active

Ces nouvelles fonctionnalités peuvent remettre en cause l’organisation actuelle de l’interface.

#### Nouveaux utilisateurs (code : AAL_US2)

En tant qu’administrateur, je peux créer deux types de profils utilisateurs afin de segmenter ma base.

- Prospect : un Prospect est un utilisateur non investisseur, il possède un compte renseigné par l’administrateur, mais ne peux pas se connecter à l’extranet, il n’a accès qu’aux informations sur les nouvelles offres
- Investisseur :
  - A qui une offre de souscription définitive est soumise
  - Qui a déjà investi dans une offre CAP INVEST

Plusieurs champs sont ajoutés aux informations personnelles des utilisateurs : cabinet comptable, notaire, banque(s), cabinet d’avocats

#### Email de présentation d’une nouvelle offre (code : AAL_US3)

En tant qu’administrateur, je peux adresser un email pour présenter une offre à la liste des utilisateurs de la base. En cliquant sur le bouton « Envoyer un email » :

- Un popup s’ouvre (système actuel)
- L’administrateur renseigne un expéditeur
- Un objet
- Un message personnalisé
- Il sélectionne le template « nouvelle offre »
- Il sélectionne le fond concerné par l’envoi
- Il clique sur le bouton envoyer

L’email contient deux CTA

- Lien vers la page de présentation
- Lien vers le formulaire d’option de souscription

L’utilisateur n’a pas besoin de se connecter pour consulter le contenu web de l’offre par contre, ces contenus ne sont pas public, la page n’est accessible qu’au travers du lien contenu dans l’email.

#### Suivi de consultation de l’activité des utilisateurs (code : AAL_US4)

En tant qu’administrateur, je peux afficher la liste des utilisateurs qui ont consulté une nouvelle offre. La liste des souscripteurs se construit selon l’activité de chaque utilisateur :

- A consulté l’offre
- A téléchargé le pdf teaser
- A posé une option
- Et dans ce cas le nombre de parts souscrites

Pour chaque utilisateurs de cette liste, l’administrateur peut sélectionner ceux qui vont pouvoir souscrire définitivement à l’offre au travers d’un bouton ou d’une checkbox.

#### Suivi (code : AAL_US6)

En tant qu’administrateur, je peux suivre la liste des investisseurs et leur activité sur le site. Dans le compte de l’investisseur :

- Je retrouve la liste des biens auxquels il a souscrit des parts
- Par utilisateur, l’administrateur doit pouvoir télécharger le Bon de souscription
- Par utilisateur, l’administrateur doit pouvoir afficher et consulter les fichiers soumis

L’administrateur reçoit une notification pour tous les actions d’un utilisateur.

#### Documents liés aux investissements (code : AAL_US9)

En tant qu’administrateur, je peux ajouter des documents (fichiers) dans la fiche de chaque fond afin que les investisseurs puissent les consulter. Sur le modèle du système mis en place pour les actifs, l’administrateur ajouter des documents à l’aide d’un champ de téléversement (Type pdf, documents office, images).

#### Actif : avancement des travaux (code : AAL_US10)

En tant qu’administrateur, je peux gérer les étapes de la construction d’un actif. Cinq étapes fixes sont définies, l’administrateurs peut en modifier le titre, le texte et l’image de présentation, chaque étape peut être ou non publié sur le site.

#### Actif : Ajout de média aux étapes de l’avancement (code : AAL_US11)

En tant qu’administrateur, je peux ajouter des images et vidéo à la volée, pour chaque étapes. Pour chaque Actif, les étapes sont fixes : Video Youtube, image jpeg et png, prévoir une limite de taille.

#### Consultation d’une nouvelle offre CAP INVEST (code : AAL_US12)

En tant qu’utilisateur, je peux consulter la page de présentation d’un nouveau fond d’investissement afin de poser une option :

- A partir du lien contenu dans l’email d’annonce, le prospect affiche la page de présentation
- Il peut télécharger le pdf teaser
- Il peut poser une option de suscription à l’aide du formulaire présent dans la page

Le téléchargement l’affichage de la page et le téléchargement doivent être mémorisés. L’utilisateur est reconnu grave au token présent dans le lien présent dans le mail, à ce stade pas besoin de connexion.

#### Poser une option de souscription (code : AAL_US13)

En tant qu’utilisateur, je peux poser une option de souscription en répondant au formulaire affiché dans la page de présentation d’une nouvelle offre :

- Le prospect renseigne ses informations personnelles
- Le prospect renseigne les champs spécifiques à l’offre
- Vérification à effectuer sur email et tél
- Acceptation CGV et respect rgpd à définir (création de compte en particulier)
- Notification administrateur de la réponse
- Notification Prospect de sa demande avec rappelle du processus de sélection à venir

Si l’utilisateur est un investisseur, il lui sera demandé de se connecter.

#### Sélection de mon option de souscription (code : AAL_US14)

En tant qu’utilisateur, je reçois un email me signalant le statut (accepté ou non) de mon option de souscription. En cas de réponse positive, un CTA me permet d’accéder à mon compte client afin de finaliser ma souscription. Lorsque qu’un utilisateur avec le statut « Prospect » saisi une offre, ce statut passe à « Investisseur »

#### Souscrire une offre (code : AAL_US15)

En tant qu’investisseur, je peux souscrire une offre en ligne une fois reçu mon email de validation :

- L’investisseur est informé par mail la notification de sa sélection
- Il se connecte à son espace client Aalto
- S’il s’agit d’une première connexion, il crée un mot de passe et vérifie ses informations personnelles
- Il sélectionne le fond concerné
- Rempli le formulaire de souscription
- A la validation du formulaire le BS pdf est généré, il peut être téléchargé et signé (offligne ou électroniquement)
- Une fois bon de souscription renvoyé (modalité à définir) l’utilisateur peut télécharger les documents légaux nécessaires

#### Suivi de l’avancement d’un chantier (code : AAL_US16)

En tant qu’investisseur, je peux consulter l’avancée du chantier de construction des biens des sci auxquelles j’ai souscrit. Les étapes s’affichent comme des onglets au mm titre qu’information et document.

#### Application Mobile (code : AAL_US17)

En tant que visiteur et / Investisseur je peux télécharger l’application Aalto sur AppleStore et/ou GooglePlay pour accéder à mon espace client et effectuer les démarches.

#### Première connexion (code : AAL_US18)

En tant qu’investisseur, lors de ma première connexion, je dois :

- Modifier le mot de passe temporaire reçu par mail
- Vérifier mes informations personnelles et celles de mes experts

Je retrouve ses information dans mon compte.

#### Accès des comptes à la plateforme (code : AAL_US19)

En tant qu’administrateur, je peux activer les comptes des investisseurs pour qu’ils accèdent à la plateforme :

- L’administrateur peut activer les compte directement à partir de la liste des investisseurs soit en cochant les cases de la colonne  « actifs » une à une, soit tout d’un coup en cochant celle en entête de colonne.
- En cliquant sur le bouton valider les comptes les modifications sont prises en compte et un email est adressé à chaque utilisateur.

Il contient le login et un mot de passe temporaire que l’utilisateur devra changer lors de sa première connexion.

### Application mobile

| Code      | Fonctionnalité de l'application                                   |
| --------- | ----------------------------------------------------------------- |
| AAL_APP1  | Authentification sécurisé                                         |
| AAL_APP2  | Création d'une API                                                |
| AAL_APP3  | Récupération de la liste des fonds                                |
| AAL_APP4  | Récupération de la liste des fonds disponibles à la souscription  |
| AAL_APP5  | Récupération des actifs                                           |
| AAL_APP6  | Récupération de la liste des actualités                           |
| AAL_APP7  | Téléchargement du relevé financier                                |
| AAL_APP8  | Modification des informations personnelles par l'utilisateur      |
| AAL_APP9  | Souscription depuis la page de détail d'une nouvelle souscription |
| AAL_APP10 | Notifications push pour une nouvelle souscription                 |

#### Authentification sécurisé (code : AAL_APP1)

L'accès à l'application passe nécessairement par une authentification via un formulaire de connexion avec email et mot de passe. Si c'est la première connexion, un formulaire de vérification des informations sera proposé à l'utilisateur, celui-ci devra changer son mot de passe et valider ses informations. La déconnexion est une option qui sera proposée à l'utilisateur depuis la page d'accueil.

#### Création d'une API (code : AAL_APP2)

La partie back-end de l'application web devra proposer une API privée dont les informations ne peuvent être consultée que par une personne possédant un jeton d'authentification. Cette API permettra à l'application de communiquer avec le back-end.

#### Récupération de la liste des fonds (code : AAL_APP3)

La liste des fonds d'investissement liés à l'investisseur connecté devra être disponible sur la page d'accueil et il sera possible d'aller voir les détails de chaque fond. L'application proposera une barre de navigation placée en bas qui sera persistante sur toutes les pages principales, on y verra la page d'accueil, le relevé financier (AAL_APP7), les informations personnelles (AAL_APP8) et la déconnexion (AAL_APP1).

#### Récupération de la liste des fonds disponibles à la souscription (code : AAL_APP4)

La liste des fonds d'investissement proposant une souscription à l'investisseur connecté devra être affichée si au moins un fond est disponible. L'utilisateur pourra entrer dans un processus de souscription à la place de l'affichage du détail du fond (cf: **AAL_APP3**).

#### Récupération des actifs (code : AAL_APP5)

Les actifs immobiliers liés à un fonds devront être récupérés lorsque l'utilisateur arrive sur la page de détails d'un fond, chacun d'entre-eux devra proposer une page de détails.

#### Récupération de la liste des actualités (code : AAL_APP6)

Les actualités rédigées par les administrateurs de Foncière AALTO pourront être récupérées depuis la page d'accueil, une page présentant les articles triés du plus récent au plus ancien permettra à l'utilisateur de choisir l'article à lire. De plus un bouton "Article suivant" sera disponible en bas de l'article si un article plus récent existe.

#### Téléchargement du relevé financier (code : AAL_APP7)

Le relevé financier proposant les dernières informations liées aux investissements de l'investisseur pourra être téléchargé par un bouton de l'écran d'accueil mais également de la barre de navigation.

#### Modification des informations personnelles par l'utilisateur (code : AAL_APP8)

L'utilisateur pourra modifier ses informations personnelles depuis trois formulaires différents :

- Informations globales : nom, prénom, adresse
- Contacts : notaire, comptable, banque, avocat
- Documents : téléversement de fichiers PDF pour les documents officiels

#### Souscription depuis la page de détail d'une nouvelle souscription (ode : AAL_APP9)

Depuis le détail d'un fond de proposé comme nouvelle souscription, l'utilisateur pourra suivre un processus de souscription où il indiquera le nombre de parts souhaitées et toutes les informations relatives à son identité. Un bon de souscription lui sera proposé après validation, disponible au téléchargera depuis la page de détail du fond qui passera de la liste des souscription à sa liste de fonds et il pourra envoyer le bon signé par téléversement depuis la page de détail.

#### Notifications push pour une nouvelle souscription (code : AAL_APP10)

Lorsque qu'un administrateur lance une nouvelle souscription, une notification push doit prévenir l'investisseur qu'une nouvelle souscription est disponible.
