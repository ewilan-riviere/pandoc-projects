# Spécifications techniques

Les spécifications techniques du back-office sont ici survolées car le projet est centré sur l'application mobile, la base de données est détaillée ainsi que certaines routes de l'API mais les vues sont celles de l'application mobile, il est possible de retrouver certaines vues du front-end des les **Annexes**.

## Uses cases

L'application devra, au minimum, couvrir ces fonctionnalités.

![Use case](assets/images/use-case.jpg)

## Maquettes

Les autres maquettes sont disponibles dans les **Annexes**.

![Pages de connexion, accueil et détail d'un fond](assets/images/design/design-main.jpg)

## Schéma de base de données

![real_estate_companies & investors avec leur table pivot (relation Many To Many)](assets/images/database/database-companies.jpg)

Les tables les plus importantes sont `real_estate_companies` (les fonds d'investissement) et `investors` (les investisseurs) avec leur table pivot (relation **Many To Many**) permettant de stocker des informations supplémentaire sur chaque relation. Ainsi **chaque fond peut avoir plusieurs investisseurs** et **chaque investisseur peut avoir plusieurs fond**.

![locals en relation Many To One avec real_estate_companies et ses dépendances](assets/images/database/database-locals.jpg)

`real_estate_companies` est en relation **One To Many** avec la table `locals` (actifs immobiliers). Celle-ci est en relation avec plusieurs tables pour gérer des dépendances entièrement gérées par `locals`. Ainsi **chaque fond peut avoir plusieurs actifs** et **chaque actif appartient à un seul fond**.

- `leases` (les bails) en relation **Many To One** avec `locals`. Ainsi **chaque bail est lié à un seul actif** et **chaque actif peut avoir plusieurs bails**.
- `tenants` (les locataires) en relation **Many to One** avec `leases`. Ainsi **chaque locataire peut avoir plusieurs bails** et **chaque bail peut avoir un seul locataire**.
- `locals_steps` (les étapes des actifs) en relation **Many To One** avec `locals`. Ainsi **chaque étape est liée à un seul actif** et **chaque actif peut avoir plusieurs étapes**.

![Tables isolées supplémentaires](assets/images/database/database-extra.jpg)

La table `posts` (les articles) gère les articles dans la partie actualités et la table `media` du paquet [**spatie/laravel-medialibrary**](https://spatie.be/docs/laravel-medialibrary/v9/introduction) gère les médias de toutes les autres tables avec une relation **Many To Many (Morph)**, cette table n'a pas été modifiée.

![Tables pour gérer les utilisateurs](assets/images/database/database-users.jpg)

La table `users` (les utilisateurs) gère les utilisateurs et a une relation **One To One** avec la table `investors`, ainsi `investors` ne gère pas les spécificités liées à l'authentification qui est laissée à la table `users` qui gère également des rôles différents que celui d'investisseur, à savoir les administrateurs de l'application. La table `addresses` gère en relation **Many to Many (Morph)** pour plusieurs tables et enfin la table `emails` gère les emails envoyés depuis l'application.

Les tables supplémentaires sont disponibles dans [**Schémas supplémentaires de la base de données**](#annexes-schemas-database), disponible dans les [**Annexes**](#annexes).

## Ressources logicielles

Pour réaliser ce projet, j'ai dû utiliser Laravel *v6.0* avec PHP *v7.4*, Vue.js *v2.x* embarqué dans Laravel, MySQL *v8.0*, Flutter *v1.2* et Nuxt.js *v2.15* avec Bootstrap Vue.

- **Laravel** a été utilisé pour la partie back-end: framework MVC[^2], connecté à la base de données en MySQL. **Pré-existait** quand j'ai repris le projet, toute la partie front-end était embarquée dans les vues de Laravel en Vue.js, séparé en deux chemin d'authentification.
- **Flutter** a été utilisé pour créer une application mobile représentant la partie investisseur déjà disponible en web mais pour pour Android et iOS, branchée sur l'API de l'application web Laravel.
- Nuxt.js a été utilisé pour créer une front-end séparé pour les investisseurs qui était, à la base, embarqué dans Laravel, pour ne laisser que la partie administrateur à Laravel et utiliser la même API que l'application pour le front-end investisseur.

Dans l'ensemble il n'était pas possible de faire évoluer facilement ces technologies vers leurs versions les plus récentes, en particulier pour Laravel, si la version utilisée, la *v6.0*, est une LTS[^3], il reste plus facile de travailler un projet avec la dernière version d'un framework. Pour Flutter, c'était plus délicat du fait de la jeunesse du framework, son évolution est pour l'instant assez constante[^4], la volonté est de maintenir l'application avec la dernière version disponible de Flutter durant son développement. La plus grande partie de l'application est réalisée et fonctionnelle mais le client continue à demander certains ajouts. Et, si besoin, l'intérêt serait de la stabiliser sur une version stable lorsque le projet ne se verra plus ajouter de nouvelles fonctionnalités.

## Architecture projet

Quand on utilise le CLI de Flutter pour initialiser un projet, on obtient cette architecture

```
├─ android
├─ ios
├─ lib
├─ test
├─ .gitignore
├─ .metadata
├─ pubspec.lock
├─ pubspec.yaml
└─ README.md
```

- `android` et `ios` proposent la configuration spécifique par plateforme
- `lib` contient tout le code de l'application avec uniquement `main.dart`
- `test` contient les tests avec uniquement `widget_test.dart`
- `pubspec.yaml` est le gestionnaire de paquets, **pub**, où on peut ajouter de nouveaux paquets

```
├─ android
├─ assets
│  └─ images
├─ ios
├─ lib
│  ├─ layouts
│  ├─ models
│  ├─ repositories
│  ├─ resources
│  ├─ screens
│  │  ├─ about
│  │  ├─ company-details
│  │  ├─ company-list
│  │  ├─ company-new-subscription
│  │  ├─ error
│  │  ├─ first-connection-form
│  │  ├─ forgot-password
│  │  ├─ investor-informations
│  │  ├─ local-details
│  │  ├─ login
│  │  ├─ posts-details
│  │  └─ posts-list
│  ├─ services
│  ├─ theme
│  ├─ widgets
│  └─ main.dart
├─ test
├─ .env
├─ .env.example
├─ .gitignore
├─ .metadata
├─ analysis_options.yaml
├─ CHANGELOG.md
├─ Makefile
├─ pubspec.lock
├─ pubspec.yaml
└─ README.md
```

Actuellement, l'application a cette architecture

- `layouts` pour gérer tous les composants représentant le cadre de l'application avec la navigation
- `models` représentation des objets de l'API
- `repositories` appels API par modèle et conversion du JSON vers de l'objet
- `resources` pour stocker des classes utilitaires utiles dans toute l'application
- `screens` pour gérer toutes les vues séparées en répertoires spécifiques
- `services` pour gérer les services comme l'API, l'authentification, les téléchargements, les téléversements, les notifications
- `theme` pour centraliser le style des composants
- `widgets` pour les composants utilisés dans plusieurs vues différentes
- `.env` pour les variables d'environnement
- `analysis_options.yaml` pour avoir les règles de *pedantic*, le *linter* de Google
- `CHANGELOG.md` pour lister les changements de versions
- `Makefile` pour rassembler les commandes afin de les simplifier
- `README.md` pour expliquer les bases du projet avec une documentation afin de l'installer

## Sécurité

Les recommandations de sécurité sont celles fournies par Laravel avec une protection contre les attaques CSRF, XSS et les injections SQL avec une API privée et une authentification sécurisée (HTTPS). Elles n'ont pas été spécifiées par le client et n'ont pas été précisée dans les spécifications. Par conséquent, la mise en place de la sécurité (et la décision quant à quelles solutions implémenter) a reposé sur les personnes en charge du développement.

Pour Laravel, la protection de l'API avec Sanctum, un paquet de Laravel, a été choisie, ainsi pour la partie front-end web, l'utilisation du paquet [**nuxt/auth**](https://auth.nuxtjs.org/) qui intègre Sanctum dans ses options permet une connexion facile à mettre en place. Pour la connexion depuis Flutter, l'utilisation d'un jeton est la solution utilisée par Sanctum et donc celle qui a été choisie ici.
