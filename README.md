# Pandoc Projects <!-- omit in toc -->

[![latex](https://img.shields.io/static/v1?label=LaTeX&message=2021&color=008080&style=flat-square&logo=latex&logoColor=ffffff)](https://www.latex-project.org)
[![pandoc](https://img.shields.io/static/v1?label=Pandoc&message=v2.13&color=000000&style=flat-square)](https://pandoc.org)

[![linux](https://img.shields.io/static/v1?label=Linux&message=with%20TexLive&color=FCC624&style=flat-square&logo=linux&logoColor=ffffff)](https://www.linux.org/)
[![macos](https://img.shields.io/static/v1?label=macOS&message=with%20Homebrew&color=000000&style=flat-square&logo=apple&logoColor=ffffff)](https://www.apple.com/macos/big-sur/)
[![windows](https://img.shields.io/static/v1?label=Windows&message=with%20WSL&color=0078D6&style=flat-square&logo=windows&logoColor=ffffff)](https://www.microsoft.com/en-us/windows)

> **Use Pandoc to generate reports from `md` files with LaTeX template.**
>
> Warning: Pandoc can have some update that will broke PDF generation here

- [**I. Install dependencies**](#i-install-dependencies)
  - [*a. Linux Debian / Windows WSL*](#a-linux-debian--windows-wsl)
  - [*b. macOS*](#b-macos)
  - [*c. Pandoc version*](#c-pandoc-version)
- [**II. Setup**](#ii-setup)
- [**III. Add new project**](#iii-add-new-project)
- [**IV. Options**](#iv-options)
  - [a. Template](#a-template)
    - [Custom template](#custom-template)
  - [b. Pandoc filters](#b-pandoc-filters)
  - [c. Highlight](#c-highlight)
  - [d. Bibliography \& CSL](#d-bibliography--csl)
    - [CSL](#csl)
- [**V. Troubles**](#v-troubles)
  - [a. Check PDF metadata](#a-check-pdf-metadata)

---

## **I. Install dependencies**

You need to install *LaTeX*, *XeLaTeX* and *Pandoc* to use this repository.

### *a. Linux Debian / Windows WSL*

Install latex and pandoc

```bash
sudo apt install -y texlive texlive-xetex pandoc texlive-latex-extra
```

### *b. macOS*

- <https://pandoc.org/installing.html#macos>
- <https://tex.stackexchange.com/questions/974/why-is-the-mactex-distribution-so-large-is-there-anything-smaller-for-os-x>

For MacOS with [Homebrew](https://brew.sh)

```bash
brew install pandoc librsvg python
```

```bash
brew tap homebrew/cask
brew install --cask basictex
eval "$(/usr/libexec/path_helper)"
# Update $PATH to include `/usr/local/texlive/2020basic/bin/x86_64-darwin`
sudo tlmgr update --self
sudo tlmgr install hyphenat texliveonfly xetex adjustbox tcolorbox collectbox ucs environ trimspaces titling enumitem rsfs multirow framed opensans fontaxes cabin-condensed fontspec xurl listings mdframed sourcesanspro cellspace tocbibind glossaries lastpage --verify-repo=none
```

```bash
sudo tlmgr install cellspace fvextra tcolorbox tocbibind lastpage framed xstring
```

### *c. Pandoc version*

Check Pandoc version, this repository have to be used with **Pandoc 2.13**

```bash
pandoc --version
```

If Pandoc is too old, download directly last `deb` file on [**Pandoc realeases**](https://github.com/jgm/pandoc/releases)

## **II. Setup**

Generate report demo

```bash
make report-demo
```

Get PDF on `ouput/demo.pdf`

Generate all reports from `reports/` to PDF into `output/`

```bash
make
```

Generate specific report to PDF

```bash
make name=title-web-developer-mobile report
```

Clean all PDF

```bash
make clean
```

## **III. Add new project**

To add new project

```bash
make name=my-awesome-project report-new
```

## **IV. Options**

### a. Template

You can change template to use with `template` option. The value have to be a `tex` file inside `templates/` dir.

> report is default template

```bash
make template=pandoc.tex
```

#### Custom template

You can find templates into `templates/`, you can modify anything to get what design you want.

### b. Pandoc filters

You can use custom filters

```bash
make filter=pandoc-mustache
```

> Here I use [pandoc-mustache](https://github.com/michaelstepner/pandoc-mustache), you need Python 3 with pip to install `pip install -U pandoc-mustache`

### c. Highlight

You can choose another highlight theme for code blocks, default is a fork of `kate` theme but you can choose original highlight theme or custom `templates/themes/basic.theme`

```bash
make highlight=zenburn
```

### d. Bibliography & CSL

You can use `citeproc` to get bibliography, in sample `metadata.yaml` you have `bibliography` key (if you don't want bibliography, just remove this key)

```yaml
bibliography: index.bib
```

In `index.bib`, for example

```bib
@Book{ref1,
  author="Thales von Milet",
  title="Doppelwinkel-Funktionen",
  url="http://de.wikipedia.org/wiki/Formelsammlung_Trigonometrie#Doppelwinkelfunktionen",
  year="600 v.Chr.",
  address="Milet, Kleinasien",
  publisher="Wikipédia"
}
```

In `assets/src/any-filename.md`

```md
# References

::: {#refs}
:::
```

`citeproc` will generate bibliography in `{#refs}`

And in any markdown file you can reference bibliography

```bash
About Doppelwinkel-Funktionen[@ref1]
```

This `[@ref1]` will reference this element from `index.bib`

#### CSL

CSL represent to format of bibliography, you can choose a different CSL format with `csl`, add just new CSL file to `templates/csl/`

>Default format is `multiple-sclerosis-journal`

```bash
make csl=new-csl.csl
```

If you want to get some infos about CSL styles, check [**example gallery**](https://github.com/KurtPfeifle/pandoc-csl-testing/blob/master/example-gallery.md) and you can download `csl` on [**official CSL styles**](https://github.com/citation-style-language/styles)

## **V. Troubles**

### a. Check PDF metadata

On Linux, you can check metadata

```bash
sudo apt install -y poppler-utils
pdfinfo code.pdf
```
