# Synthèse

Le groupe CAP Transactions, spécialiste de l'immobilier d'entreprise en Bretagne, possède un département investissement appelé Foncière AALTO. Le site web de Foncière AALTO n'a pas été réalisé par Useweb, le projet concerne un extranet dont l'extranet est disponible sur [client.fonciere-aalto.com](client.fonciere-aalto.com).

Le projet présenté ici est une refonte de l'extranet réalisé, à l'origine, en 2018 par Adrien Beaudouin de Useweb. Foncière AALTO a souhaité une refonte de l'application en septembre 2020, le but était d'améliorer l'application web actuelle en ajoutant des fonctionnalités nécessitant une modification non-négligeable de la base de données ainsi que de créer une toute nouvelle application mobile (Android & iOS) branchée sur une API provenant de l'application web pré-existante.

L'application Foncière AALTO a été réalisée à l'origine avec le framework PHP Laravel, la partie front étant réalisée en Vue.js directement embarqué dans Laravel. Cela qui permet par conséquent de se passer d'API et de communiquer avec les contrôleurs en utilisant le système intégré d'authentification de Laravel (CSRF Token), tout en utilisant l'efficacité de Vue.js.

L'application propose deux applications en une :

- le front back-office, pour les administrateurs, dans l'optique de créer des nouveaux fonds, des nouveaux investisseurs, gérer les souscriptions existantes...
- le front tableau de bord, pour les investisseurs, afin de consulter les informations sur les fonds souscrits, récupérer une synthèse des informations les concernant ou modifier leurs informations personnelles
  
Afin de rendre le déplacement plus aisé pour les administrateurs, une fonctionnalité d'usurpation est disponible dans la liste des investisseurs pour usurper le profil d'un investisseur afin de consulter la partie front de l'application sans avoir à se reconnecter avec un véritable compte investisseur.

L'application étant un extranet, aucune inscription n'est possible par un utilisateur qui arriverait sur la page de connexion, celui-ci doit déjà avoir un compte créé par un administrateur et recevera un e-mail lui indiquant ses identifiants.

\clearpage

# Améliorations

## Application web

- Technologie : Laravel 6, Vue.js 2, MySQL
- Temps de développement : environ un mois

Sur l'application d'origine, les investisseurs ne recevaient pas de nouvelles offres directement, la souscription était manuelle par les administrateurs pour les investisseurs. La mise à jour principale a été de refondre une partie du front pour améliorer la création des fonds afin de permettre d'envoyer des e-mails aux investisseurs pour signaler un nouveau fond et de rendre la souscription directement possible par les investisseurs eux-mêmes dans leur tableau de bord (avec des e-mails de confirmation). De plus, les administrateurs peuvent consulter le back-office pour savoir si les investisseurs ont ouvert la souscription depuis le mail.

Les fonds se sont vu améliorés par des champs supplémentaire (informations, images, fichiers...), les investisseurs ont également la possibilité de rensigner des informations concernant les professionnels financiers qui leurs sont affiliés ainsi que les documents nécessaires pour la souscription. Cette dernière est maintenant validée par un bon de souscription auto-généré d'après les données renseignées que l'utilisateur doit télécharger, signer et renvoyer (par voie numérique ou physique).

Une autre mise à jour a consisté en un système de mise à jour des informations lors de la première connexion d'un utilisateur avec les informations habituelles du compte que l'utilisateur va devoir vérifier. La dernière partie a été un système de répéteur d'étapes avec des champs de texte, des images, des fichiers et des vidéos, hébergée sur YouTube mais exécutables sur l'application web.

### Base de données

La base de données s'est vue améliorée de plusieurs colonnes dans des tables existantes, le but étant d'ajouter des nouvelles données tout en conservant les anciennes si nécessaire. Une table pivot a également été ajoutée.

### Plans futurs

Le client a été satisfait de la production réalisée et a commandé un CRM (*Custom Relatiosnhip Management*) afin de gérer ses investisseurs de manière plus fine, à travers des groupes pour cibler plus efficacement les propositions.

## Application mobile

- Technologie : Flutter 1.2
- Temps de développement : environ un mois

Foncière AALTO a souhaité lancer une application mobile pour que ses investisseurs puisse utiliser leurs services directement sur leur smartphone sans avoir à passer par le site web mais par une application native.

Après une recherche de la technologie la plus adaptée au projet, c'est Flutter qui a été choisi pour la possibilité de porter facilement le projet sur Android et sur iOS avec la même base de code. NativeScript Vue, Cordova et React Native ont été étudiés pour s'assurer des aspects positifs de chaque technologie. Flutter étant soutenu par Google et par une communauté dynamique, c'était le choix le plus intéressant pour ce projet. Il est à noter que le développement en Android (Java ou Kotlin) et en iOS (Swift) n'a pas été retenu à cause du temps de développement trop important. Le seul point négatif est le fait que personne dans l'entreprise n'avait fait de Dart (le langage de Flutter), de ce fait aucune personne ressource ne pouvait être disponible pour les éventuelles difficultés rencontrées sur le projet. Cela nécessitait donc un temps de d'auto-formation et de recherche.

De plus, la mise en place d'une API a été nécessaire car l'application d'origine était embarquée à Laravel, un système d'authentification a aussi dû être ajouté. C'est Laravel Sanctum qui a été choisi avec la mise en place de tokens pour les applications mobile.

L'application mobile actuelle permet donc de :

- se connecter à son espace investisseur
- consulter ses fonds : informations financière globales, documents à téléchargers
  - consulter les actifs occupant les locaux de ses fonds : informations financière globales, documents à télécharger, photos/vidéos de chantier sous forme de carousel
- télécharger son relevé de transactions
- modifier ses informations utilisateur
- modifier les informations de ses consultants
- téléverser des documents financiers
- recevoir des notifications push lorsque des informations sont envoyées depuis le serveur

Le développement s'est fait en s'appuyant sur les paquets officiels et ceux de la communauté afin de rendre certaines fonctionnalités plus faciles à développer. L'application est aujourd'hui disponible sur le Google Play et l'App Store :

- [play.google.com/store/apps/details?id=com.useweb.fonciere.aalto](https://play.google.com/store/apps/details?id=com.useweb.fonciere.aalto)
- [apps.apple.com/fr/app/fonciere-aalto-espace-client/id1549849593](https://apps.apple.com/fr/app/fonciere-aalto-espace-client/id1549849593)

Note : l'application Foncière AALTO nécessite une connexion dès le premier écran, elle ne peut donc être testée directement. Il est tout à fait possible de faire parvenir à toute personne le souhaitant une version permettant le test des fonctionnalités.
