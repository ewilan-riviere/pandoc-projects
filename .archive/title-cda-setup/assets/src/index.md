# Checklist

- [ ] english speech
- [ ] Dossier professionnel, résumé, rapport d'activité, livret d'évaluation, carte d'identité, convocation papier
- [ ] mac
  - [ ] connection multi screen
  - [ ] emulator
  - [ ] repository of project
  - [ ] bookshelves

## Questions

- Écrire des requêtes SQL : faire une requête avec double jointure et une condition.
- Écrire des requêtes SQL avec un left ou right join.
- Que pensez-vous de votre schéma de bdd ?
- Que feriez-vous pour faire évoluer votre schéma de bdd.
- Expliquez nous comment fonctionnent les injections SQL.
- Expliquez nous comment fonctionnent les rôles d’accès de votre application.
- Expliquez nous comment fonctionne l’encodage des mots de passe de votre application.
- Expliquez nous les cardinalités de votre schéma de bdd.
- Question de sécurité sur HTTPS.
- Question de sécurité sur Certificats https.
- Comment fonctionnent les certificats http.
- Quel est le contenu des certificats http.
- Question de sécurité sur les injections SQL (les risques de faire des requêtes à la main?).
- Question de sécurité sur Hash et chiffrement (du mot de passe par exemple).
- Parlez-nous des tests unitaires.
- Parlez-nous des tests fonctionnels.
- Parlez-nous des tests de non-régression.
- Question sur le cycle en V  
- Question sur les méthodes agiles (SCRUM, etc.)  
- Qu'est-ce qu'un MVP?
- Demande de rajouter une table à votre bdd avec clé étrangère pour une relation One to Many  
- Comment transformeriez vous une relation One to Many en Many to Many
- Quelles sont les différentes méthodes de gestion de projet.
- Quels sont les différents types de test ? (test de securité, tests fonctionnels, stress test...)
- Que doit-on faire avant une mise en production ?
- Quels organismes informent sur les nouvelles failles de securité et la securité en general ? (owasp…)
- Parlez nous des media queries ?
- Citez nous différentes attaques informatiques possibles.

# Sécurité

## Encodage mots de passe

Laravel utilise Bcrypt par défaut via `Hash`, `Hack::check` permet de vérifier que le chiffrage d'un mot de passe correspond à un autre pour la connexion.

À partir d'une donnée fournie en entrée, calcule une empreinte numérique servant à identifier rapidement la donnée initiale, au même titre qu'une signature pour identifier une personne.

## HTTPS

![alt](assets/images/http-vs-https.png)

L'HyperText Transfer Protocol Secure est la combinaison du HTTP avec une couche de chiffrement comme SSL ou TLS.

HTTPS permet au visiteur de vérifier l'identité du site web auquel il accède, grâce à un certificat d'authentification émis par une autorité tierce, réputée fiable (et faisant généralement partie de la liste blanche des navigateurs internet). Il garantit théoriquement la confidentialité et l'intégrité des données envoyées par l'utilisateur (notamment des informations entrées dans les formulaires) et reçues du serveur. Il peut permettre de valider l'identité du visiteur, si celui-ci utilise également un certificat d'authentification client.

- le client — par exemple le navigateur Web — contacte un serveur — par exemple Wikipédia — et demande une connexion sécurisée, en lui présentant un certain nombre de méthodes de chiffrement de la connexion (des suites cryptographiques)
- le serveur répond en confirmant pouvoir dialoguer de manière sécurisée et en choisissant dans cette liste une méthode de chiffrement et surtout, en produisant un certificat garantissant qu'il est bien le serveur en question et pas un serveur pirate déguisé (on parle de l'homme du milieu). Ces certificats électroniques sont délivrés par une autorité tierce en laquelle tout le monde a confiance, un peu comme un notaire dans la vie courante, et le client a pu vérifier auprès de cette autorité que le certificat est authentique (il y a d'autres variantes, mais c'est le principe général). Le certificat contient aussi un cadenas en quelque sorte (une clé dite publique) qui permet de prendre un message et de le mélanger avec ce cadenas pour le rendre complètement secret et uniquement déchiffrable par le serveur qui a émis ce cadenas (grâce à une clé dite privée, que seul le serveur détient, on parle de chiffrement asymétrique)
- cela permet au client d'envoyer de manière secrète un code (une clé symétrique) qui sera mélangé à tous les échanges entre le serveur et le client de façon que tous les contenus de la communication — y compris l'adresse même du site web, l'URL — soient chiffrés. Pour cela on mélange le contenu avec le code, ce qui donne un message indéchiffrable et à l'arrivée refaire la même opération avec ce message redonne le contenu en clair, comme dans un miroir.

## Informations

- CVE (*Common Vulnerabilities and Exposures*) est la référence mondiale en matière de failles et vulnérabilités. Malheureusement, elle n’est pas nécessairement la plus agréable en ce qui concerne la navigation et la recherche.
- CERT-FR, le centre gouvernemental de veille, d’alerte et de réponse aux attaques informatiques, qui dépend de l’ANSSI, est la base de données officielle française autour des vulnérabilités. C’est aussi une source indispensable de bonnes pratiques.
- Security Focus est l’une des plus complètes qui soient. Sa fonction de recherche avancée permet de sélectionner un éditeur, puis un logiciel et enfin un numéro de version et d’afficher toutes les vulnérabilités connues s’y rapportant. En cliquant sur l’une des occurrences remontées, vous pouvez connaître non seulement si celle-ci est exploitée (autrement dit si des exploits sont en circulation) et les solutions à implémenter pour s’en protéger.
- Exploit-db.com est le site de référence pour tout savoir sur les exploits en vogue pour pénétrer les défenses des entreprises. Le site recueille et regroupe tous les exploits rencontrés sur Internet (« In The Wild ») ou découverts par des chercheurs du monde entier.
- Le NVD (*National Vulnerability Database*) est la base d’information officielle du gouvernement américain avec des fonctions de recherche avancée et une mise en évidence des failles les plus critiques.
- L’Enisa (*European Union Agency for Network and Information Security*) est avant tout une source de bonnes pratiques et de conseils pour protéger ses systèmes d’information.
- LiveHacking est la référence du Hacking éthique. C’est une mine de données et d’informations présentées sous un axe très hacker.
- L’Infosec Institute est un site toujours pratique pour suivre les attaques les plus répandues du moment.
- OWASP, Open Web Application Security Project is an online community that produces freely-available articles, methodologies, documentation, tools, and technologies in the field of web application security.

## All

### CSRF

En sécurité des systèmes d'information, le cross-site request forgery, abrégé CSRF (parfois prononcé sea-surf en anglais) ou XSRF, est un type de vulnérabilité des services d'authentification web. L’objet de cette attaque est de transmettre à un utilisateur authentifié une requête HTTP falsifiée qui pointe sur une action interne au site, afin qu'il l'exécute sans en avoir conscience et en utilisant ses propres droits. L’utilisateur devient donc complice d’une attaque sans même s'en rendre compte. L'attaque étant actionnée par l'utilisateur, un grand nombre de systèmes d'authentification sont contournés.

- token

### XSS

Le cross-site scripting (abrégé XSS) est un type de faille de sécurité des sites web permettant d'injecter du contenu dans une page, provoquant ainsi des actions sur les navigateurs web visitant la page. Les possibilités des XSS sont très larges puisque l'attaquant peut utiliser tous les langages pris en charge par le navigateur (JavaScript, Java...) et de nouvelles possibilités sont régulièrement découvertes notamment avec l'arrivée de nouvelles technologies comme HTML5. Il est par exemple possible de rediriger vers un autre site pour de l'hameçonnage ou encore de voler la session en récupérant les cookies.

Laravel

- validation rules
- purification des inputs (échappement)
- ne jamais faire confiance aux données

### SQL injections

La faille SQLi, abréviation de SQL Injection, soit injection SQL en français, est un groupe de méthodes d'exploitation de faille de sécurité d'une application interagissant avec une base de données. Elle permet d'injecter dans la requête SQL en cours un morceau de requête non prévu par le système et pouvant en compromettre la sécurité.

- <https://cyberpanda.la/blog/laravel-sql-injections>

```sql
SELECT uid FROM Users WHERE name = 'Dupont' AND password = '' or 1 --';
```

Utiliser des caractères spéciaux à insérer dans la requête pour se connecter ou effacer des données. Laravel exécute une requête préparée à travers son ORM.

Risques

- query param : vérifier la donnée possible
- validate si variable passée dans les rules
- raw queries

### DDoS

Une attaque par déni de service (abr. DoS attack pour Denial of Service attack en anglais) est une attaque informatique ayant pour but de rendre indisponible un service, d'empêcher les utilisateurs légitimes d'un service de l'utiliser. À l’heure actuelle la grande majorité de ces attaques se font à partir de plusieurs sources, on parle alors d'attaque par déni de service distribuée (abr. DDoS attack pour Distributed Denial of Service attack).

# CSS

```css
@media screen and (max-width: 640px) {
  .bloc {
    display:block;
    clear:both;
  }
}
```

```css
@media screen and (min-width: 200px) and (max-width: 640px) {
  .bloc {
    display:block;
    clear:both;
  }
}
```

# Management

## Cycle en V

Le cycle en V (« V model » ou « Vee model » en anglais) est un modèle d'organisation des activités d'un projet qui se caractérise par un flux d'activité descendant qui détaille le produit jusqu'à sa réalisation, et un flux ascendant, qui assemble le produit en vérifiant sa qualité. Ce modèle est issu du modèle en cascade dont il reprend l'approche séquentielle et linéaire de phases.

Il l'enrichit cependant d'activités d'intégration de système à partir de composants plus élémentaires, et il met en regard chaque phase de production successive avec sa phase de validation correspondante, lui conférant ainsi la forme d'un V1.

## Agilité

Les pratiques agiles mettent en avant la collaboration entre des équipes auto-organisées et pluridisciplinaires et leurs clients1. Elles s'appuient sur l'utilisation d'un cadre méthodologique léger mais suffisant centré sur l'humain et la communication2. Elles préconisent une planification adaptative, un développement évolutif, une livraison précoce et une amélioration continue, et elles encouragent des réponses flexibles au changement3,4.

Cette approche a été popularisée à partir de 2001 par le Manifeste pour le développement agile de logiciels. Les quatre valeurs et les douze principes adoptés dans ce manifeste sont issus d'un large éventail de méthodes dont Scrum et eXtreme Programming. Depuis lors, les méthodes qui s'inscrivent dans la philosophie de ce manifeste sont appelées « méthodes agiles ».

Les méthodes agiles se veulent plus pragmatiques que les méthodes traditionnelles, impliquent au maximum le demandeur

## MVP

Dans le cadre de la conception de produit, le produit minimum viable (ou MVP, de l'anglais : minimum viable product) est la version d'un produit qui permet d'obtenir un maximum de retours client avec un minimum d'effort. Par extension, il désigne aussi la stratégie utilisée pour fabriquer, tester et mettre sur le marché ce produit. L'intérêt du produit minimum viable est d'évaluer la viabilité d'un nouveau modèle d'entreprise.

# MEP

Vérifier que tout fonctionne sur un environnement de recette.

# Tests

## Tests unitaires

En programmation informatique, le test unitaire (ou « T.U. », ou « U.T. » en anglais) est une procédure permettant de vérifier le bon fonctionnement d'une partie précise d'un logiciel ou d'une portion d'un programme (appelée « unité » ou « module »).

Dans les applications non critiques, l'écriture des tests unitaires a longtemps été considérée comme une tâche secondaire. Cependant, les méthodes Extreme programming (XP) ou Test Driven Development (TDD) ont remis les tests unitaires, appelés « tests du programmeur », au centre de l'activité de programmation.

## Tests fonctionnels

Un test fonctionnel est le test qui servira à tester automatiquement toutes les fonctionnalités de votre application. "Toutes", ça veut dire : les fonctionnalités qui étaient demandées dans le cahier des charges du projet (ou autres spécifications). Par exemple, vous pourrez avoir à tester qu'un membre peut bien s'inscrire, se connecter, se déconnecter…

## Tests non-regression

Un test de non régression permet de valider que la mise en ligne d’une nouvelle fonctionnalité sur un logiciel n’impactera pas les fonctions déjà existantes. Les tests fonctionnels auront bien validé que la nouvelle fonction est opérationnelle mais c’est le test de non régression qui validera que cette dernière n’impacte pas les autres fonctionnalités du logiciel.

Le test de non régression est déployé sur un environnement de recette et doit vérifier au minimum que les fonctionnalités principales ou « critiques » du logiciel sont toujours disponibles après la livraison de nouveaux développements.

# SQL

- (INNER) JOIN: Returns records that have matching values in both tables
- LEFT (OUTER) JOIN: Returns all records from the left table, and the matched records from the right table
- RIGHT (OUTER) JOIN: Returns all records from the right table, and the matched records from the left table
- FULL (OUTER) JOIN: Returns all records when there is a match in either left or right table

```sql
SELECT c.name, l.label, le.signed_on, t.name
FROM real_estate_companies as c,
    locals as l,
    leases as le,
    tenants as t
WHERE c.id = 15
    AND c.id = l.real_estate_company_id
    AND le.local_id = l.id
    AND le.tenant_id = t.id
```

```sql
SELECT l.label, steps.step_number,
    steps.title, steps.text
FROM locals as l,
    locals_steps as steps
WHERE l.id = 64
    AND l.id = steps.local_id
```

```sql
SELECT i.email, c.id, c.name,
        c.total_investment_amount, c.status,
        c.rcs, pivot.shares_quantity,
        pivot.collected_rent_amount,
        pivot.is_validate, pivot.has_subscribed
    FROM investor_real_estate_company AS pivot
INNER JOIN real_estate_companies AS c
    ON c.id = pivot.real_estate_company_id
INNER JOIN investors AS i
    ON i.id = pivot.investor_id
        WHERE i.email = 'michel.trauth@useweb.com'
            AND pivot.has_subscribed = 1
```

```sql
SELECT i.email, c.id, c.name,
    c.total_investment_amount, c.status,
    c.rcs, pivot.shares_quantity,
    pivot.collected_rent_amount,
    pivot.is_validate, pivot.has_subscribed
FROM investors as i,
 real_estate_companies as c,
    investor_real_estate_company as pivot
WHERE i.email = 'michel.trauth@useweb.com'
    AND pivot.has_subscribed = 1
    AND i.id = pivot.investor_id
    AND c.id = pivot.real_estate_company_id
```

![alt](assets/images/sql.jpg)

- **INNER JOIN** : jointure interne pour retourner les enregistrements quand la condition est vrai dans les 2 tables. C’est l’une des jointures les plus communes.
- CROSS JOIN : jointure croisée permettant de faire le produit cartésien de 2 tables. En d’autres mots, permet de joindre chaque lignes d’une table avec chaque lignes d’une seconde table. Attention, le nombre de résultats est en général très élevé.
- **LEFT JOIN** (ou LEFT OUTER JOIN) : jointure externe pour retourner tous les enregistrements de la table de gauche (LEFT = gauche) même si la condition n’est pas vérifié dans l’autre table.
- **RIGHT JOIN** (ou RIGHT OUTER JOIN) : jointure externe pour retourner tous les enregistrements de la table de droite (RIGHT = droite) même si la condition n’est pas vérifié dans l’autre table.
- FULL JOIN (ou FULL OUTER JOIN) : jointure externe pour retourner les résultats quand la condition est vrai dans au moins une des 2 tables.
- SELF JOIN : permet d’effectuer une jointure d’une table avec elle-même comme si c’était une autre table.
- NATURAL JOIN : jointure naturelle entre 2 tables s’il y a au moins une colonne qui porte le même nom entre les 2 tables SQL
- UNION JOIN : jointure d’union

# Administratif

Votre session d’examen final conduisant au Titre de Concepteur-trice Développeur-euse d’Applications se déroulera aux dates suivantes (cf : tableau ci-dessous)
Pour vous inscrire, merci de nous retourner le formulaire de préinscription (PJ) complété ainsi qu’un chèque de caution de 150 € avant la date limite d’inscription.

- DP
- Résumé du rapport d’activité
- Rapport d’activité
- Livret d’évaluation
- PPT à faire mais pas à fournir

## Calendrier

- Session CDA : 08 juin 2021 (sur une ½ journée)
- Limite inscription : vendredi 05 mars 2021
- Limite désistement : vendredi 23 avril 2021
- Limite[^1] retour versions électroniques à rapports35@eni-ecole.fr : Lundi 31 mai 2021 Avant 10h00
  - Résumé du cahier des charges
  - le Dossier Professionnel (DP) : NomPrenomDP.pdf
  - le Rapport d’activités (40 à 60 pages annexes non comprises, « Compétences Rapport CDA.docx » ci-joint à intégrer) : NomPrenomRapport.pdf
- Un mois avant le début de la session, vous recevrez votre convocation papier : conservez-la pour pouvoir vous présenter avec le jour de l’examen

> Si, dans le cadre d’une situation de handicap, vous avez besoin d’un aménagement particulier, merci de revenir vers moi très rapidement.
>
> Une inscription n’est validée qu’à réception du bulletin de préinscription (ci-joint) et du chèque de caution de 150€ à l’ordre de ENI Ecole Informatique.

[^1]: Un seul de ces délais est dépassé et le chèque de caution est encaissé. En cas d’absence à la soutenance, le chèque sera également encaissé, sauf motif médical justifié par un arrêt maladie.

## Procédure d’inscription

Si vous êtes en formation dans notre centre, vous pouvez remettre le bulletin de préinscription avec le chèque de caution (montant 150€ à l’ordre de ENI Ecole Informatique) directement au service administratif et ce avant la fin du délai d’inscription.  
Le délai d’inscription passé vous devrez vous inscrire à une autre session sous réserve des places disponibles.

>Vous pouvez également envoyer le bulletin de préinscription ainsi que le chèque par courrier :  
>  
>ENI Ecole Informatique  
>8 rue Léo Lagrange  
>35131 CHARTRES DE BRETAGNE  

## Organisation et durée des épreuves

- Lieu : dans nos locaux – Chartres de Bretagne
- La durée du passage devant le jury est de 1h45 minimum.

### L’épreuve de Synthèse

>1h25

#### Présentation du Projet réalisé en entreprise

>40 min

Le candidat commence sa présentation au jury par un résumé en anglais de son projet. Il présente ensuite son projet à l’aide d’un support de présentation réalisé en amont de l’épreuve, et selon ce canevas :

- Présentation de l’entreprise et/ou du service
- Contexte du projet (cahier des charges, contraintes, livrables attendus)
- Gestion de projet (planning et suivi, environnement humain et technique, objectifs de qualité)
- Analyse du besoin
- Conception et codage
- Présentation des éléments les plus significatifs de l’interface de l’application
- Présentation du jeu d’essai de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues) et analyse des écarts éventuels
- Présentation d’un exemple de recherche effectuée à partir de site anglophone
- Synthèse et conclusion (satisfactions et difficultés rencontrées)

#### Entretien technique

>45 min

Le jury questionne le candidat sur la base de son dossier de projet et de sa présentation afin de s’assurer de la maîtrise des compétences couvertes par le projet. Un questionnement complémentaire lui permet d’évaluer les compétences qui ne sont pas couvertes par le projet.

#### L’entretien final

>20 min

Echanges avec le candidat sur le DP.
Le jury vérifie la capacité du candidat à enchainer les 3 activités du titre. Il revient notamment sur la gestion des projets, sur la qualité des développements et sur l’aspect sécurité.
  
## Contenu du rapport d’activité professionnelle

Le projet retenu pour le rapport d’activité doit couvrir les trois activités du titre CDA. Si certaines des compétences contenues dans les activités ne sont pas pratiquées pendant le projet, il faudra alors être capable de faire la preuve que vous maîtrisez ces compétences.
Le dossier de projet à préparer en vue de l’épreuve respecte ce plan type :

- Liste des compétences du référentiel qui sont couvertes par le projet (cf pièce jointe)
- Résumé du projet en anglais d’une longueur d’environ 20 lignes soit 200 à 250 mots, ou environ 1200 caractères espaces non compris
- Cahier des charges ou expression des besoins du projet
- Gestion de projet (planning et suivi, environnement humain et technique, objectifs de qualité)
- Spécifications fonctionnelles du projet
- Spécifications techniques du projet, élaborées par le candidat, y compris pour la sécurité
- Réalisations du candidat comportant les extraits de code les plus significatifs et en les argumentant, y compris pou la sécurité
- Présentation du jeu d’essai élaboré par le candidat de la fonctionnalité la plus représentative (données en entrée, données attendues, données obtenues)
- Description de la veille, effectuée par le candidat durant le projet, sur les vulnérabilités de sécurité
- Description d’une situation de travail ayant nécessité une recherche et effectuée par le candidat durant le projet
- Ne pas oublier la présentation de l’entreprise, du service,
- Ne pas inonder le rapport de copies d’écran ou bien d’extraits de code (les mettre en annexe si besoin),
- Attention à la rédaction et aux fautes d’orthographe.
  
### Le projet doit couvrir les compétences suivantes

Pour l’activité 1 «Concevoir et Développer des composants d’interface utilisateur en intégrant les recommandations de sécurité» :

- Maquetter une application
- Développer des composants d’accès aux données
- Développer la partie front-end d’une interface utilisateur Web
- Développer la partie back-end d’une interface utilisateur Web

Pour l’activité 2 «Concevoir et développer la persistance des données en intégrant les recommandations de sécurité» :

- Mettre en place une base de données

Pour l’activité 3 «Concevoir et développer une application multicouche répartie en intégrant les recommandations de sécurité» :

- Concevoir une application
- Développer des composants métier
- Construire une application organisée en couches

Sa longueur hors annexes est de 40 à 60 pages, avec 75000 caractères espaces non-compris.

## DP

Ce document permet au (à la) candidat(e) de mettre en valeur ses compétences en décrivant, de manière détaillée et à partir d'exemples concrets, les activités professionnelles en rapport direct et étroit avec le titre professionnel visé.
Le (la) candidat(e) a également la possibilité de fournir tout support illustrant cette description.

1 mois au plus tard avant le début de la session, vous pourrez adresser votre DP à votre responsable de formation pour une relecture. Passé ce délai, aucune lecture ne sera effectuée.

## Résumé du cahier des charges

Le candidat remet à l’organisateur un résumé du cahier des charges du projet, d’une longueur de 200 à 300 mots qui sera destiné au Jury.
Vous pouvez y inclure des informations du type :

- Présentation succincte de l’entreprise
- Contexte du projet
- Objectifs,
- Besoins
- Environnement et technologie utilisée
- Etc…

## Fiche de suivi de projet en entreprise

Vous trouverez en pièce jointe la fiche des compétences couvertes par le projet à compléter et à faire signer par l’entreprise et vous-même. Celle-ci sera à intégrer dans votre rapport d’activités.
Je vous rappelle que le projet doit couvrir au minimum les 7 compétences obligatoires.

Enfin, n’hésitez pas à vous référez au REAC (Référentiel emploi Activités Compétences) et au RC (Référentiel de Certification) du Titre.

## Résultats

Les résultats seront affichés à l’ENI une fois que les Procès Verbaux seront validés et transmis par la DIRECCTE (délais généralement d’environ 15 jours – 3 semaines).
