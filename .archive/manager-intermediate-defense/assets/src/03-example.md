# Exemple : Participation

>Le projet prit en exemple ci-dessous est l'un des plus compliqués qui a été mené par Useweb, j'aurai pu prendre un projet simple et mené avec efficacité, mais je n'aurai alors que peu de choses à en dire. Il s'agit ici d'analyser pourquoi ce projet s'est passé ainsi pour éviter de devoir faire face de nouveau à ces problèmes.
>Je n'ai pas piloté le projet, mais ayant été la développeuse principale, j'ai pu prendre un certain recul sur ce projet pour pouvoir l'analyser en détails.

Pour souligner ces activités, je vais prendre l'exemple de **Participation[^participation], un site en extranet développé pour le Ministère de la transition écologique** en France. Ce projet est la refonte d'un site web qui a été réalisé deux années plus tôt, une plateforme appelée ECHOPP[^echopp] réalisé pour la DREAL[^dreal] de Bretagne. Le projet original développé en 2019 proposait une plateforme en extranet permettant aux particuliers et aux acteurs publics de partager des expériences sur leurs actions sur le terrain concernant des sujets environnementaux et la mise à disposition des outils de participation publique. Le Ministère de la transition écologique s'est emparé de cette notion pour la porter au niveau national et a souhaité la mise en place d'une plateforme de l'État pour réunir tous les acteurs concernés.

C'est en 2021 que le ministère a contacté Useweb afin de discuter de la possibilité de ce projet, à savoir reprendre la plupart des contenus de l'ancienne plateforme mais réorganiser toute l'interface pour une meilleure (+UIUX) et aux couleurs de l'Etat français avec l'utilisation du DSFR[^dsfr] qui est le *Système de Design de l'État* en open-source sur GitHub. Au lancement, cette plateforme doit disponible en interne dans un premier temps puis sur l’ensemble du territoire national. Le budget de ce projet est d'environ 45 000 euros.

## En amont

Différents ateliers (+UIUX) ont été mis en place par Useweb afin de proposer différentes possibilités graphiques, par conséquent cela nous garantissait, en théorie, une bonne compréhension (des deux côtés) de ce que la plateforme allait être. Cela a mené à la création de *personas* représentant les futurs utilisateurs afin de considérer chaque fonctionnalité demandée et l'appréhender à travers le prisme de l'(+UIUX). Le but de la plateforme était de respecter plusieurs conditions :

- Simple d’usage
- Utile
- Interactive
- Accessible à tous
- Riche en contenus
- Éco-conçue
- Respectant les normes de création Internet du Gouvernement

Le but étant de donner au public ayant accès à la plateforme une boîte à outils pour la participation publique, pour y trouver de façon simple tout ce dont les acteurs ont besoin pour les accompagner dans leur démarche : retours d’expérience, cadre réglementaire, cahier des charges, formation, etc.
Plus encore, la plateforme est considérée par la plupart des participants comme un futur vecteur de lien entre eux, leurs collègues, les collectivités, mais aussi avec les membres d’autres Ministères. Ils l’imaginent interactive, se transformant au gré de leurs besoins et alimentée en permanence de nouveaux contenus.

C'est la méthode AGILE qui a été proposée au client afin qu'il y ait le plus d'échanges possibles entre nous pour que la plateforme respecte au maximum leurs demandes et puisse continuer à évoluer par la suite.

L'une des conditions était le respect de la charte graphique de l'État (le DSFR) qui est très strict avec une sélection de couleurs liées à des règles, deux polices disponibles, des composants d'interface tous décrits précisément avec des règles d'utilisation selon les circonstances.

Cela a été le premier point de légère friction, le pôle webdesign n'a pas pu faire de proposition particulièrement élaborée au vu des règles très précises de la charte graphique du DSFR. Cela a posé quelques problèmes à notre client qui trouvait que le site allait être "*trop blanc*" ou "*manquait de couleurs*" sachant que c'était eux-mêmes qui nous ont demandé d'utiliser cette charte. Une autre demande a été celle d'un logo moderne mettant en avant le thème de "Participation" qui a été l'une des rares libertés du pôle design. Mais le client, après avoir choisi un logo parmi les propositions graphiques, est allé contacter sa hiérarchie, est revenu sur la question en indiquant que le logo ne pouvait pas apparaitre sur l'en-tête du site web qui devait respecter la charte graphique.

>Ces premiers problèmes proviennent principalement d'un client à l'avis très changeant à cause de la présence de trois personnes différentes qui sont nos interlocutrices dont deux qui n'ont que des connaissances limitées dans le domaine du web et de ses contraintes.
>Si nous n'avions eu affaire qu'à une seule personne dont l'avis était stable, ce qui s'est passé en amont du projet aurait permis d'anticiper largement les problèmes que nous avons pu rencontrer par la suite. Il reste une part de la responsabilité de notre côté dans notre omission d'avoir rédigé un cahier des charges suffisamment solide.

## Un développement instable

Dès le début du projet, l'équipe de développement voulait partir sur l'utilisation de Strapi[^strapi] pour répondre aux besoins métiers. Ce (+FRAMEWORK) offre plusieurs possibilités dont les suivantes :

- Tableau de bord généré selon les entités
- Développement du back-end ultra rapide
- Entités et relations créées à la volée
- Connecté à MySQL
- Génère une API, disponible en REST et en GraphQL
- Modules disponibles pour ajouter des fonctionnalités
- Bibliothèque de médias avec champs riches
- Propulsé par Node.js

De plus, ce framework permettait de créer un système d'authentification qui convenait très bien au framework choisit pour le (+FRONTEND), Nuxt.js, basé sur Vue.js 2, qui devait utiliser le DSFR comme système de style et communiquer avec une (+API) créée par Strapi. Nuxt.js propose plusieurs aspects très positifs :

- Développement rapide, meilleur SEO avec un rendu SSR
- Routes implicites, plugins et middlewares
- Modules *plug-and-play*, notamment pour Strapi
- La recherche globale utilise Meilisearch, qui connecté à Strapi, permet de retrouver n'importe quelle entité rapidement par recherche multi-champs

D'un côté nous avons pu construire le (+BACKOFFICE) avec Strapi qui nous permet de créer rapidement et facilement des entités dans une interface générée d'après ces entités. Le seul défaut est la maîtrise plus faible que nous avons sur les migrations et donc sur la base de données. Et de l'autre nous avons un (+FRONTEND) avec les contenus consultables uniquement si l'utilisateur est connecté et avec de multiples formulaires permettant d'ajouter des contenus.

Les besoins client ont fait émerger un nombre d'entités assez important qui ne cadrait pas tellement avec sa volonté de faire une plateforme simple à comprendre et facile à utiliser mais Strpai nous a permis de les ajouter très rapidement, en voici une illustration.

![Relations principales](assets/images/relations.jpg)

- Entités principales : beaucoup champs de texte riche pour chaque entité, système de publication, catégories pour chaque entité, images
  - cadres juridiques
  - construire sa démarche
  - retours d'expérience
  - formations
  - actualités
- ressources utiles : fichiers liés aux autres entités
- lexique
- utilisateurs : administrateurs pour le (+BACKOFFICE) et utilisateurs pour le (+FRONTEND)

>Le premier problème a été le nombre de champs disponibles pour chaque entité avec la volonté d'avoir presque toujours un champ de texte riche (qui permet de mettre du style sur le texte) diminuant le contrôle qu'on peut avoir sur le contenu. Cela créait deux problèmes : un temps de chargement plus lourd à cause de ces champs complexes et une (+UIUX) qui en souffre à cause du trop grand nombre de champs par entité. Cela a impacté les demandes du client qui souhaitait un site web éco-conçu et avec une utilisation simple et efficace.

Les retours clients étaient rares, il aurait été sans doute préférable de considérer les problèmes auxquels nous faisions face et les confronter directement au client afin d'obtenir des réponses claires de sa part tout en lui expliquant les conséquences de chaque possibilité. Cependant, comme nous étions sous pression temporelle, nous avons préféré continuer à avancer jusqu'au déploiement d'une version de test accessible au client.

Lors de ce déploiement, nous avons fait une présentation client dirigée par moi-même afin de présenter chaque fonctionnalité au client pour que celui-ci puisse voir où en était le projet. Cette réunion s'est très bien passée, les acteurs étant plutôt satisfaits des fonctionnalités présentées et faisant quelques retours assez peu impactants. La version de test est restée en ligne pendant plus de trois mois, les acteurs avec lesquels nous étions en relation pouvaient y accéder, mais nous avons eu très peu de retours après la réunion.

## Des retours tardifs

Nous sommes restés sans nouvelle pendant plus d'un mois alors que le déploiement en production était en questionnement. Encore une fois, nous étions sous pression temporelle pour les projets qui arrivaient et donc nous n'avons pas recontacté le client pour le relancer, ce qui a généré des problèmes par la suite.

Lorsque le client nous a recontacté, le ton était différent, beaucoup plus pressant et dirigiste afin de *demander* un déploiement en production rapidement. Ils ont également fait un très grand nombre de retours qui n'étaient pas du tout prévu à l'origine. Nous avons dû par exemple limiter la largeur de chaque page à environ 1300 pixels parce que les écrans du client étaient limités à cette largeur. C'est une information que nous avons donc obtenu plus de cinq mois après : le lancement du projet, la validation des maquettes, des réunions multiples, une présentation et un serveur de recette disponible. Cela a impacté toutes les pages et la façon dont les contenus étaient proposés. En plus de cela, nous avons reçu aussi de très nombreux retours sur plusieurs points qui ont augmenté les tensions entre Useweb et le client.

Ces tensions ont pu être diminuées en partie par des réunions multiples afin de diminuer les frictions qui avaient atteint un niveau préoccupant. Nous avons accédé à toutes les demandes du client afin de clore le projet et de lancer la phase de production.

>Aujourd'hui, ce projet est considéré comme terminé, avec la possibilité de TMA mais avec des relations client compliquées où des tensions restent malgré les efforts qui ont été fait pour tenter de les faire disparaitre.

[^echopp]: <https://www.outils-participation-public.fr/>
[^participation]: <https://participation.developpement-durable.gouv.fr/>
[^dreal]: Direction régionale de l’environnement, de l’aménagement et du logement, <https://www.bretagne.developpement-durable.gouv.fr/>
[^dsfr]: <https://github.com/GouvernementFR/dsfr>
[^strapi]: <https://strapi.io/>
