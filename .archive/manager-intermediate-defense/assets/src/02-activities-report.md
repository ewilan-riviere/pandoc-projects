# Mes activités en entreprise

Le centre d'activité de Useweb étant la création de sites web, mes activités sont principalement liées à ce domaine. Les clients sont généralement des entreprises locales, comme l'imprimerie Le Galliard à Cesson-Sévigné ou bien nationales, comme l'agence immobilière Laforêt mais nous avons également des clients en interne comme pour Naviso. Les applications web réalisées respectent un modèle qui est souvent le même : une partie (+FRONTEND), une partie (+BACKEND) et les deux communiquant à travers une (+API), l'idée étant d'utiliser des technologies récentes et modernes permettant un développement rapide et efficace avec une maintenance plus simple sur le long terme. Par conséquent, nous n'utilisons pas de CMS tel que Wordpress ou encore Drupal[^drupal] mais nous préférons développer avec des outils permettant davantage de latitude, quand bien même cela nécessite de prendre plus de temps.

Concernant ce dernier point, cela est parfois source de perte de certains appels d'offre, car si le temps de développement est plus important, cela influe sur le prix proposé alors que les autres agences proposent parfois des solutions avec Wordpress qui coûteront moins cher. C'est pourquoi nous essayons de promouvoir d'autres plus-values à travers des propositions graphiques de la part du pôle webdesign, de notre expertise en SEO, du sur-mesure proposé à nos clients et de notre écoute de leurs besoins. Nous prônons également des technologies modernes et open-sources afin que les projets puissent être repris par d'autres prestataires si nécessaire.

## Un environnement ouvert

Useweb est dans un open-space qu'elle partage avec deux autres entreprises du pôle technologique (Sogescot et Naviso), je suis donc généralement entourée par mes collègues et nous avons une très bonne entente. J'effectue mes missions dans l'équipe de développement de Useweb, sous la supervision de mon manager Michel TRAUTH. La plupart du temps, je m'occupe de mes tâches de manière autonome ou en équipe si un sujet demande qu'on prenne de la distance pour évaluer l'angle d'approche d'une tâche particulièrement coûteuse en temps. Ces tâches me sont assignées par mon manager qui s'occupe de définir la priorité de chacune d'entre-elles, une priorisation qui peut avoir tendance à se voir modifier selon l'urgence d'avancer sur un projet plutôt qu'un autre. Je suis cependant libre de faire des propositions pour remonter des problèmes qui n'ont pas été anticipés, par exemple une solution technique qui ne cadre pas avec la demande client, ce qui peut engager une remise en question et la recherche d'une solution plus adaptée.

Je travaille sur un îlot de quatre personnes avec un ordinateur portable livré par la (+DSI) et un écran secondaire utilisable si nécessaire, la machine que j'utilise me permet de travailler efficacement et de pouvoir aller en réunion avec celle-ci si cela s'avère nécessaire. Je gère mon environnement comme je le souhaite, la seule obligation étant de devoir utiliser le système d'exploitation installé par la (+DSI), à savoir Windows[^windows] et d'envoyer mon travail sur BitBucket, une plateforme permettant de partager mon travail avec mes collègues. J'ai une liberté totale sur les logiciels que j'utilise tant que je peux travailler en équipe et, mes collègues et moi-même recherchant une certaine efficience, il nous arrive de partager nos configurations entre nous, ce qui est très utile pour améliorer certains processus par l'apport d'un oeil extérieur. L'outil de tickets, pour gérer les tâches, qui est utilisé par l'équipe est Redmine, où nous pouvons communiquer avec les clients et proposer nos estimations.

## Des libertés de travail

J'utilise Visual Studio Code comme (+IDE), très flexible il permet de travailler avec des langages très différents grâce à des extensions qui permettent d'améliorer nettement ma productivité. J'utilise Git qui permet de versionner le code afin de collaborer plus facilement en équipe et d'anticiper les éventuels problèmes comme devoir revenir en arrière suite à une mise en ligne problématique. Les langages qui sont le plus utilisés à Useweb sont le PHP pour le (+BACKEND) et Vue.js pour le (+FRONTEND), deux langages qui sont très bien adaptés à Visual Studio Code. Il m'arrive d'utiliser d'autres langages comme du Dart lorsque je fais du développement mobile, ce qui nécessite des outils supplémentaires comme un émulateur mobile. J'apprécie modifier mon environnement de travail si cela me permet de mieux travailler et mon outil le plus important est le terminal qui me permet de réaliser des tâches rapidement de la mise en place d'un environnement de travail sur ma machine au déploiement en production d'une application en (+SSH) en passant par l'installation rapide de paquets logiciels pour configurer mon espace de travail rapidement.

## Des missions variées

En entreprise mes activités ont évolué avec le temps, en diversité pour les nouvelles et en complexité pour celles que j'avais déjà.

![Mes missions actuelles](assets/images/activities.jpg)

### Le back-end

Dans le web, c'est ce qui sera utilisé pour gérer les données en utilisant des langages serveur comme du PHP ou du Node.js à Useweb. Notre équipe de développement est spécialisée dans la mise en place de (+^FRAMEWORK) modernes tels que (+LARAVEL) (PHP) ou Strapi (Node.js) afin de créer les entités qui représenteront les règles métiers pour les présenter sous la forme d'une (+API) permettant la communication avec d'autres applications. Souvent la création du (+BACKOFFICE) se fait également de ce côté avec des technologies comme (+VUEJS) afin de permettre au client de modifier les données de son site web.

### Le front-end

La construction d'interfaces pour les utilisateurs finaux à travers des (+^FRAMEWORK) modernes tels que (+VUEJS) (JavaScript/TypeScript) et Nuxt.js pour la partie (+SEO) avec la notion de (+SSR) en relation avec la partie (+BACKEND) par une (+API) afin de présenter les données en respectant les contraintes d'UI/UX provenant de la maquette ou des demandes du client.

La maquette est créée par le pôle webdesign sur Adobe XD et partagée entre les différents développeurs sur le projet, nous avons une planche spécifique où toutes les couleurs, les polices et les spécificités sont rassemblées pour rendre l'intégration plus facile.

En ce qui concerne cette dernière, nous utilisons la plupart du temps Tailwind CSS sur les projets récents ce qui nous permet d'intégrer la maquette très rapidement mais nous avons aussi pu utiliser des (+^FRAMEWORK) tels que BootstrapVue ou Vuetify.

### Le développement mobile

Useweb a produit une application mobile, ce n'est donc pas la spécialité de l'entreprise, mais je m'en suis occupée, ce qui m'a permis de découvrir les possibilités de développement sur cette plateforme. J'ai donc pu développer une application avec Flutter, un (+FRAMEWORK) développé par Google permettant de développer une application Android et iOS avec le même code qui peut communiquer avec le (+BACKEND) d'une application et donc devenir une alternative à une application web (+FRONTEND).

### Proposer des solutions techniques

Proposer des solutions techniques et en débattre avec l'équipe pour établir les points positifs et négatifs de chaque proposition : choisir les technologies et les (+^FRAMEWORK) les plus adaptés aux règles métiers détaillées dans un cahier des charges. Cela arrive souvent lorsqu'un appel d'offre est gagné afin de répondre au mieux aux demandes du client.

### Traduire un cahier des charges

Les spécifications fonctionnelles étant assez légères, il faut être capable de traduire celles-ci et le cahier des charges en spécifications techniques qui ne seront pas rédigées, mais pour directement développer. Ce qui implique de créer une base de données, les différentes entités en (+BACKEND) et les relations entre elles sur la base de documents assez peu détaillés. Cela demande plusieurs vérifications auprès de mon manager et peut créer des tensions si le client avait énoncé une demande différente de celle qui a été finalement réalisée. Si les différences sont souvent minimes cela génère suffisamment de tensions alors qu'elles pourraient être évités facilement.

### Communiquer avec les clients

Lorsque je suis en charge d'un projet, je dois assister aux réunions et communiquer avec le client sur le niveau d'avancement du projet ou affiner certains détails si besoin. Lors de la phase de recette, je peux également faire la présentation de l'application, en particulier du (+BACKOFFICE) pour présenter son utilisation.

Pour les clients qui ont un compteur de (+TMA)[^1], je vais répondre à leurs questions à travers des tickets.

### Piloter les projets

Piloter les projets de taille restreinte ou moyenne en supervisant une équipe de développeur·euses juniors et suivre les demandes du client. Cela m'est proposé depuis quelques mois, étant donné que j'ai davantage d'expérience que lors de mon arrivée et que certains développeurs sont arrivés plus récemment.

Par conséquent, je développe certaines fonctionnalités, en particulier côté (+BACKEND) et je délègue des tâches, ce qui me permet de gagner du temps et de me concentrer sur des considérations différentes comme l'architecture du projet ou les choix technologiques. Cela me donne plus de recul sur le projet et me permet de mieux le mener que si je devais développer rapidement toutes les fonctionnalités pour rentrer dans le planning.

### Déployer des projets

Déployer des projets sur les serveurs de recette et de production avec un système d'auto déploiement depuis la branche principale de Git avec la mise en place d'un (+CICD). Cela permet à l'équipe de développeurs de suivre le projet en temps réel, en voyant le résultat en ligne, mais aussi au client de participer activement en indiquant ce qui ne lui plaît pas.

Je peux également intervenir sur un projet en cas d'urgence, directement sur le serveur, afin de régler des bugs rapidement.

### Rédiger de la documentation

Rédiger de la documentation de projets pour que toute personne de l'équipe de développement puisse reprendre la main sur un projet. Et cela, même si elle ne le connait pas, en indiquant les bases les plus importantes.

J'ai peu de temps pour écrire de la documentation, mais j'essaye d'en écrire dès que j'en ai la possibilité avec les fichiers *README* dans chaque dépôt et si possible une documentation plus complète sur un support plus adapté. Nous avons pu mettre en place un intranet de type wiki permettant une documentation en Markdown.

### Faire des tests

Ce n'est pas vraiment dans la politique de Useweb à cause de la pression temporelle des projets, mais sur les plus gros projets, il a été possible de mener des tests. Cela permet de rendre les applications beaucoup plus solides et d'être capables d'anticiper les éventuels bugs lors de développement de fonctionnalités supplémentaires. Cela permet de confier le projet à un développeur junior sans s'inquiéter de fonctionnalités qui pourraient souffrir d'effets de bord.

### Répondre aux demandes variées

Je reçois plusieurs demandes par semaines qui ne passent pas par mon manager, qu'elles viennent de Useweb ou des entreprises autour desquelles nous gravitons. La plupart du temps ces demandes concernent des évolutions sur des projets réalisés en interne par Useweb et rentrant dans la liste de tickets détaillant chaque tâche pour chacun des projets. Cela est parfois source de confusion entre les différents acteurs de chaque projet, les demandes sont théoriquement censées passer par mon manager, mais il arrive que les personnes concernées s'adressent directement à moi pour la réalisation de tâches urgentes... ce qui peut me place en position difficile dans la mesure où d'autres tâches occupent souvent mon temps.

Pour tenter de réduire les tensions, je réalise certains tickets prioritaires pour les personnes les ayant rédigés, mais non prioritaires pour mon manager. Je les traite lorsque je termine plus rapidement que prévu une tâche spécifique. Et cela m'amène assez fréquemment à devoir me justifier d'avoir réalisé des tickets auprès de mon manager.

### Éco-conception

Useweb se tourne vers l'éco-conception de sites web par sa politique de RSE afin de créer des sites web respectant certaines normes afin de moins consommer. C'est devenu une de mes missions actuelles et une des valeurs principales de l'entreprise. Il faut souligner qu'il y a un décalage entre la volonté de la direction qui souhaite promouvoir une politique écologique alors que l'équipe de développement qui ne se voit pas donner de temps de veille pour améliorer ses pratiques.

[^1]: notamment à travers Redmine (un outil de pistage des anomalies et de propositions d'évolutions).
[^drupal]: Useweb a longtemps utilisé Drupal pour proposer des (+^BACKOFFICE) prêts à l'emploi, mais les demandes spécifiques des clients ont conduit l'entreprise à préférer un développement sur-mesure avec des outils plus flexibles.
[^windows]: cela m'a posé quelques problèmes au début, car je suis plus habituée à des systèmes plus ouverts comme Linux, mais je m'y suis habituée en quelques jours.
