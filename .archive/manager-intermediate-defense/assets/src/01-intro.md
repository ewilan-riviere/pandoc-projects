# Introduction {.unnumbered}

>Le domaine informatique étant particulièrement anglophone, il est difficile de passer outre les termes concernés dans un document tel que celui-ci. Ainsi, **les termes sont en italique et accompagnés d'un astérisque "\*", ils sont référencés dans le glossaire disponible à la fin de ce document** afin d'approfondir les définitions de ceux-ci. Certains de ces termes définis dans le glossaire auront également une note de bas de page pour des explications propres au contexte si besoin.
>
>D'autres **termes présentent "[ref. #]" qui indique la référence de bibliographie** concernée, disponible à la fin de ce document.

En alternance au sein de l'agence web **Useweb**[@useweb] depuis **octobre 2019**, j'ai le rôle de développeuse web (+FULLSTACK). Mon entreprise travaille surtout sur la réalisation de sites web sur mesure pour des clients, en particulier des entreprises, allant de 3 000 € à 300 000 €. Le but de Useweb est de proposer un suivi précis des demandes des clients afin de leur proposer une solution répondant au mieux à leurs besoins avec la possibilité d'avoir un suivi sur du long terme. Il arrive aussi que nos clients demandent des développements supplémentaires après plusieurs mois si l'outil leur convient mais que leurs besoins ont évolué. Nous suivons donc nos principaux client sur plusieurs années dans le domaine du numérique et la plupart des clients de Useweb ont leurs sites web hébergés sur nos serveurs.

Nous sommes donc une PME d'une dizaine de personnes rachetée par Michael HYOT en 2010 du conseil d'administration de Secob, entreprise de comptabilité. Nous proposons plusieurs prestations :

- **Design alliant l'UI à l'UX**
- **Développement web**
- **Référencement naturel**
- **Référencement payant**
- **Création de site web**
- **Social Ads**

# Useweb, l'agence web

## Une entreprise ? Des entreprises

Présenter Useweb, c'est d'abord présenter SECOB (fondée en 1963), une entreprise de comptabilité comptant environ 300 employés basée à Cesson-Sévigné. Et c'est dans le pôle technologique de SECOB que fut créée Useweb en 1999, elle va côtoyer deux autres entreprises en interne de SECOB : Naviso et Sogescot, éditrices de logiciels pour les comptables. La petite agence web va vivre quelques problèmes de rentabilité jusqu'à son rachat par Mickael HYOT, associé de SECOB, en 2012 où il y aura une refonte de la gestion de l'entreprise. Son activité principale reste encore aujourd'hui la création de site web pour les entreprises allant de budgets de 5 000 € à 300 000 €, par exemple le site web de Laforêt[^laforet] (2019) ou celui de Domitys[^domitys] (2022).

Côté valeurs, SECOB prône l'égalité femme/homme, la diversité dans le recrutement, le dialogue social et le respect de l'articulation vie professionnelle/personnelle, ces valeurs étant partagées par les entreprises en interne dont Useweb. Elle est dirigée par des associés dont Christophe Merel, le président et Stéphanie Gomes, associée principale. Son capital social est élevé à plus de 400 000 €.

- 1999 : création de Useweb
- 2001 : création de Naviso
- 2007 : rachat de cabinet à Nantes, Pornic, Montaigu, Orleans et St Grégoire
- 2012 : rachat de Useweb (créé en 1999)
- 2013 : rachat de Lorient et Paris
- 2021 : création des agences de Montpellier et St Nazaire

![Organigramme de Secob](assets/images/secob.jpg)

Useweb continue de cohabiter avec Naviso et Sogescot dans le pôle technologique même si elle est en partie indépendante du fait de son rachat par un associé de SECOB. Bien qu'elle ait le status de PME avec sa dizaine d'employés, elle bénéficie des avantages de la SECOB ce qui la protège des éventuels problèmes qu'elle a pu rencontrer durant son existence. La présence de deux autres entreprises du monde numérique à ses côtés permet l'échange de connaissances et la migration en interne des employés selon leurs choix de carrière. Ses problèmes de rentabilité ont diminué depuis son rachat et elle dispose aujourd'hui d'un capital social de 100 000 €.

En plus des valeurs de SECOB, le pôle technologique a lancé une nouvelle politique en 2021, celle de la Responsabilité Sociale des Entreprise afin de repenser ses pratiques dans le numérique, pour notamment commencer à envisager de créer des sites éco-responsables. Actuellement l'organisation interne permet un jour de télétravail par semaine depuis la crise sanitaire du COVID-19.

## Organisation

L'équipe est constituée de plusieurs parties qui communiquent les unes avec les autres pour chaque projet.

![Organigramme de Useweb](assets/images/useweb-team.jpg)

La partie référencement portée par Loren LANDRU afin d'améliorer le score de référencement naturel des sites proposés par Useweb mais également de publier des articles sur le sujet, elle est secondée par Sinan SIRKET qui est traffic manager.

La partie design dirigée par Camille BONIFACE, ce qui permet de proposer des maquettes novatrices afin de gagner les appels d'offre, Adèle FOUGERES travaille surtout sur l'aspect UI/UX afin de proposer aux utilisateurs une interface intuitive qui permettra d'améliorer leur expérience.

La partie technique est dirigée par Michel TRAUTH qui répartit les tâches entre les différents profils selon les compétences et les disponibilités de chacun.

- Laurent Chevalier, développeur senior, travaille généralement sur des projets plus anciens et effectue de la maintenance.
- Mathieu MONNIER a une double responsabilité : développeur (+FULLSTACK) et assistant superviseur d'équipe pour seconder Michel TRAUTH.
  - Ewilan RIVIERE, moi-même, ayant pour poste développeuse (+FULLSTACK) en alternance.
  - Ambre VANNEUVILLE qui est développeuse (+FRONTEND) junior.
  - Thomas RETY qui est développeur (+FULLSTACK) en alternance.

Les appels d'offre sont gérés par Caroline LEVASSEUR en relation avec Camille BONIFACE et Michel TRAUTH pour constituer un dossier à la fois technique et de maquettage pour proposer une réponse aux clients.

Enfin la direction est assurée par Mickael HYOT ainsi que par Agnès HUMBERDOT, directrice adjointe, qui dirigent aussi le pôle technologique et donc Naviso et Sogescot. Maryse PASQUIER assure toute la partie gestion de la facturation et des communications internes.

### Communication interne

![Projet lors d'un appel d'offres](assets/images/project.jpg)

Nous allons prendre ici la communication interne lors d'un appel d'offre. Lorsque l'appel gagné (avec l'alliance de la partie technique, design et de C. LEVASSEUR), cela mène à la réalisation d'une maquette finale en prenant soin de contacter régulièrement le client pour que celui-ci puisse valider la partie graphique. En parallèle, Michel TRAUTH va constituer le cahier des charges lors de plusieurs réunions avec le client, puis il rédigera des spécifications fonctionnelles légères et l'équipe de développement travaillera directement avec celles-ci sans passer par des spécifications techniques[^techniques]. Lorsque tout sera terminé côté design, l'équipe de développement pourra commencer et les tâches seront réparties selon la nature du projet et la disponibilité de chacun, à travers un outil de gestion des tâches, Redmine.

Dès que le projet aura atteint une phase stable, une application de recette sera déployée afin de permettre au client de voir l'évolution de son projet et de faire des retours[^retours]. Une fois tous les points des spécifications effectués et validés, une phase de déploiement[^déploiement] en production se fait en relation avec le client, notamment selon si l'hébergement se fait du côté de Useweb ou de son côté.

Enfin, si le client le souhaite, un compteur de TMA[^tma] est mis en place afin de réalisation des évolutions sur plusieurs mois suivant la mise en ligne de la première version. C'est là que la partie référencement met en place des solutions pour améliorer le score de l'application auprès des moteurs de recherche.

Il est à noter que la direction influe rarement sur les projets et leur évolution, en-dehors des projets internes comme par exemple la réalisation des sites web de Naviso ou de SECOB.

L'exemple généraliste ici illustre une situation où tout s'est bien passé. Cependant, la communication est l'un des points faibles de Useweb puisqu'il arrive fréquemment que des fonctionnalités soient vendues alors que l'équipe de développement ne l'apprend qu'au dernier moment et doit réagir très rapidement ou qu'une fonctionnalité soit développée d'une certaine manière pour récupérer du temps sur un projet alors que celle-ci ne correspondra pas exactement à la demande du client.

[^laforet]: https://www.laforet.com
[^domitys]: https://www.domitys.fr
[^techniques]: cette situation est habituelle et est causée par la pression temporelle, chaque projet étant à finir au plus vite. Cela n'est pas sans causer quelques complications, notamment si le client revient sur certains points qui ont été interprétés côté équipe de développement parce qu'ils étaient flous dans le cahier des charges.
[^retours]: généralement, à ce moment le client peut revenir sur certains points précis qui ont pu être interprétés pour indiquer plus précisément ce qui était souhaité. Si c'est indiqué suffisamment en amont, il est souvent assez facile de modifier certains axes du développement pour correspondre aux besoins du client, même si cela peut générer un léger retard.
[^déploiement]: il arrive que le client commence à réagir qu'au moment du déploiement et trouve nombre de points qui lui pose problème, ce qui génère un retard conséquent et des frictions parfois assez intenses en interne comme pendant les réunions client.
[^tma]: il faut bien définir ce qui est une *évolution* de ce qui est une *anomalie*, ce qui est loin d'être simple, en particulier lorsque les spécifications sont floues et non validées.,
