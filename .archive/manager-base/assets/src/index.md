La société Useweb a été chargée du projet de développement d'un extranet pour Télésécurité Loire Bretagne (TLB), filiale de Groupama, par Groupama Loire Bretagne (GLB), il s'agit du troisième projet développé par Useweb pour le client Groupama. TLB[^1] est une société proposant des solutions de surveillance à des professionnels et des particuliers.

La problématique est la suivante : **comment synchroniser les données des techniciens de TLB et celles de l'assureur Groupama sur une seule plateforme ?**

Actuellement les différents acteurs s'échangent les données à travers des feuilles Excel et cela est source d'erreur, il est donc nécessaire de proposer une plateforme répondant à ces besoins.

Afin de centraliser les échanges entre TLB et GLB[^2] sur l'installation du matériel chez chaque client, le client Groupama a besoin d'une plateforme de type extranet qui rassemble plusieurs fonctionnalités. Un système d'authentification permettant d'accéder à l'application web mais en considérant le fait que les utilisateurs qui peuvent se connecter ont des rôles différents (administrateur, support de TLB, commerciaux...). Par conséquent il sera nécessaire de gérer l'affichage selon le rôle de l'utilisateur courant afin que celui-ci n'ait accès qu'aux données autorisées. Les administrateurs auront accès à une liste des utilisateurs afin de pouvoir effectuer les opérations habituelles d'ajout, d'édition et de suppression.

La fonctionnalité principale de l'application permet de lister les fiches clients afin de résumer leur situation et l'avancée de la situation en cours. Chaque entrée client permet de retrouver ses coordonnées ainsi que son statut actuel par rapport à TLB, à savoir l'installation d'équipement à son domicile en passant par la mise en place d'un devis. L'intérêt étant de pouvoir proposer des rendez-vous au client, des devis et de changer le statut en cours directement depuis cette plateforme afin de suivre le client plus efficacement. Le tout en communiquant avec les données techniques de TLB qui permettront d'avoir accès aux données telles que la facturation et sur le technicien assigné à ce client. La résiliation d'un client est également possible, tout comme l'appel au SAV avec trois niveaux de difficulté.

Les données des listes d'utilisateurs et de clients seront sous forme de table de données afin de proposer plusieurs colonnes sur lesquelles il sera possible d'effectuer des opérations de tri et de filtre avec une option d'export au format Excel pour les clients. Il y aura également des statistiques sur le déroulement de l'installation de matériel ou sur les types de produits choisis par exemple.

![Manière dont s'articule la réception d'une demande client](assets/images/workflow.jpg)

La plateforme doit être livré en avril 2022.

[^1]: Télésécurité Loire Bretagne, [www.alarmetlb.fr/qui-sommes-nous](https://www.alarmetlb.fr/qui-sommes-nous).
[^2]: Groupama Loire Bretagne.
