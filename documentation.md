# Documentation

- best tips:
  - <https://learnbyexample.github.io/customizing-pandoc/#pdf-properties>
  - <https://www.flutterbys.com.au/stats/tut/tut17.3.html#h3_11>
  - <https://github.com/alexeygumirov/pandoc-for-pdf-how-to>
- header/footer: <https://en.wikibooks.org/wiki/LaTeX/Customizing_Page_Headers_and_Footers>
- code block lua: <https://github.com/jgm/pandoc/issues/703>
- templating
  - <https://gist.github.com/DannyQuah/04b46cd583f0e87cea7b5009adfb9c5d>
  - <https://github.com/jgm/pandoc/issues/4627> / <https://github.com/jgm/pandoc/issues/1958>

## Templates

- [kjhealy/pandoc-templates](https://github.com/kjhealy/pandoc-templates)
- [ryangrose/easy-pandoc-templates](https://github.com/ryangrose/easy-pandoc-templates)
- [prosegrinder/pandoc-templates](https://github.com/prosegrinder/pandoc-templates)
- [maehr/academic-pandoc-template](https://github.com/maehr/academic-pandoc-template)
- [w8s/pandoc-pdf-template](https://github.com/w8s/pandoc-pdf-template)
- [wikiti/pandoc-book-template](https://github.com/wikiti/pandoc-book-template)
- [Eisvogel](https://github.com/Wandmalfarbe/pandoc-latex-template)

## Pandoc

- use longtable: <https://stackoverflow.com/questions/34073615/how-to-control-tables-in-pandoc-generated-latex>
- code block legend
  - <https://tex.stackexchange.com/questions/127131/pandoc-caption-for-code-blocks>
  - <http://lierdakil.github.io/pandoc-crossref>
- <https://tex.stackexchange.com/questions/234786/how-to-set-a-font-family-with-pandoc>
- <https://superuser.com/questions/796565/correctly-sizing-png-images-in-markdown-with-pandoc-for-html-pdf-docx>
- <https://stackoverflow.com/questions/13515893/set-margin-size-when-converting-from-markdown-to-pdf-with-pandoc>
- <https://tex.stackexchange.com/questions/139139/adding-headers-and-footers-using-pandoc>
- <https://pandoc.org/MANUAL.html>
- <https://tex.stackexchange.com/questions/247416/adding-a-logo-to-a-title-in-pandocs-pdf-output>
- <https://bookdown.org/yihui/rmarkdown-cookbook/latex-logo.html>
- <https://stackoverflow.com/questions/27000906/how-to-set-the-font-size-for-code-blocks-in-pandoc-markdown/47472547>
- <https://github.com/Wandmalfarbe/pandoc-latex-template#custom-template-variables>
- <https://tex.stackexchange.com/questions/369424/pandoc-latex-to-docx-change-font-size>
- <https://pandoc.org/MANUAL.html#pandocs-markdown>
- <https://stackoverflow.com/questions/25591517/pandoc-inserting-pages-before-generated-table-of-contents>
- <https://steemit.com/linux/@karaagac/markdown-document-as-pdf-with-title-page-and-table-of-contents>
- <https://tex.stackexchange.com/questions/139139/adding-headers-and-footers-using-pandoc>
- <https://github.com/jgm/pandoc/issues/5491>
- <https://tex.stackexchange.com/questions/28516/how-to-change-the-title-of-toc>
- <https://superuser.com/questions/1349187/pandoc-how-to-get-pagebreak-between-title-block-and-the-table-of-contents>
- cover
  - <https://github.com/Wandmalfarbe/pandoc-latex-template/issues/43>
  - <https://steemit.com/linux/@karaagac/markdown-document-as-pdf-with-title-page-and-table-of-contents>
  - <https://pandoc.org/MANUAL.html>
  - <https://tex.stackexchange.com/questions/554523/how-to-not-generate-a-title-page-in-pandoc-markdown>
  - make conver with html file <https://bookdown.org/yihui/rmarkdown/template-pandoc.html>
  - template <https://github.com/Wandmalfarbe/pandoc-latex-template>
- change blocks with pandoc <https://github.com/owickstrom/pandoc-emphasize-code>
- filename auto
  - <https://github.com/jgm/pandoc/issues/3250>
- full
  - <https://jdhao.github.io/2019/05/30/markdown2pdf_pandoc/>
- epub
  - <https://pandoc.org/MANUAL.html#epubs>
- latex to markdown
  - <https://tex.stackexchange.com/questions/341899/latex-to-markdown-converter>

### Metadata

- <https://stackoverflow.com/questions/26395374/what-can-i-control-with-yaml-header-options-in-pandoc>
- <https://pandoc.org/MANUAL.html#extension-yaml_metadata_block>

#### Use array or object

```yml
author:
  - name: Ewilan RIVIERE
    email: contact@ewilan-riviere.com
  - name: EwieFairy
authors:
  - { name: Iain Banks, book: The Algebraist }
  - { name: Isaac Asimov, book: Foundation }
```

```tex
$for(authors)$
  $authors.name$ wrote $authors.book$\\
$endfor$
```

### Highlight

- <https://pandoc.org/MANUAL.html#syntax-highlighting>
- <https://stackoverflow.com/questions/30880200/pandoc-what-are-the-available-syntax-highlighters>
- wrap lines <https://stackoverflow.com/questions/20788464/pandoc-doesnt-text-wrap-code-blocks-when-converting-to-pdf>
- themes <https://github.com/KDE/syntax-highlighting/tree/master/data/themes>

```bash
pandoc --list-highlight-languages
pandoc --list-highlight-styles
pandoc --print-highlight-style zenburn > zenburn.theme
pandoc documentation.md --highlight-style zenburn.theme --pdf-engine=xelatex -o document.pdf
```

### `listings`

Add `--listings` flag on `pandoc` command to enable `listings` package

```tex
\usepackage{listings}
\usepackage{color}

\definecolor{background}{RGB}{39, 40, 34}
\definecolor{string}{RGB}{230, 219, 116}
\definecolor{comment}{RGB}{117, 113, 94}
\definecolor{normal}{RGB}{248, 248, 242}
\definecolor{identifier}{RGB}{166, 226, 46}

\lstset{
  language=python,                   % choose the language of the code
  numbers=left,                     % where to put the line-numbers
  stepnumber=1,                     % the step between two line-numbers.        
  numbersep=5pt,                    % how far the line-numbers are from the code
  numberstyle=\tiny\color{black}\ttfamily,
  backgroundcolor=\color{background},    % choose the background color. You must add \usepackage{color}
  showspaces=false,                 % show spaces adding particular underscores
  showstringspaces=false,           % underline spaces within strings
  showtabs=false,                   % show tabs within strings adding particular underscores
  tabsize=4,                        % sets default tabsize to 2 spaces
  captionpos=b,                     % sets the caption-position to bottom
  breaklines=true,                  % sets automatic line breaking
  breakatwhitespace=true,           % sets if automatic breaks should only happen at whitespace
  title=\lstname,                   % show the filename of files included with \lstinputlisting;
  basicstyle=\color{normal}\ttfamily,     % sets font style for the code
  keywordstyle=\color{magenta}\ttfamily, % sets color for keywords
  stringstyle=\color{string}\ttfamily,  % sets color for strings
  commentstyle=\color{comment}\ttfamily, % sets color for comments
  emph={format_string, eff_ana_bf, permute, eff_ana_btr},
  emphstyle=\color{identifier}\ttfamily
}

\newcommand{\passthrough}{\languageshorthands{none}\passthroughtwo}
\newcommand{\passthroughtwo}[1]{#1}
```

### Slides

- <http://pages.stat.wisc.edu/~yandell/statgen/ucla/Help/Producing%20slide%20shows%20with%20Pandoc.html>
- <https://github.com/alexeygumirov/pandoc-beamer-how-to>
