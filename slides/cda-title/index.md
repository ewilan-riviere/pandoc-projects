---
title: "Foncière AALTO"
author: "Ewilan RIVIERE"
subtitle: "Application Flutter"
institute: "Titre de Conceptrice Développeuse d'Applications"
theme: "Frankfurt"
colortheme: "orchid"
fonttheme: "structurebold"
highlight: tango
fontsize: 11pt
aspectratio: 169
date: 08/06/2021
---

# Présentation de l'entreprise

## L'entreprise

![Useweb](images/entreprise/useweb.jpg)

# Contexte du projet

## Cahier des charges

![Back-office et application](images/contexte/contexte.jpg)

## Contraintes & Livrables

![Application Flutter et Laravel](images/contexte/home-tech.jpg)

## Analyse du besoin

![Analyse des besoins](images/contexte/use-case_maquette.jpg)

# Gestion du projet

## Planning et suivi : Gantt du back-end

![Gantt du back-end](images/gestion/gantt-back.jpg)

## Planning et suivi : Gantt de l'application

![Gantt de l'application](images/gestion/gantt-app.jpg)

## Environnement

![Matrice RACI](images/gestion/matrice-raci.jpg)

- Développeuse : Ewilan RIVIERE
- Équipe de développeurs : Adrien Beaudouin, Mathieu MONNIER
- Manager : Michel TRAUTH
- Client : Valentin Ratier

## Environnement : management

![Cycles](images/gestion/cycles.jpg)

## Objectifs de qualité

![Qualité de l'application](images/contexte/qualite.jpg)

# Conception

## Technologies

![Technologies utilisées](images/conception/technologies.jpg)

## Architecture

![architecture](images/conception/architecture.jpg)

## Tables principales

![Tables principales](images/conception/database-main.jpg)

## Tables secondaires

![Tables secondaires](images/conception/database-extra.jpg)

# Développement

## CompanyController & Routes API

![API: CompanyController](images/developement/company-controller.jpg)

## Représentations SQL

![sql](images/developement/sql-examples-controller.jpg)

## Resources

![API: Resources](images/developement/company-resource.jpg)

## Dart : modèle et sérialisation

![model](images/developement/model.jpg)

## IHM : connexion

![Home](images/developement/vue-connexion.jpg)

## Appel API : Sanctum

![Home](images/developement/controller-connexion.jpg)

## IHM : Fonds d'investissement

![Home](images/developement/vue-companies.jpg)

## Sécurité

- CSRF : requêtes effectuées par token
- XSS : validate() et purifier
- SQL injections : PDO binding avec Eloquent
- HTTPS : chiffrement et Let's Encrypt

# Démonstration

## Démonstration

![Screenshots de l'app](images/demonstration/screens.jpg)

# Recherche

## Recherche sur la sérialisation

![Recherche](images/rechercher/flutter-dev.jpg)

# Conclusion

## Application sur les stores

![L'app sur les stores](images/conclusion/stores.jpg)

## Satisfaction

![alt](images/conclusion/satisfaction.jpg)

## Questions

::: columns

:::: {.column width=60%}

![questions](images/conclusion/questions.jpg)

::::

:::: {.column width=40%}

**Crédits**

- Diaporama : Pandoc & LaTeX
- Gantt: TeamGantt
- Images : unDraw, BrowserFrame, Carbon

::::

:::
